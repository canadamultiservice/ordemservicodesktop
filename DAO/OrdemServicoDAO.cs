﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class OrdemServicoDAO
    {
        //INSERIR ORDEM DE SERVIÇO
        public void inserirOrdemServico(tb_os objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_os(objEntrada);

            objbanco.SaveChanges();
        }

        //ALTERAR ORDEM DE SERVICO
        public void AlterarOrdemServiço(tb_os objEntrada)
        {
            banco objbanco = new banco();

            tb_os objresgate = objbanco.tb_os.FirstOrDefault(p => p.cod_os == objEntrada.cod_os);          
            
            objresgate.servicoRealizado_os = objEntrada.servicoRealizado_os;

            objbanco.SaveChanges();
        }
               
        public List<OrdemServicoVO> ConsultarOS(int codigo, int tipo, DateTime data_inicio, DateTime data_fim)
        {
            //instancia banco
            banco objbanco = new banco();


            //CRIA UMA LISTA DO TIPO TB_OS
            List<tb_os> ListaConsultaOS = new List<tb_os>();

            //instancia o objeto ListaConsulta da tb_veiculo
            List<tb_status_os> ListaConsulta = new List<tb_status_os>();

            //instancia o objeto ListaRetorno da classe veiculoVO
            List<OrdemServicoVO> ListaRetorno = new List<OrdemServicoVO>();

            

            //placa veio vazia?
           

            if (tipo == 0)
            {
                //placa vazia? não, Lista a consulta com parametro "PLACA"
                ListaConsultaOS = objbanco.tb_os.Include("tb_cliente.tb_cidades").Include("tb_tecnico").Include("tb_status_os.tb_status").Include("tb_modelo.tb_marca").Where(p => p.cod_os == codigo).ToList();
                

            }
            else
            {                                                        //.Where(p => p.cod_vendedor == codVendedor && p.data_venda >= data_inicio && p.data_venda <= data_fim).ToList();
                //placa vazia? sim, Lista a consulta com parametro "TIPO"
                ListaConsulta = objbanco.tb_status_os.Include("tb_os.tb_cliente.tb_cidades").Include("tb_status").Include("tb_os.tb_tecnico").Include("tb_os.tb_modelo.tb_marca").Where(p => p.cod_status == tipo && p.data >= data_inicio && p.data <= data_fim).ToList();
            }

            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                //1 passo criar meu objeto
                OrdemServicoVO objvo = new OrdemServicoVO();

                //2 Atribuir valores para as propriedades daquele objeto
                objvo.CodOS = ListaConsulta[i].tb_os.cod_os.ToString();
                objvo.CodCliente = ListaConsulta[i].tb_os.cod_cliente.ToString();
                objvo.NomeCliente = ListaConsulta[i].tb_os.tb_cliente.nome_cliente;
                objvo.Equipamento = ListaConsulta[i].tb_os.tb_modelo.nome_modelo;
                objvo.MarcaEquip = ListaConsulta[i].tb_os.tb_modelo.tb_marca.nome_marca;
                objvo.StatusOS = ListaConsulta[i].tb_status.tipo_status;
                objvo.NomeTecnico = ListaConsulta[i].tb_os.tb_tecnico.nome_tecnico;
                objvo.CodTecnico = ListaConsulta[i].tb_os.tb_tecnico.cod_tecnico;
                objvo.DataOS = ListaConsulta[i].data.ToString();
                objvo.servicoRealizado = ListaConsulta[i].tb_os.servicoRealizado_os;
                objvo.CidadeCliente = ListaConsulta[i].tb_os.tb_cliente.tb_cidades.nome_cidade ;
                objvo.CodCidade = ListaConsulta[i].tb_os.tb_cliente.tb_cidades.cod_cidade;
                objvo.DescricaoOS = ListaConsulta[i].tb_os.descricao_os.ToString();

                objvo.EmailCliente = ListaConsulta[i].tb_os.tb_cliente.email_cliente;
                objvo.EnderecoCliente = ListaConsulta[i].tb_os.tb_cliente.endereco_cliente;
                objvo.BairroCliente = ListaConsulta[i].tb_os.tb_cliente.bairro_cliente;
                objvo.CelularCliente = ListaConsulta[i].tb_os.tb_cliente.celular_cliente;
                objvo.TelefoneCliente = ListaConsulta[i].tb_os.tb_cliente.telefone_cliente;
                objvo.ContatoCliente = ListaConsulta[i].tb_os.tb_cliente.contato_cliente;
                objvo.CnpjCliente = ListaConsulta[i].tb_os.tb_cliente.cnpj_cliente;
                objvo.CpfCliente = ListaConsulta[i].tb_os.tb_cliente.cpf_cliente;
                

                
                //objvo.tipoOS = ListaConsulta[i].tb_os.tipo_os.ToString();

                //Adiciona na lista
                ListaRetorno.Add(objvo);


            }

            //usa o for para preencher os campos na tela
            for (int i = 0; i < ListaConsultaOS.Count; i++)
            {
                //1 passo criar meu objeto
                OrdemServicoVO objvo = new OrdemServicoVO();

                //2 Atribuir valores para as propriedades daquele objeto
                objvo.CodOS = ListaConsultaOS[i].cod_os.ToString();
                objvo.NomeCliente = ListaConsultaOS[i].tb_cliente.nome_cliente;
               //objvo.StatusOS = ListaConsulta[i].tb_status.tipo_status;
                objvo.NomeTecnico = ListaConsultaOS[i].tb_tecnico.nome_tecnico;
                objvo.CodTecnico = ListaConsultaOS[i].tb_tecnico.cod_tecnico;
                objvo.DataOS = ListaConsultaOS[i].data_os.ToString();
                objvo.DescricaoOS = ListaConsultaOS[i].descricao_os;
                objvo.servicoRealizado = ListaConsultaOS[i].servicoRealizado_os;
                objvo.CidadeCliente = ListaConsultaOS[i].tb_cliente.tb_cidades.nome_cidade;
                objvo.CodCidade = ListaConsultaOS[i].tb_cliente.tb_cidades.cod_cidade;
                objvo.Equipamento = ListaConsultaOS[i].tb_modelo.nome_modelo;
                objvo.MarcaEquip = ListaConsultaOS[i].tb_modelo.tb_marca.nome_marca;
                objvo.StatusOS = ListaConsultaOS[i].tb_status_os.First().tb_status.tipo_status;
                objvo.servicoRealizado = ListaConsultaOS[i].servicoRealizado_os;

                objvo.EmailCliente = ListaConsultaOS[i].tb_cliente.email_cliente;
                objvo.EnderecoCliente = ListaConsultaOS[i].tb_cliente.endereco_cliente;
                objvo.BairroCliente = ListaConsultaOS[i].tb_cliente.bairro_cliente;
                objvo.CelularCliente = ListaConsultaOS[i].tb_cliente.celular_cliente;
                objvo.TelefoneCliente = ListaConsultaOS[i].tb_cliente.telefone_cliente;
                objvo.ContatoCliente = ListaConsultaOS[i].tb_cliente.contato_cliente;
                objvo.CnpjCliente = ListaConsultaOS[i].tb_cliente.cnpj_cliente;
                objvo.CpfCliente = ListaConsultaOS[i].tb_cliente.cpf_cliente;
               
               

                //Adiciona na lista
                ListaRetorno.Add(objvo);


            }

            return ListaRetorno;
        }

        public List<OrdemServicoVO> ConsultarTecnicoOS(int codigo, DateTime data_inicio, DateTime data_fim)
        {
            banco objbanco = new banco();

            List<tb_os> ListaConsulta = new List<tb_os>();

            List<OrdemServicoVO> ListaRetorno = new List<OrdemServicoVO>();

            ListaConsulta = objbanco.tb_os.Include("tb_cliente").Include("tb_tecnico").Include("tb_status_os.tb_status").Where(p => p.cod_tecnico == codigo && p.data_os >= data_inicio && p.data_os <= data_fim).ToList();

            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                OrdemServicoVO objvo = new OrdemServicoVO();

                objvo.NomeTecnico = ListaConsulta[i].tb_tecnico.nome_tecnico;
                objvo.NomeCliente = ListaConsulta[i].tb_cliente.nome_cliente;
                objvo.DataOS = ListaConsulta[i].tb_status_os.First().data.ToString();
                objvo.StatusOS = ListaConsulta[i].tb_status_os.First().tb_status.tipo_status;

                ListaRetorno.Add(objvo);

            }
            return ListaRetorno;
        }


    }
}
