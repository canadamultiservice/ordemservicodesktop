﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ServicoDAO

    {
        public void InserirServico(tb_servico objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_servico(objEntrada);

            objbanco.SaveChanges();
        }

        public List<tb_servico> ListarServico()
        {
            banco objbanco = new banco();

            List<tb_servico> consulta = objbanco.tb_servico.ToList();

            return consulta;

        }
    }
}
