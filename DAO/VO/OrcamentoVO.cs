﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class OrcamentoVO
    {
        public int codOrc { get; set; }
        public string produto { get; set; }
        public string nomeCliente { get; set; }
        public string nomeVendedor { get; set; }
        public int quantidade { get; set; }
        public decimal preco { get; set; }
        public decimal Total { get; set; }
        public int codProduto { get; set; }
        public decimal subTotal { get; set; }
        public decimal maoObra { get; set; }
        public decimal Desconto { get; set; }
        public string MargenLucro { get; set; }
        public string Observacao { get; set; }
        public string Status { get; set; }
        public string enderecocliente { get; set; }
        public string cnpjCliente { get; set; }
        public string emailCliente { get; set; }
        public string telefoneCliente { get; set; }
        public string cepCliente { get; set; }
        public string contatoCliente { get; set; }
        public string dataEntradaCliente { get; set; }
        public string servicosSolicitados { get; set; }
        public string codCliente { get; set; }
        public string codItens { get; set; }
        public int codCidade { get; set; }
        public string nomeCidade { get; set; }

        //usado somente para impressao do orcamento
        public decimal TotalRelat { get; set; }
        public decimal SubTotalRelat { get; set; }
    }
}
