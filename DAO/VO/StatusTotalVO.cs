﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class StatusTotalVO
    {
        public int TotalAbertas { get; set; }
        public int TotalFechadas { get; set; }
    }
}
