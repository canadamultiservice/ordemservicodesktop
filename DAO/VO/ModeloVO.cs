﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class ModeloVO
    {
        public string NomeModelo { get; set; }
        public string NomeMarca { get; set; }
        public int codMarca { get; set; }
        public int codModelo { get; set; }
        public string codCategoria { get; set; }
        public string Preco { get; set; }
    }
}
