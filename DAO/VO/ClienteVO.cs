﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class ClienteVO
    {
        public string CodCliente { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string bairro { get; set; }
        public string Contato { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Complemento { get; set; }
        public string cod_cidade { get; set; }
        public string nome_cidade { get; set; }
        public string Cnpj { get; set; }
        public string Cpf { get; set; }
        public string inscricaoEstadual { get; set; }
        public string DataCadastro { get; set; }
        public string Observacao { get; set; }
       
        
       


    }
}
