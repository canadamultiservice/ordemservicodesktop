﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class ProdutoVO
    {
       
        public string nomeProduto { get; set; }
        public string modeloProduto { get; set; }
        public string marcaProduto { get; set; }
    }
}
