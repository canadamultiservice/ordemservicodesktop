﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class TecnicoVO
    {
        public string CodTecnico { get; set; }
        public string NomeTecnico { get; set; }
        public string EmailTecnico { get; set; }
        public string celularTecnico { get; set; }
        public string enderecoTecnico { get; set; }
        public string dataCadastro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cpf { get; set; }
        public string Observacao { get; set; }
        public string codCidade { get; set; }
    }
}
