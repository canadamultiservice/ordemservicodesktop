﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class CidadeVO
    {
        public string cod_cidade { get; set; }
        public string nome_cidade { get; set; }
    }
}
