﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class OrdemServicoVO
    {
        public string CodOS { get; set; }
        public string CodCliente { get; set; }
        public string NomeCliente { get; set; }
        public string EnderecoCliente { get; set; }
        public string EmailCliente { get; set; }
        public string CidadeCliente { get; set; }
        public int CodCidade { get; set; }
        public string NomeTecnico { get; set; }
        public int CodTecnico { get; set; }
        public string DataOS { get; set; }    
        public string BairroCliente { get; set; }
        public string TelefoneCliente { get; set; }
        public string CelularCliente { get; set; }
        public string ContatoCliente { get; set; }
        public string CnpjCliente { get; set; }
        public string CpfCliente { get; set; }
        public string tipoOS { get; set; }
        public string DescricaoOS { get; set; }
        public string servicoRealizado { get; set; }
        public string Equipamento { get; set; }
        public string MarcaEquip { get; set; }
        public string StatusOS { get; set; }
       
    }
}
