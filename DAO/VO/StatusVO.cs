﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
    public class StatusVO
    {
        public string CodOs { get; set; }
        public string NomeCliente { get; set; }
        public string NomeTecnico { get; set; }
        public string CodStatus { get; set; }
        public string nomeStatus { get; set; }
        public string DataEntrada { get; set; }
        public string DataSaida { get; set; }

    }
    
}
