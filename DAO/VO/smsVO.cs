﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.VO
{
   public class smsVO
    {
        public string ipGAteway { get; set; }
        public string usuarioGateway { get; set; }
        public string senhaGateway { get; set; }
        public string numeroCelular { get; set; }
        public string nomeTecnico { get; set; }
    }
}
