﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class Status_OrcamentoDAO
    {
        public void InserirStatus(tb_status_orcamento objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_status_orcamento(objEntrada);
            objbanco.SaveChanges();
        }

        public void AlterarStatus(tb_status_orcamento objEntrada)
        {
            banco objbanco = new banco();

            tb_status_orcamento retorno = objbanco.tb_status_orcamento.FirstOrDefault(p => p.cod_status_orc == objEntrada.cod_status_orc);

            retorno.cod_status = objEntrada.cod_status;

            objbanco.SaveChanges();
        }
    }
}
