﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class MarcaDAO
    {

        public void Inserirmarca(tb_marca objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_marca(objEntrada);

            objbanco.SaveChanges();
        }

        public void AlterarMarca(tb_marca objEntrada)
        {
            banco objbanco = new banco();

            tb_marca retorno = objbanco.tb_marca.FirstOrDefault(p => p.cod_marca == objEntrada.cod_marca);

            retorno.nome_marca = objEntrada.nome_marca;

            objbanco.SaveChanges();
        }

        public void ExcluirMarca(int cod)
        {
            banco objbanco = new banco();

            tb_marca objmarca = objbanco.tb_marca.FirstOrDefault(p => p.cod_marca == cod);

            objbanco.DeleteObject(objmarca);

            objbanco.SaveChanges();
        }

        public List<tb_marca> ListarMarcas()
        {
            banco objbanco = new banco();

            List<tb_marca> listaConsulta = objbanco.tb_marca.ToList();

            return listaConsulta;


        }

       

    }
}
