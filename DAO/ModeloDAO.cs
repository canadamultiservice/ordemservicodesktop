﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ModeloDAO
    {
        public void InserirModelo(tb_modelo objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_modelo(objEntrada);

            objbanco.SaveChanges();

        }

        public void AlterarModelo(tb_modelo objEntrada)
        {
            banco objbanco = new banco();

            tb_modelo retorno = objbanco.tb_modelo.FirstOrDefault(p => p.cod_modelo == objEntrada.cod_modelo);

            retorno.nome_modelo = objEntrada.nome_modelo;
            retorno.cod_marca = objEntrada.cod_marca;
            retorno.cod_categoria = objEntrada.cod_categoria;
            retorno.preco_modelo = objEntrada.preco_modelo;

            objbanco.SaveChanges();
        }

        public List<ModeloVO> ListarModelos(string nome)
        {
            banco objbanco = new banco();

            List<ModeloVO> listaRetorno = new List<ModeloVO>();

            List<tb_modelo> listaConsulta = new List<tb_modelo>();

            if (nome == string.Empty)
            {
                listaConsulta = objbanco.tb_modelo.Include("tb_marca").Include("tb_categoria").OrderBy(p => p.nome_modelo).ToList();
            }
            else
            {
                listaConsulta = objbanco.tb_modelo.Include("tb_marca").Where(p => p.nome_modelo.Contains(nome)).OrderBy(p => p.nome_modelo).ToList();
            }

            

            for (int i = 0; i < listaConsulta.Count; i++)
            {
                ModeloVO objvo = new ModeloVO();

                
               objvo.NomeModelo = listaConsulta[i].nome_modelo;
               objvo.NomeMarca = listaConsulta[i].tb_marca.nome_marca;
               objvo.codMarca = listaConsulta[i].tb_marca.cod_marca;
               objvo.codModelo = listaConsulta[i].cod_modelo;
               objvo.codCategoria = listaConsulta[i].cod_categoria.ToString();
               objvo.Preco = listaConsulta[i].preco_modelo;

                listaRetorno.Add(objvo);

            }

            return listaRetorno;
            
        }


        public void ExcluirModelo(int cod)
        {
            banco objbanco = new banco();

            tb_modelo objmodelo = objbanco.tb_modelo.FirstOrDefault(p => p.cod_modelo == cod);

            objbanco.DeleteObject(objmodelo);

            objbanco.SaveChanges();
        }

        public List<tb_modelo> ConsultarModelo(int codMarca)
        {
            banco objbanco = new banco();

            List<tb_modelo> consulta = objbanco.tb_modelo.Where(p => p.cod_marca == codMarca).ToList();

            return consulta;
        }

        public List<tb_modelo> ConsultarMarcaCategoria(int cod)
        {
            banco objbanco = new banco();

            List<tb_modelo> Consulta = objbanco.tb_modelo.Where(p => p.cod_marca == cod).ToList();

            return Consulta;
        }

        public List<tb_modelo> consultarTodosModelos()
        {
            banco objbanco = new banco();

            List<tb_modelo> consulta = objbanco.tb_modelo.ToList();

            return consulta;
        }

        public tb_modelo ConsultarProduto(int codModelo)
        {
            banco objbanco = new banco();

            tb_modelo consulta = objbanco.tb_modelo.FirstOrDefault(p => p.cod_modelo == codModelo);

           
            return consulta;
        }

    }
}
