﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
   public class TecnicoDAO
    {
        public void InserirTecnico(tb_tecnico objEntrada)
        {
            //instancia o banco
            banco objbanco = new banco();

            //adiciona objeto no banco
            objbanco.AddTotb_tecnico(objEntrada);

            //salva no banco
            objbanco.SaveChanges();
        }

        public void AlterarTecnico(tb_tecnico objEntrada)
        {
            //instancia banco
            banco objbanco = new banco();

            //resgata o codigo e compara cod_tecnico com o objeto objEntrada.cod_tecnico
            tb_tecnico objRetorno = objbanco.tb_tecnico.FirstOrDefault(p => p.cod_tecnico == objEntrada.cod_tecnico);

            //atualiza os registro

            
            objRetorno.nome_tecnico = objEntrada.nome_tecnico;
            objRetorno.data_cadastro = objEntrada.data_cadastro;
            objRetorno.celular_tecnico = objEntrada.celular_tecnico.ToString();
            objRetorno.email_tecnico = objEntrada.email_tecnico;
            //objRetorno.senha_tecnico = objEntrada.senha_tecnico.ToString();
            objRetorno.endereco_tecnico = objEntrada.endereco_tecnico;
            objRetorno.bairro = objEntrada.bairro;
            objRetorno.cpf_tecnico = objEntrada.cpf_tecnico;
            objRetorno.cod_cidade = Convert.ToInt32(objEntrada.cod_cidade);
            objRetorno.complemento_tecnico = objEntrada.complemento_tecnico;
            objRetorno.email_tecnico = objEntrada.email_tecnico;
            objRetorno.observacao_tecnico = objEntrada.observacao_tecnico;


            objbanco.SaveChanges();

        }

        public void ExcluirTecnico(int codTecnico)
        {
            banco objbanco = new banco();

            tb_tecnico objTecncico = objbanco.tb_tecnico.FirstOrDefault(p => p.cod_tecnico == codTecnico);

            objbanco.DeleteObject(objTecncico);

            objbanco.SaveChanges();
        }

        public List<TecnicoVO> ConsultaTecnico(string nome)
        {
            //instancia o banco
            banco objbanco = new banco();

            List<tb_tecnico> ListaConsulta = new List<tb_tecnico>();

            List<TecnicoVO> ListaRetorno = new List<TecnicoVO>();

            if (nome == string.Empty)
            {
                ListaConsulta = objbanco.tb_tecnico.OrderBy(p => p.nome_tecnico).ToList();
            }
            else
            {
                ListaConsulta = objbanco.tb_tecnico.Where(p => p.nome_tecnico.Contains(nome)).OrderBy(p => p.nome_tecnico).ToList();
            }

            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                TecnicoVO objvo = new TecnicoVO();

                objvo.CodTecnico = ListaConsulta[i].cod_tecnico.ToString();
                objvo.NomeTecnico = ListaConsulta[i].nome_tecnico;
                objvo.enderecoTecnico = ListaConsulta[i].endereco_tecnico;
                objvo.celularTecnico = ListaConsulta[i].celular_tecnico;
                objvo.EmailTecnico = ListaConsulta[i].email_tecnico;
                objvo.dataCadastro = ListaConsulta[i].data_cadastro.ToString();
                objvo.Complemento = ListaConsulta[i].complemento_tecnico;
                objvo.Cpf = ListaConsulta[i].cpf_tecnico;
                objvo.Bairro = ListaConsulta[i].bairro;
                objvo.celularTecnico = ListaConsulta[i].celular_tecnico;
                objvo.Observacao = ListaConsulta[i].observacao_tecnico;
                objvo.codCidade = ListaConsulta[i].cod_cidade.ToString();



                ListaRetorno.Add(objvo);
            }

            return ListaRetorno;
           
        }

        public List<tb_tecnico> ListarTecnico()
        {
            banco objbanco = new banco();

            List<tb_tecnico> Listaconsulta = objbanco.tb_tecnico.OrderBy(p => p.nome_tecnico).ToList();

            return Listaconsulta;
        }

        public tb_tecnico buscarTecnico(int cod)
        {
            banco objbanco = new banco();

            tb_tecnico retorno = objbanco.tb_tecnico.FirstOrDefault(p => p.cod_tecnico == cod);

            return retorno;
        }



    }
}
