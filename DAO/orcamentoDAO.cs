﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class orcamentoDAO
    {
        public void inserirOrcamento(tb_orcamento objentrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_orcamento(objentrada);

            objbanco.SaveChanges();
        }

        public void inserirItens(tb_itens objentrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_itens(objentrada);

            objbanco.SaveChanges();
        }

        public List<OrcamentoVO> PesquisarOrcamento(int codOrc, int codStatus, DateTime dtini, DateTime dtfim)
        {
            banco objbanco = new banco();

            List<tb_orcamento> listaconsulta = new List<tb_orcamento>();

            List<tb_status_orcamento> listaconsultaStatus = new List<tb_status_orcamento>();

            List<OrcamentoVO> listaretrono = new List<OrcamentoVO>();

            if (codStatus == 0)
            {
                listaconsulta = objbanco.tb_orcamento.Include("tb_cliente").Include("tb_itens.tb_modelo").Include("tb_administrador").Include("tb_status_orcamento.tb_status").Where(p => p.cod_orcamento == codOrc).ToList();
            }
            else
            {
                listaconsultaStatus = objbanco.tb_status_orcamento.Include("tb_orcamento.tb_cliente").Include("tb_orcamento.tb_itens").Include("tb_status").Include("tb_orcamento.tb_administrador").Where(p => p.cod_status == codStatus && p.data_status_orc >= dtini && p.data_status_orc <= dtfim).ToList();
            }


            for (int i = 0; i < listaconsulta.Count; i++)
            {
                OrcamentoVO objvo = new OrcamentoVO();

                objvo.codOrc = listaconsulta[i].cod_orcamento;
                objvo.nomeCliente = listaconsulta[i].tb_cliente.nome_cliente.ToString();
                objvo.nomeVendedor = listaconsulta[i].tb_administrador.nome_administrador;
                objvo.Status = listaconsulta[i].tb_status_orcamento.First().tb_status.tipo_status;

                listaretrono.Add(objvo);

            }

            for (int i = 0; i < listaconsultaStatus.Count; i++)
            {
                OrcamentoVO objvo = new OrcamentoVO();

                objvo.codOrc = listaconsultaStatus[i].cod_orcamento;
                objvo.nomeCliente = listaconsultaStatus[i].tb_orcamento.tb_cliente.nome_cliente;
                objvo.nomeVendedor = listaconsultaStatus[i].tb_orcamento.tb_administrador.nome_administrador;
                objvo.Status = listaconsultaStatus[i].tb_status.tipo_status;

                listaretrono.Add(objvo);

            }

            return listaretrono;


        }

        public List<OrcamentoVO> PesquisarTodosOrcamento()
        {
            banco objbanco = new banco();

            List<tb_orcamento> listaconsulta = new List<tb_orcamento>();

            List<OrcamentoVO> listaretrono = new List<OrcamentoVO>();

            listaconsulta = objbanco.tb_orcamento.Include("tb_cliente").Include("tb_itens.tb_modelo").Include("tb_administrador").Include("tb_status_orcamento.tb_status").OrderBy(p => p.cod_orcamento).ToList();

            for (int i = 0; i < listaconsulta.Count; i++)
            {
                OrcamentoVO objvo = new OrcamentoVO();

                objvo.codOrc = listaconsulta[i].cod_orcamento;
                objvo.nomeCliente = listaconsulta[i].tb_cliente.nome_cliente.ToString();
                objvo.nomeVendedor = listaconsulta[i].tb_administrador.nome_administrador;
                objvo.Status = listaconsulta[i].tb_status_orcamento.First().tb_status.tipo_status;

                listaretrono.Add(objvo);

            }

            return listaretrono;


        }

        public List<OrcamentoVO> ListarItensOrcamento(int codOrc)
        {
            banco objbanco = new banco();

            List<tb_itens> listaconsulta = new List<tb_itens>();

            List<OrcamentoVO> listaretorno = new List<OrcamentoVO>();

            listaconsulta = objbanco.tb_itens.Include("tb_orcamento.tb_cliente.tb_cidades").Include("tb_orcamento").Include("tb_orcamento.tb_administrador").Include("tb_orcamento.tb_status_orcamento.tb_status").Include("tb_modelo").Where(p => p.cod_orcamento == codOrc).ToList();

            for (int i = 0; i < listaconsulta.Count; i++)
            {
                OrcamentoVO objvo = new OrcamentoVO();

                // decimal preco = Convert.ToDecimal(listaconsulta[i].valor_itens);

                objvo.enderecocliente = listaconsulta[i].tb_orcamento.tb_cliente.endereco_cliente;
                objvo.codCliente = listaconsulta[i].tb_orcamento.tb_cliente.cod_cliente.ToString();
                objvo.cnpjCliente = listaconsulta[i].tb_orcamento.tb_cliente.cnpj_cliente;
                objvo.telefoneCliente = listaconsulta[i].tb_orcamento.tb_cliente.telefone_cliente;
                objvo.emailCliente = listaconsulta[i].tb_orcamento.tb_cliente.email_cliente;
                objvo.contatoCliente = listaconsulta[i].tb_orcamento.tb_cliente.contato_cliente;
                objvo.dataEntradaCliente = listaconsulta[i].tb_orcamento.data_orcamento.ToString();
                objvo.produto = listaconsulta[i].tb_modelo.nome_modelo;
                objvo.quantidade = listaconsulta[i].qtde_itens;
                objvo.preco = Convert.ToDecimal(listaconsulta[i].valor_itens);
                objvo.servicosSolicitados = listaconsulta[i].tb_orcamento.obs_orcamento;
                objvo.codOrc = listaconsulta[i].tb_orcamento.cod_orcamento;
                objvo.nomeVendedor = listaconsulta[i].tb_orcamento.tb_administrador.nome_administrador;
                objvo.maoObra = Convert.ToDecimal(listaconsulta[i].tb_orcamento.mo_orcamento == "" ? "0,00" : (listaconsulta[i].tb_orcamento.mo_orcamento));
                objvo.Desconto = Convert.ToDecimal(listaconsulta[i].tb_orcamento.desconto_orcamento);
                objvo.nomeCliente = listaconsulta[i].tb_orcamento.tb_cliente.nome_cliente;
                objvo.Observacao = listaconsulta[i].tb_orcamento.obs_orcamento.ToString();
                objvo.codProduto = listaconsulta[i].cod_modelo;
                objvo.codCidade = Convert.ToInt32(listaconsulta[i].tb_orcamento.tb_cliente.cod_cidade);
                objvo.nomeCidade = listaconsulta[i].tb_orcamento.tb_cliente.tb_cidades.nome_cidade;
                



                listaretorno.Add(objvo);


            }

            return listaretorno;
        }

        public void AlterarOrcamento(tb_orcamento objEntrada)
        {
            banco objbanco = new banco();

            tb_orcamento objresgate = objbanco.tb_orcamento.FirstOrDefault(p => p.cod_orcamento == objEntrada.cod_orcamento);

            objresgate.data_orcamento = objEntrada.data_orcamento;
            objresgate.desconto_orcamento = objEntrada.desconto_orcamento;
            objresgate.mo_orcamento = objEntrada.mo_orcamento;
            objresgate.obs_orcamento = objEntrada.obs_orcamento;

            objbanco.SaveChanges();
        }

        public void ExcluirItens(int codOrc)
        {
            banco objbanco = new banco();

            tb_itens objitens = objbanco.tb_itens.FirstOrDefault(p => p.cod_orcamento == codOrc);
            
            objbanco.DeleteObject(objitens);

            objbanco.SaveChanges();

        }

    }
}
