﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class StatusDAO
    {
        public void InserirStatus(tb_status_os objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_status_os(objEntrada);

            objbanco.SaveChanges();
        }

        //CARREGA STATUS NA COMBO-BOX - ESTATICO
        public List<tb_status> CosnultarStatus()
        {
            banco objbanco = new banco();

            List<tb_status> listaConsulta = objbanco.tb_status.ToList();

            return listaConsulta;

        }

        //CARREGA GRIDVIEW MONITOR DE STATUS
        public List<StatusVO> ConsultarStatus(DateTime dtinicio, DateTime dtfinal)

        {
            banco objbanco = new banco();

            List<tb_status_os> ListaConsulta = new List<tb_status_os>();

            List<StatusVO> ListaRetorno = new List<StatusVO>();

            ListaConsulta = objbanco.tb_status_os.Include("tb_os.tb_cliente.tb_cidades").Include("tb_status").Include("tb_os.tb_tecnico").Include("tb_os.tb_modelo.tb_marca").Where(p => p.data >= dtinicio && p.data <= dtfinal).ToList();
            //ListaConsulta = objbanco.tb_status_os.Include("tb_os.tb_cliente").Include("tb_status").Include("tb_os.tb_tecnico").OrderBy(p => p.cod_os).ToList();

            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                StatusVO objvo = new StatusVO();

                objvo.CodOs = ListaConsulta[i].cod_os.ToString();
                objvo.NomeCliente = ListaConsulta[i].tb_os.tb_cliente.nome_cliente.ToString();
                objvo.NomeTecnico = ListaConsulta[i].tb_os.tb_tecnico.nome_tecnico.ToString();
                objvo.CodStatus = ListaConsulta[i].tb_status.tipo_status.ToString();
                objvo.DataEntrada = ListaConsulta[i].tb_os.data_os.ToString();
                objvo.DataSaida = ListaConsulta[i].data.ToString();
                ListaRetorno.Add(objvo);

                
            }

            return ListaRetorno;

        }

      //  public List<StatusVO> ConsultarStatusTecnico(int codigo, DateTime dt_inico, DateTime dt_fim)
      //  {
         //   banco objbanco = new banco();

           // List<tb_status_os> ListaConsulta = new List<tb_status_os>();

            //List<StatusVO> ListaRetorno = new List<StatusVO>();

//ListaConsulta = objbanco.tb_status_os.Include("tb_os.tb_ciente").Include("tb_status").Include("tb_os.tb_tecnico").Where(p => p. ).ToList;

      //  }

     
        public void AlterarStatusOS(tb_status_os objEntrada)
        {
            banco objbanco = new banco();

            tb_status_os objresgate = objbanco.tb_status_os.FirstOrDefault(p => p.cod_os == objEntrada.cod_os);

            objresgate.cod_status = objEntrada.cod_status;

            objbanco.SaveChanges();

        }



        public List<StatusTotalVO> ConsultarTotalStatus(int codigo)
        {
            banco objbanco = new banco();

            List<tb_status_os> ListaConsulta = new List<tb_status_os>();

            List<StatusTotalVO> listaretorno = new List<StatusTotalVO>();

            ListaConsulta = objbanco.tb_status_os.Where(p =>  p.cod_status == codigo).ToList();


            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                StatusTotalVO objvo = new StatusTotalVO();

                objvo.TotalAbertas = ListaConsulta[i].cod_status;
               
                listaretorno.Add(objvo);
                
            }
                      
            return listaretorno;
        }




    }
}
