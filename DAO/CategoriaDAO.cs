﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
   public class CategoriaDAO
    {
        public void InserirCategoria(tb_categoria objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_categoria(objEntrada);

            objbanco.SaveChanges();
        }

        public List<tb_categoria> ConsultarCategoria()
        {
            banco objBanco = new banco();

            List<tb_categoria> listaConsulta = objBanco.tb_categoria.ToList();

            return listaConsulta;
        }

        public void AlterarCategoria(tb_categoria objEntrada)
        {
            banco objbanco = new banco();

           tb_categoria Retorno = objbanco.tb_categoria.FirstOrDefault(p => p.cod_categoria == objEntrada.cod_categoria);

            Retorno.nome_categoria = objEntrada.nome_categoria;
           

            objbanco.SaveChanges();

        }

        public void ExcluirCategoria(int cod)
        {
            banco objbanco = new banco();

            tb_categoria retorno = objbanco.tb_categoria.FirstOrDefault(p => p.cod_categoria == cod);

            objbanco.DeleteObject(retorno);

            objbanco.SaveChanges();
        }



    }
}
