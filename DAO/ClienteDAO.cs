﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ClienteDAO
    {
        public void InserirCliente(tb_cliente objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_cliente(objEntrada);

            objbanco.SaveChanges();
        }

        public void InserirCidade(tb_cidades objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_cidades(objEntrada);

            objbanco.SaveChanges();
        }

        //carrega ComboBox-Cidade
        public List<tb_cidades> ConsultarCidade()
        {
            banco objbanco = new banco();


            List<tb_cidades> ListaConsulta = objbanco.tb_cidades.OrderBy(p => p.nome_cidade).ToList();

            return ListaConsulta;

        }


        public List<CidadeVO> FiltrarCidade(int cod)
        {
            banco objbanco = new banco();

            List<tb_cidades> ListaConsulta = new List<tb_cidades>();

            List<CidadeVO> ListaRetorno = new List<CidadeVO>();



            if (cod == 0 )
            {
                ListaConsulta = objbanco.tb_cidades.OrderBy(p => p.nome_cidade).ToList();
            }
            else
            {
                ListaConsulta = objbanco.tb_cidades.Where(p => p.cod_cidade == cod).ToList();
            }


            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                CidadeVO objvo = new CidadeVO();

              
                objvo.nome_cidade = ListaConsulta[i].nome_cidade;
                objvo.cod_cidade = ListaConsulta[i].cod_cidade.ToString();

                ListaRetorno.Add(objvo);

            }

            return ListaRetorno;
        }

        //alterar cliente
        public void AlterarCliente(tb_cliente objEntrada)
        {
            banco objbanco = new banco();

            tb_cliente retorno = objbanco.tb_cliente.FirstOrDefault(p => p.cod_cliente == objEntrada.cod_cliente);

            retorno.nome_cliente = objEntrada.nome_cliente;
            retorno.celular_cliente = objEntrada.celular_cliente;
            retorno.cnpj_cliente = objEntrada.cnpj_cliente;
            retorno.cpf_cliente = objEntrada.cpf_cliente;
            retorno.email_cliente = objEntrada.email_cliente;
            retorno.endereco_cliente = objEntrada.endereco_cliente;
            retorno.telefone_cliente = objEntrada.telefone_cliente;
            retorno.cod_cidade = objEntrada.cod_cidade;
            retorno.bairro_cliente = objEntrada.bairro_cliente;
            retorno.observacao_cliente = objEntrada.observacao_cliente;
            retorno.complemento_cliente = objEntrada.complemento_cliente;
            retorno.inscricaoEstadual_cliente = objEntrada.inscricaoEstadual_cliente;
            retorno.contato_cliente = objEntrada.contato_cliente;

            objbanco.SaveChanges();

        }

        //filtra cliente pelo codigo
        public tb_cliente FiltrarCliente(int codigo)
        {
            //instancia o banco
            banco objbanco = new banco();

            //compara cod_cliente com o parametro --> codigo
            tb_cliente retorno = objbanco.tb_cliente.FirstOrDefault(p => p.cod_cliente == codigo);

            //retorna consulta
            return retorno;
        }

        //consulta cliente pelo nome
        public List<ClienteVO> ConsultarCliente(string nome)
        {
            banco objbanco = new banco();

            List<tb_cliente> ListaConsulta = new List<tb_cliente>();

            List<ClienteVO> ListaRetorno = new List<ClienteVO>();



            if (nome == string.Empty)
            {
                ListaConsulta = objbanco.tb_cliente.Include("tb_cidades").OrderBy(p => p.nome_cliente).ToList();
            }
            else
            {
                ListaConsulta = objbanco.tb_cliente.Include("tb_cidades").Where(p => p.nome_cliente.Contains(nome)).OrderBy(p => p.nome_cliente).ToList();
            }


            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                ClienteVO objvo = new ClienteVO();

                objvo.CodCliente = ListaConsulta[i].cod_cliente.ToString();
                objvo.Nome = ListaConsulta[i].nome_cliente;
                objvo.Endereco = ListaConsulta[i].endereco_cliente;
                objvo.Celular = ListaConsulta[i].celular_cliente;
                objvo.Cnpj = ListaConsulta[i].cnpj_cliente;
                //objvo.nome_cidade = (ListaConsulta[i].tb_cidades != null ? ListaConsulta[i].tb_cidades.nome_cidade : "");
                objvo.nome_cidade = ListaConsulta[i].tb_cidades.nome_cidade;
                objvo.cod_cidade = ListaConsulta[i].tb_cidades.cod_cidade.ToString();
                objvo.Telefone = ListaConsulta[i].telefone_cliente;
                objvo.Email = ListaConsulta[i].email_cliente;
                objvo.Cpf = ListaConsulta[i].cpf_cliente;
                objvo.bairro = ListaConsulta[i].bairro_cliente;
                objvo.inscricaoEstadual = ListaConsulta[i].inscricaoEstadual_cliente;
                objvo.Complemento = ListaConsulta[i].complemento_cliente;
                objvo.Contato = ListaConsulta[i].contato_cliente;
                objvo.DataCadastro = ListaConsulta[i].data_cliente.ToString();
                objvo.Observacao = ListaConsulta[i].observacao_cliente;

                ListaRetorno.Add(objvo);

            }

            return ListaRetorno;
        }

        //consulta cliente por telefone
        public List<ClienteVO> ConsultarClienteTelefone(string telefone)
        {
            banco objbanco = new banco();

            List<tb_cliente> ListaConsulta = new List<tb_cliente>();

            List<ClienteVO> ListaRetorno = new List<ClienteVO>();

            ListaConsulta = objbanco.tb_cliente.Where(p => p.telefone_cliente.Contains(telefone)).OrderBy(p => p.telefone_cliente).ToList();

            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                ClienteVO objvo = new ClienteVO();

                objvo.CodCliente = ListaConsulta[i].cod_cliente.ToString();
                objvo.Nome = ListaConsulta[i].nome_cliente;
                objvo.Endereco = ListaConsulta[i].endereco_cliente;
                objvo.Celular = ListaConsulta[i].celular_cliente;
                objvo.Cnpj = ListaConsulta[i].cnpj_cliente;
                objvo.nome_cidade = (ListaConsulta[i].tb_cidades != null ? ListaConsulta[i].tb_cidades.nome_cidade : "");
                objvo.Telefone = ListaConsulta[i].telefone_cliente;
                objvo.Email = ListaConsulta[i].email_cliente;
                objvo.Cpf = ListaConsulta[i].cpf_cliente;
                objvo.bairro = ListaConsulta[i].bairro_cliente;
                objvo.inscricaoEstadual = ListaConsulta[i].inscricaoEstadual_cliente;
                objvo.Complemento = ListaConsulta[i].complemento_cliente;
                objvo.Contato = ListaConsulta[i].contato_cliente;
                objvo.DataCadastro = ListaConsulta[i].data_cliente.ToString();
                objvo.Observacao = ListaConsulta[i].observacao_cliente;

                ListaRetorno.Add(objvo);


            }
            return ListaRetorno;
        }

        //consulta cliente por data
        public List<ClienteVO> ConsultarClienteData(DateTime dt_inicial, DateTime dt_final)
        {
            banco objbanco = new banco();

            List<tb_cliente> ListaConsulta = new List<tb_cliente>();

            List<ClienteVO> ListaRetorno = new List<ClienteVO>();


            ListaConsulta = objbanco.tb_cliente.Include("tb_cidade").Where(p => p.data_cliente >= dt_inicial && p.data_cliente <= dt_final).ToList();

            for (int i = 0; i < ListaConsulta.Count; i++)
            {
                ClienteVO objvo = new ClienteVO();

                objvo.CodCliente = ListaConsulta[i].cod_cliente.ToString();
                objvo.Nome = ListaConsulta[i].nome_cliente;
                objvo.Endereco = ListaConsulta[i].endereco_cliente;
                objvo.Celular = ListaConsulta[i].celular_cliente;
                objvo.Cnpj = ListaConsulta[i].cnpj_cliente;
                objvo.nome_cidade = (ListaConsulta[i].tb_cidades != null ? ListaConsulta[i].tb_cidades.nome_cidade : "");
                objvo.Telefone = ListaConsulta[i].telefone_cliente;
                objvo.Email = ListaConsulta[i].email_cliente;
                objvo.Cpf = ListaConsulta[i].cpf_cliente;
                objvo.bairro = ListaConsulta[i].bairro_cliente;
                objvo.inscricaoEstadual = ListaConsulta[i].inscricaoEstadual_cliente;
                objvo.Complemento = ListaConsulta[i].complemento_cliente;
                objvo.Contato = ListaConsulta[i].contato_cliente;
                objvo.DataCadastro = ListaConsulta[i].data_cliente.ToString();
                objvo.Observacao = ListaConsulta[i].observacao_cliente;

                ListaRetorno.Add(objvo);

            }

            return ListaRetorno;
        }

        //exclui cliente
        public void ExcluirCliente(int codCliente)
        {
            banco objbanco = new banco();

            tb_cliente objcliente = objbanco.tb_cliente.FirstOrDefault(p => p.cod_cliente == codCliente);

            objbanco.DeleteObject(objcliente);

            objbanco.SaveChanges();
        }

    }
}
