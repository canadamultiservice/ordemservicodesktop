﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class smsDAO
    {
        public void inserirGateway(tb_sms objEntrada)
        {
            banco objbanco = new banco();

            objbanco.AddTotb_sms(objEntrada);

            objbanco.SaveChanges();

        }


        public void alterarGateway(tb_sms objEntrada)
        {
            banco objbanco = new banco();

            tb_sms retorno = objbanco.tb_sms.FirstOrDefault(p => p.cod_sms == objEntrada.cod_sms);

            retorno.ip_sms = objEntrada.ip_sms;
            retorno.user_sms = objEntrada.user_sms;
            retorno.senha_sms = objEntrada.senha_sms;

            objbanco.SaveChanges();


        }

        public tb_sms ConsultarGW()
        {
            banco objbanco = new banco();

            tb_sms retorno = objbanco.tb_sms.FirstOrDefault();
            return retorno;

        }


        public List<smsVO> consultarGateway()
        {
            banco objbanco = new banco();

            List<tb_sms> listaConsulta = new List<tb_sms>();

            List<smsVO> listaRetorno = new List<smsVO>();

            listaConsulta = objbanco.tb_sms.ToList();

            for (int i = 0; i < listaConsulta.Count; i++)
            {
                smsVO objvo = new smsVO();

                objvo.ipGAteway = listaConsulta[i].ip_sms;
                objvo.usuarioGateway = listaConsulta[i].user_sms;
                objvo.senhaGateway = listaConsulta[i].senha_sms;

                listaRetorno.Add(objvo);               

            }

            return listaRetorno;          
            

        }




        public void enviarSMS(string numero)
        {

        }
    }
}
