﻿namespace ordemService
{
    partial class frmRpConsultaCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRpConsultaCliente));
            this.txtNomeBusca = new System.Windows.Forms.TextBox();
            this.chkListarTodos = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEmitir = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNomeBusca
            // 
            this.txtNomeBusca.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeBusca.Location = new System.Drawing.Point(40, 106);
            this.txtNomeBusca.Name = "txtNomeBusca";
            this.txtNomeBusca.Size = new System.Drawing.Size(513, 26);
            this.txtNomeBusca.TabIndex = 0;
            this.txtNomeBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeBusca_KeyPress);
            // 
            // chkListarTodos
            // 
            this.chkListarTodos.AutoSize = true;
            this.chkListarTodos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkListarTodos.Location = new System.Drawing.Point(40, 39);
            this.chkListarTodos.Name = "chkListarTodos";
            this.chkListarTodos.Size = new System.Drawing.Size(102, 20);
            this.chkListarTodos.TabIndex = 2;
            this.chkListarTodos.Text = "Listar Todos";
            this.chkListarTodos.UseVisualStyleBackColor = true;
            this.chkListarTodos.CheckedChanged += new System.EventHandler(this.chkListarTodos_CheckedChanged);
            this.chkListarTodos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkListarTodos_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Buscar por Nome:";
            // 
            // btnEmitir
            // 
            this.btnEmitir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmitir.Image = ((System.Drawing.Image)(resources.GetObject("btnEmitir.Image")));
            this.btnEmitir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEmitir.Location = new System.Drawing.Point(156, 183);
            this.btnEmitir.Name = "btnEmitir";
            this.btnEmitir.Size = new System.Drawing.Size(142, 38);
            this.btnEmitir.TabIndex = 49;
            this.btnEmitir.Text = "Emitir Relatório";
            this.btnEmitir.UseVisualStyleBackColor = true;
            this.btnEmitir.Click += new System.EventHandler(this.btnEmitir_Click);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(304, 183);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(142, 38);
            this.btnSair.TabIndex = 48;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // frmRpConsultaCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 233);
            this.Controls.Add(this.btnEmitir);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkListarTodos);
            this.Controls.Add(this.txtNomeBusca);
            this.KeyPreview = true;
            this.Name = "frmRpConsultaCliente";
            this.Text = "Relatorio de Clientes";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmRpConsultaCliente_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNomeBusca;
        private System.Windows.Forms.CheckBox chkListarTodos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEmitir;
        private System.Windows.Forms.Button btnSair;
    }
}