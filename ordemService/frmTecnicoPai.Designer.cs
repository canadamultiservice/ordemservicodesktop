﻿namespace ordemService
{
    partial class frmTecnicoPai
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTecnicoPai));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNovoTecnico = new System.Windows.Forms.ToolStripButton();
            this.btnEditarTecnico = new System.Windows.Forms.ToolStripButton();
            this.btnExcluirTecnico = new System.Windows.Forms.ToolStripButton();
            this.btnImprimirRelstorioTecnico = new System.Windows.Forms.ToolStripButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNomeTecnicoFiltro = new System.Windows.Forms.TextBox();
            this.dgvRelatorioTecnico = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRelatorioTecnico)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNovoTecnico,
            this.toolStripSeparator1,
            this.btnEditarTecnico,
            this.toolStripSeparator2,
            this.btnExcluirTecnico,
            this.toolStripSeparator3,
            this.btnImprimirRelstorioTecnico});
            this.toolStrip1.Location = new System.Drawing.Point(16, 23);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(344, 70);
            this.toolStrip1.TabIndex = 19;
            this.toolStrip1.Text = "MenuClientes";
            // 
            // btnNovoTecnico
            // 
            this.btnNovoTecnico.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoTecnico.Image")));
            this.btnNovoTecnico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNovoTecnico.Name = "btnNovoTecnico";
            this.btnNovoTecnico.Size = new System.Drawing.Size(84, 67);
            this.btnNovoTecnico.Text = "Novo Técnico";
            this.btnNovoTecnico.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNovoTecnico.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.btnNovoTecnico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNovoTecnico.Click += new System.EventHandler(this.btnNovoTecnico_Click);
            // 
            // btnEditarTecnico
            // 
            this.btnEditarTecnico.Image = ((System.Drawing.Image)(resources.GetObject("btnEditarTecnico.Image")));
            this.btnEditarTecnico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditarTecnico.Name = "btnEditarTecnico";
            this.btnEditarTecnico.Size = new System.Drawing.Size(90, 67);
            this.btnEditarTecnico.Text = "Alterar Técnico";
            this.btnEditarTecnico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnEditarTecnico.Click += new System.EventHandler(this.btnEditarTecnico_Click);
            // 
            // btnExcluirTecnico
            // 
            this.btnExcluirTecnico.Image = ((System.Drawing.Image)(resources.GetObject("btnExcluirTecnico.Image")));
            this.btnExcluirTecnico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExcluirTecnico.Name = "btnExcluirTecnico";
            this.btnExcluirTecnico.Size = new System.Drawing.Size(89, 67);
            this.btnExcluirTecnico.Text = "Excluir Técnico";
            this.btnExcluirTecnico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExcluirTecnico.Click += new System.EventHandler(this.btnExcluirTecnico_Click);
            // 
            // btnImprimirRelstorioTecnico
            // 
            this.btnImprimirRelstorioTecnico.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimirRelstorioTecnico.Image")));
            this.btnImprimirRelstorioTecnico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnImprimirRelstorioTecnico.Name = "btnImprimirRelstorioTecnico";
            this.btnImprimirRelstorioTecnico.Size = new System.Drawing.Size(60, 67);
            this.btnImprimirRelstorioTecnico.Text = "Imprimir ";
            this.btnImprimirRelstorioTecnico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnImprimirRelstorioTecnico.Click += new System.EventHandler(this.btnImprimirRelstorioTecnico_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(390, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "Pesquisar por Nome";
            // 
            // txtNomeTecnicoFiltro
            // 
            this.txtNomeTecnicoFiltro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.txtNomeTecnicoFiltro.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeTecnicoFiltro.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txtNomeTecnicoFiltro.Location = new System.Drawing.Point(393, 46);
            this.txtNomeTecnicoFiltro.Name = "txtNomeTecnicoFiltro";
            this.txtNomeTecnicoFiltro.Size = new System.Drawing.Size(438, 35);
            this.txtNomeTecnicoFiltro.TabIndex = 16;
            this.txtNomeTecnicoFiltro.TextChanged += new System.EventHandler(this.txtNomeTecnicoFiltro_TextChanged);
            // 
            // dgvRelatorioTecnico
            // 
            this.dgvRelatorioTecnico.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRelatorioTecnico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRelatorioTecnico.Location = new System.Drawing.Point(12, 109);
            this.dgvRelatorioTecnico.MultiSelect = false;
            this.dgvRelatorioTecnico.Name = "dgvRelatorioTecnico";
            this.dgvRelatorioTecnico.ReadOnly = true;
            this.dgvRelatorioTecnico.RowHeadersVisible = false;
            this.dgvRelatorioTecnico.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRelatorioTecnico.Size = new System.Drawing.Size(948, 301);
            this.dgvRelatorioTecnico.TabIndex = 14;
            this.dgvRelatorioTecnico.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRelatorioTecnico_CellDoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSair);
            this.groupBox1.Controls.Add(this.txtNomeTecnicoFiltro);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.toolStrip1);
            this.groupBox1.Location = new System.Drawing.Point(12, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(948, 102);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // btnSair
            // 
            this.btnSair.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSair.Location = new System.Drawing.Point(858, 33);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(75, 58);
            this.btnSair.TabIndex = 25;
            this.btnSair.UseVisualStyleBackColor = true;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // frmTecnicoPai
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 431);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvRelatorioTecnico);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTecnicoPai";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TÉCNICO";
            this.Load += new System.EventHandler(this.frmTecnicoPai_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmTecnicoPai_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRelatorioTecnico)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNovoTecnico;
        private System.Windows.Forms.ToolStripButton btnEditarTecnico;
        private System.Windows.Forms.ToolStripButton btnExcluirTecnico;
        private System.Windows.Forms.ToolStripButton btnImprimirRelstorioTecnico;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNomeTecnicoFiltro;
        public System.Windows.Forms.DataGridView dgvRelatorioTecnico;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}