﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmOrcamentoClienteNCadastrado : Form
    {
        public frmOrcamentoClienteNCadastrado()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        public void CarregarGrid()
        {
            ModeloDAO objdao = new ModeloDAO();

            dgvProdutoCadastrado.DataSource = objdao.ListarModelos(textBox1.Text.Trim());

            dgvProdutoCadastrado.Columns["NomeMarca"].HeaderText = "Fabricante/Tipo";
            dgvProdutoCadastrado.Columns["NomeModelo"].HeaderText = "Produto";
            dgvProdutoCadastrado.Columns["CodModelo"].Visible = false;
            dgvProdutoCadastrado.Columns["CodMarca"].Visible = false;
            dgvProdutoCadastrado.Columns["CodCategoria"].Visible = false;


        }

        private void frmOrcamentoClienteNCadastrado_Load(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
