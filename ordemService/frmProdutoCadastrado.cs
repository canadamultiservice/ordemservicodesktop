﻿using DAO;
using DAO.VO;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmProdutoCadastrado : MetroForm
    {
        public frmProdutoCadastrado()
        {
            InitializeComponent();
        }

        private void frmProdutoCadastrado_Load(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        public void CarregarTela()
        {
            CarregarGrid();
        }

        public void CarregarGrid()
        {
            ModeloDAO objdao = new ModeloDAO();

            dgvProdutoCadastrado.DataSource = objdao.ListarModelos(txtPesquisar.Text.Trim());

            dgvProdutoCadastrado.Columns["NomeMarca"].HeaderText = "Fabricante/Tipo";
            dgvProdutoCadastrado.Columns["NomeModelo"].HeaderText = "Produto";
            dgvProdutoCadastrado.Columns["CodModelo"].Visible = false;
            dgvProdutoCadastrado.Columns["CodMarca"].Visible = false;
            dgvProdutoCadastrado.Columns["CodCategoria"].Visible = false;


        }

        private void txtPesquisar_TextChanged_1(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void BtnSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnNovoProduto_Click(object sender, EventArgs e)
        {
            frmCadastroModeloMarca frm = Util.RetornafrmCadastroModeloMarca();
            frm.Activate();
            frm.tipoOrc = 1;
            Util.chamadora(Util.TipoTela.ProdutoCadastrado);
            frm.Show();
        }

        private void dgvProdutoCadastrado_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvProdutoCadastrado.RowCount > 0)
            {
                ModeloVO obj = (ModeloVO)dgvProdutoCadastrado.CurrentRow.DataBoundItem;

                if (Util.tipo == Util.TipoTela.Orcamento)
                {

                    frmOrcamento objform = Util.RetornafrmOrcamento();
                    //objform.Tela = 1;
                    objform.RecebeProduto(obj);
                    objform.Activate();
                    Util.chamadora(Util.TipoTela.Orcamento);
                    objform.Show();
                    this.Hide();




                }
                else
                {

                    frmOrcamento objform = Util.RetornafrmOrcamento();
                    //objform.Tela = 1;
                    objform.RecebeProduto(obj);
                    objform.Activate();
                    Util.chamadora(Util.TipoTela.Orcamento);
                    objform.Show();
                    this.Hide();




                }


            }
        }
    }
}
