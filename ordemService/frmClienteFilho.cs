﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ordemService
{
    public partial class frmClienteFilho : Form
    {
        public frmClienteFilho()
        {
           // Thread t = new Thread(new ThreadStart(SplashScreen));
          //  t.Start();
           // Thread.Sleep(5000);
           // t.Abort();
            InitializeComponent();

        }

      //  public void SplashScreen()
       // {
            //Executar a Tela de Splash
       //     Application.Run(new frmSplash());
      //  }

        #region EventosTela

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                ClienteDAO objdao = new ClienteDAO();

                tb_cliente objCliente = new tb_cliente();



                objCliente.cod_administrador = 1;
                objCliente.data_cliente = dt_data.Value;
                objCliente.nome_cliente = txtNomeCliente.Text;
                objCliente.endereco_cliente = txtEnderecoCliente.Text;
                objCliente.bairro_cliente = txtBairroCliente.Text;
                objCliente.cod_cidade = Convert.ToInt32(cmbcidade.SelectedValue);
                objCliente.celular_cliente = mkdFoneCelularCliente.Text;
                objCliente.telefone_cliente = mkdFoneComercialCliente.Text;
                objCliente.cnpj_cliente = mkdCnpjCliente.Text;
                objCliente.inscricaoEstadual_cliente = mkdInscricaoCliente.Text;
                objCliente.cpf_cliente = mkdCpfCliente.Text;
                objCliente.contato_cliente = txtContatoCliente.Text;
                objCliente.email_cliente = txtEmailCliente.Text;
                objCliente.complemento_cliente = txtComplementoCliente.Text;
                objCliente.observacao_cliente = txtObsCliente.Text;

                try
                {
                    if (txtCodCliente.Text.Trim() == string.Empty)
                    {
                        objdao.InserirCliente(objCliente);
                    }
                    else
                    {
                        objCliente.cod_cliente = Convert.ToInt32(txtCodCliente.Text);
                        objdao.AlterarCliente(objCliente);
                    }


                    LimparCampos();

                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    ChamaTelaPai();

                }


                catch (Exception )
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }




            }

        }

        private void btnPesquisarServiços_Click(object sender, EventArgs e)
        {

        }

        private void frmClienteFilho_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }
        #endregion

        #region MetodosTela

        private bool ValidarCampos()
        {
            bool ret = true;
            string campos = string.Empty;

            if (txtNomeCliente.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += " \n -Inserir nome do Cliente ";
                txtNomeCliente.Focus();
            }
            if (txtEnderecoCliente.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += " \n -Inserir endereço Cliente ";
                txtEnderecoCliente.Focus();
            }
            if (mkdFoneComercialCliente.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += " \n -inserir Telefone Cliente";
                mkdFoneComercialCliente.Focus();
            }
            if (cmbcidade.SelectedIndex < 0)
            {
                ret = false;
                campos += " \n -Escolher a cidade do Cliente";
                cmbcidade.Focus();
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atencao", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return ret;
        }

        private void LimparCampos()
        {
            txtNomeCliente.Clear();
            txtEnderecoCliente.Clear();
            txtEmailCliente.Clear();
            txtContatoCliente.Clear();
            txtComplementoCliente.Clear();
            txtCodCliente.Clear();
            txtBairroCliente.Clear();
            mkdCnpjCliente.Clear();
            mkdCpfCliente.Clear();
            mkdFoneCelularCliente.Clear();
            mkdFoneComercialCliente.Clear();
            mkdInscricaoCliente.Clear();
            cmbcidade.SelectedIndex = -1;
            txtObsCliente.Clear();
        }

        private void CarregarData()
        {
            dt_data.Value = DateTime.Now;
        }

        public void CarregarTela()
        {
            // CarregarCidade(0);
            CarregarData();
        }

        public void CarregarCidade(int cod)
        {

            ClienteDAO objcli = new ClienteDAO();

            cmbcidade.DataSource = objcli.ConsultarCidade();

        }

        public void RecebeInformacao(ClienteVO objvo)
        {
            txtNomeCliente.Text = objvo.Nome;
            txtCodCliente.Text = Convert.ToString(objvo.CodCliente);
            txtEnderecoCliente.Text = objvo.Endereco;
            txtEmailCliente.Text = objvo.Email;
            mkdCpfCliente.Text = objvo.Cpf;
            mkdCnpjCliente.Text = objvo.Cnpj;
            mkdFoneCelularCliente.Text = objvo.Celular;
            mkdFoneComercialCliente.Text = objvo.Telefone;
            txtBairroCliente.Text = objvo.bairro;
            txtComplementoCliente.Text = objvo.Complemento;
            mkdInscricaoCliente.Text = objvo.inscricaoEstadual;
            cmbcidade.SelectedValue = Convert.ToInt32(objvo.cod_cidade);
            // dt_data.Value = Convert.ToDateTime(objvo.DataCadastro);
            txtObsCliente.Text = objvo.Observacao;
            txtContatoCliente.Text = objvo.Contato;





        }

        private void ChamaTelaPai()
        {

            frmClientePai frm = Util.Retornafrmcliente();
            frm.Activate();
            frm.CarregarTela();
            frm.Show();

        }


        #endregion

        private void frmClienteFilho_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}
