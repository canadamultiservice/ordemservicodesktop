﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<OrcamentoVO> listaItens = new List<OrcamentoVO>();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            SalvarDados();
        }

        public void SalvarDados()
        {
            try
            {
                if (gdvItens.Rows.Count > 1)
                {
                  
                    
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        private void gdvItens_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 3)
                {
                    decimal cell1 = Convert.ToDecimal(gdvItens.CurrentRow.Cells[2].Value);
                    decimal cell2 = Convert.ToDecimal(gdvItens.CurrentRow.Cells[3].Value);
                    if (cell1.ToString() != "" && cell2.ToString() != "")
                    {
                        gdvItens.CurrentRow.Cells[4].Value = cell1 * cell2;
                    }
                }
                decimal valorTotal = 0;
                string valor = "";

                if (gdvItens.CurrentRow.Cells[4].Value != null)
                {
                    valor = gdvItens.CurrentRow.Cells[4].Value.ToString();
                    if (!valor.Equals(""))
                    {
                        for (int i = 0; i <= gdvItens.RowCount - 1; i++)
                        {
                            if (gdvItens.Rows[i].Cells[4].Value != null)
                                valorTotal += Convert.ToDecimal(gdvItens.Rows[i].Cells[4].Value);
                        }
                        if (valorTotal == 0)
                        {
                            MessageBox.Show("Nenhum registro encontrado");
                        }
                        txtTotal.Text = valorTotal.ToString("C");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                orcamentoDAO objdao = new orcamentoDAO();

                OrcamentoVO objvo = new OrcamentoVO();

                listaItens = objdao.ListarItensOrcamento(33).ToList();

                gdvItens.DataSource = listaItens;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void formataGridView()
        {
            var grade = gdvItens;
            grade.AutoGenerateColumns = false;
            grade.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            grade.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            //altera a cor das linhas alternadas no grid
            grade.RowsDefaultCellStyle.BackColor = Color.White;
            grade.AlternatingRowsDefaultCellStyle.BackColor = Color.Cyan;
            //altera o nome das colunas
            grade.Columns[0].HeaderText = "Id";
            grade.Columns[1].HeaderText = "Descrição";
            grade.Columns[2].HeaderText = "Quantidade";
            grade.Columns[3].HeaderText = "Preco";
            grade.Columns[4].HeaderText = "Total";
            grade.Columns[0].Width = 70;
            grade.Columns[1].Width = 150;
            //formata as colunas valor, vencimento e pagamento
            grade.Columns[3].DefaultCellStyle.Format = "c";
            grade.Columns[4].DefaultCellStyle.Format = "c";
            //seleciona a linha inteira
            grade.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //não permite seleção de multiplas linhas    
            grade.MultiSelect = false;
            // exibe nulos formatados
            //grade.DefaultCellStyle.NullValue = " - ";
            //permite que o texto maior que célula não seja truncado
            grade.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //define o alinhamamento à direita
            grade.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            grade.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
    }
}
