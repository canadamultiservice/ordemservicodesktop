﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                UsuarioDAO objdao = new UsuarioDAO();
                string cnpj = txtLogin.Text.Trim();
                string senha = txtSenha.Text.Trim();

                tb_administrador objuser = objdao.ValidarLogin(cnpj, senha);

                if (objuser == null)
                {
                    MessageBox.Show("Usuario nao encontrado", "Atencao", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    Usuario.CodigoLogado = objuser.cod_administrador;
                    Usuario.NomeLogado = objuser.nome_administrador;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ValidarCampos()
        {
            bool ret = true;
            string campos = string.Empty;

            if (txtLogin.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n -Login";
            }

            if (txtSenha.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n -Senha";
            }

            if (!ret)
            {
                MessageBox.Show(string.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "atencao", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return ret;
        }
    }
}
