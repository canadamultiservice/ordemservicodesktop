﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ordemService
{
    public static class Util
    {

        public enum TipoTela
        {
            OS,
            clientePai,
            Orcamento,
            ProdutoCadastrado
        }

        public static TipoTela tipo;

        public static void chamadora(TipoTela tipoT)
        {
            tipo = tipoT;
        }



        private static frmCadastroOS frmcadastro = null;

        public  static frmCadastroOS RetornaFormCadastro()
        {
            if(frmcadastro == null)
            {
                frmcadastro = new frmCadastroOS();
            }
            return frmcadastro;
        }


        private static ClienteCadastrado frmcliente = null;

        public static ClienteCadastrado RetornaClienteCad()
        {
            if (frmcliente == null)
            {
                frmcliente = new ClienteCadastrado();
            }
            return frmcliente;
        }


        private static frmClientePai clientecadastrado = null;

        public static frmClientePai Retornafrmcliente()
        {
            if (clientecadastrado == null)
            {
                clientecadastrado = new frmClientePai();
                //clientecadastrado.CarregarTela();
            }
            return clientecadastrado;
        }


        private static frmClienteFilho frmfilhocadastrado = null;

        public static frmClienteFilho Retornafrmclientefilho()
        {
            if (frmfilhocadastrado == null)
            {
                frmfilhocadastrado = new frmClienteFilho();

            }
            return frmfilhocadastrado;
        }


        private static frmTecnicoFilho frmtecnicoFilhocadastrado = null;

        public static frmTecnicoFilho RetornafrmTecnicoFilho()
        {
            if (frmtecnicoFilhocadastrado == null)
            {
                frmtecnicoFilhocadastrado = new frmTecnicoFilho();
            }
            return frmtecnicoFilhocadastrado;
        }


        private static frmTecnicoPai frmtecnicoPaicadastrado = null;

        public static frmTecnicoPai RetornafrmTecnicoPai()
        {
            if (frmtecnicoPaicadastrado == null)
            {
                frmtecnicoPaicadastrado = new frmTecnicoPai();
            }
            return frmtecnicoPaicadastrado;
        }


        private static frmOrcamento frmorcamentocadastro = null;

        public static frmOrcamento RetornafrmOrcamento()
        {
            if (frmorcamentocadastro == null)
            {
                frmorcamentocadastro = new frmOrcamento();
            }
            return frmorcamentocadastro;
        }


        private static frmProdutoCadastrado frmprodutocadastrado = null;

        public static frmProdutoCadastrado RetornafrmProdutoCadastrado()
        {
            if (frmprodutocadastrado == null)
            {
                frmprodutocadastrado = new frmProdutoCadastrado();
            }
            return frmprodutocadastrado;
        }


        private static frmCadastroModeloMarca frmcadastromodelomarca = null;

        public static frmCadastroModeloMarca RetornafrmCadastroModeloMarca()
        {
            if (frmcadastromodelomarca == null)
            {
                frmcadastromodelomarca = new frmCadastroModeloMarca();
            }
            return frmcadastromodelomarca;
        }

    }
}
