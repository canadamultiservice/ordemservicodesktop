﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class TesteEmail : Form
    {
        public TesteEmail()
        {
            InitializeComponent();
        }


        public void EnviarEmail()
        {

        }

        private void btnenviar_Click(object sender, EventArgs e)
        {
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential("teledigitordemservico@gmail.com", "testeordem");
            MailMessage mail = new MailMessage();
            mail.Sender = new System.Net.Mail.MailAddress("goncalves_179@hotmail.com", "teledigitordemservico@gmail.com");
            mail.From = new MailAddress("teledigitordemservico@gmail.com", "TELEDIGIT");
            mail.To.Add(new MailAddress("goncalves_179@hotmail.com", "ORDEM DE SERVIÇO"));
            mail.Subject = "Ordem de Serviço - Teledigit";
            mail.Body = " Mensagem do site:<br/> Nome:  " + txtemail.Text + "<br/> Email : " + txtemail.Text + " <br/> Mensagem : " + txtmsg.Text;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            try
            {
                client.Send(mail);
                MessageBox.Show(MenssagemAmbiente.EmailEnviado, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception )
            {
                MessageBox.Show(MenssagemAmbiente.EmailNaoEnviado, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                mail = null;
            }
        }
    }
}
