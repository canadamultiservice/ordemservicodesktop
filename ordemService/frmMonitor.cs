﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmMonitor : Form
    {
        public frmMonitor()
        {
            InitializeComponent();
        }


        private void frmMonitor_Load(object sender, EventArgs e)
        {
            CarregarTela();

        }

        #region METODOS

       



        public void CarregarGrid()
        {
            StatusDAO objdao = new StatusDAO();

            dgvMonitor.DataSource = objdao.ConsultarStatus();

            dgvMonitor.Update();


            //Esconde colunas
            //grdMarcas.Columns["cod_marca"].Visible = false;


            //Altera Texto das colunas
            dgvMonitor.Columns["CodOs"].HeaderText = "CODIGO DA O.S";
            dgvMonitor.Columns["CodStatus"].HeaderText = "STATUS";
            dgvMonitor.Columns["DataEntrada"].HeaderText = "DATA";
            dgvMonitor.Columns["NomeCliente"].HeaderText = "CLIENTE";
            dgvMonitor.Columns["NomeTecnico"].HeaderText = "TECNICO";
            dgvMonitor.Columns["nomeStatus"].Visible = false;
            dgvMonitor.Columns["DataSaida"].Visible = false;

        }

        public void CarregarTotalFechado()
        {
            StatusDAO objdao = new StatusDAO();

            List<StatusTotalVO> listaretorno = new List<StatusTotalVO>();

            listaretorno = objdao.ConsultarTotalStatus(2);

            string result = listaretorno.Count.ToString();

            txtResultFechado.Text = result;

          //  txtResultAberta.Text = ;

        }

        public void CarregarTotalCancelado()
        {
            StatusDAO objdao = new StatusDAO();

            List<StatusTotalVO> listaretorno = new List<StatusTotalVO>();

            listaretorno = objdao.ConsultarTotalStatus(3);

            string result = listaretorno.Count.ToString();

            txtResultCancelado.Text = result;

            //  txtResultAberta.Text = ;

        }

        public void CarregarTotalAberta()
        {
            StatusDAO objdao = new StatusDAO();

            List<StatusTotalVO> listaretorno = new List<StatusTotalVO>();

            listaretorno = objdao.ConsultarTotalStatus(1);

            string result = listaretorno.Count.ToString();

            txtResultAberta.Text = result;
            txtResultAberta.Enabled = false;
            //  txtResultAberta.Text = ;

        }


        public void CarregarTela()
        {
            CarregarGrid();
            CarregarTotalAberta();
            CarregarTotalCancelado();
            CarregarTotalFechado();
        }

        private void dgvMonitor_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridViewRow r = dgvMonitor.Rows[e.RowIndex];

            if (r.Cells["CodStatus"].Value.ToString() == "ABERTO")
            {
                r.DefaultCellStyle.BackColor = Color.Yellow;
                r.DefaultCellStyle.ForeColor = Color.Black;

            }
          
            if (r.Cells["CodStatus"].Value.ToString() == "FECHADO")
            {
                r.DefaultCellStyle.BackColor = Color.Green;
                r.DefaultCellStyle.ForeColor = Color.White;

            }
            if (r.Cells["CodStatus"].Value.ToString() == "CANCELADO")
            {
                r.DefaultCellStyle.BackColor = Color.Red;
                r.DefaultCellStyle.ForeColor = Color.White;
            }
            if (r.Cells["CodStatus"].Value.ToString() == "REINCIDÊNCIA")
            {
                r.DefaultCellStyle.BackColor = Color.Maroon;
                r.DefaultCellStyle.ForeColor = Color.White;
            }
           


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Byte tamanhoEspaco = 35;

            DateTime data = new DateTime();
            CultureInfo culture = new CultureInfo("pt-BR");
        
            DateTimeFormatInfo dtfi = culture.DateTimeFormat;
            //string semana = dtfi.GetDayName(data.DayOfWeek);
            string SemanaMes = DateTime.Now.ToLongDateString();
            string hora = DateTime.Now.ToLongTimeString();

            var result = string.Concat(SemanaMes.PadRight(tamanhoEspaco,' ') + hora);

            relogioMonitor.Text = result;

            
           
           

        }



        #endregion

        private void timer2_Tick(object sender, EventArgs e)
        {
            CarregarGrid();
            CarregarTotalAberta();
            CarregarTotalCancelado();
            CarregarTotalFechado();
        }

        private void relogioMonitor_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            
        }
    }
}
