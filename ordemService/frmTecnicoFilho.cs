﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmTecnicoFilho : Form
    {
        public frmTecnicoFilho()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                TecnicoDAO objdao = new TecnicoDAO();

                tb_tecnico objtb = new tb_tecnico();

                DateTime data = DateTime.Now;

                objtb.cod_administrador = 1;
                objtb.nome_tecnico = txtNomeTecnico.Text;
                objtb.endereco_tecnico = txtEnderecoTecnico.Text;
                objtb.bairro = txtBairroTecnico.Text;
                objtb.cod_cidade = Convert.ToInt32(cmbCidadeTecFilho.SelectedValue);
                objtb.complemento_tecnico = txtComplementoTecnico.Text;
                objtb.observacao_tecnico = txtObsTecnico.Text;
                objtb.email_tecnico = txtEmailTecnico.Text;
                objtb.data_cadastro = data;
                objtb.celular_tecnico = mkdFoneCelularTecnico.Text;
                objtb.cpf_tecnico = mkdCpfTecnico.Text;


                try
                {
                    if (txtCodTecnico.Text.Trim() == string.Empty)
                    {
                        objdao.InserirTecnico(objtb);
                    }
                    else
                    {
                        objtb.cod_tecnico = Convert.ToInt32(txtCodTecnico.Text.Trim());
                        objdao.AlterarTecnico(objtb);
                    }

                    LimparCampos();

                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    ChamaTelaPai();
                }
                catch (Exception )
                {
                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }
        }

        private void frmTecnicoFilho_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }




        public void RecebeInformacao(TecnicoVO objvo)
        {
            txtCodTecnico.Text = objvo.CodTecnico.ToString();
            txtNomeTecnico.Text = objvo.NomeTecnico;
            txtEnderecoTecnico.Text = objvo.enderecoTecnico;
            txtEmailTecnico.Text = objvo.EmailTecnico;
            txtDataTecnico.Text = objvo.dataCadastro.ToString();
            mkdFoneCelularTecnico.Text = objvo.celularTecnico;
            txtComplementoTecnico.Text = objvo.Complemento;
            txtObsTecnico.Text = objvo.Observacao;
            txtBairroTecnico.Text = objvo.Bairro;
            mkdCpfTecnico.Text = objvo.Cpf;
            mkdFoneCelularTecnico.Text = objvo.celularTecnico;
            cmbCidadeTecFilho.SelectedValue = Convert.ToInt32(objvo.codCidade);



        }

        public bool ValidarCampos()
        {
           bool ret = true;

           string campos = string.Empty;

            if (txtNomeTecnico.Text.Trim() == string.Empty)
            {
                ret = false;

                campos += " \n -digitar Nome do TECNICO";
            }
            if (txtEmailTecnico.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += " \n - digitar E-mail do TECNICO";
            }
            if (mkdFoneCelularTecnico.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += " \n -digitar Celular do TECNICO";
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atencao", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            return ret;
        }

        public void CarregarCidade()
        {
            ClienteDAO objdao = new ClienteDAO();

            cmbCidadeTecFilho.DataSource = objdao.ConsultarCidade().ToList();
        }

        private void CarregarTela()
        {
            //CarregarCidade();
        }

        private void LimparCampos()
        {
            txtBairroTecnico.Clear();
            txtCodTecnico.Clear();
            txtComplementoTecnico.Clear();
            txtDataTecnico.Clear();
            txtEmailTecnico.Clear();
            txtEnderecoTecnico.Clear();
            txtNomeTecnico.Clear();
            txtObsTecnico.Clear();
            mkdCpfTecnico.Clear();
            mkdFoneCelularTecnico.Clear();
        }

        private void ChamaTelaPai()
        {
            frmTecnicoPai frm = Util.RetornafrmTecnicoPai();
            frm.Activate();
            frm.CarregarTela();
            frm.Show();
        }

        private void frmTecnicoFilho_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}
