﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(1);
            //Ser a progrresBar 1 for igual a 100
            if (progressBar1.Value == 100)
            {
                //Pare
                timer1.Stop();
            }
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            this.Opacity = 0;
            for (double cont = 0; cont <= 1; cont += 0.1)
            {
                this.Opacity = cont;
                this.Refresh();
                System.Threading.Thread.Sleep(15);
            }
        }
    }
}
