﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmCadastroModeloMarca : Form
    {
        public frmCadastroModeloMarca()
        {
            InitializeComponent();
        }

       

        public int tipoOrc = 0;

        #region EVENTOS TELA
        private void btnNovo_Click(object sender, EventArgs e)
        {
            CarregarTela();
            btnSalvar.Enabled = true;
            txtNomeModelo.Enabled = true;
            txtPreco.Enabled = true;
            cmbFabricante.Enabled = true;
            cmbCategoria.Enabled = true;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                ModeloDAO objdao = new ModeloDAO();

                tb_modelo objmodelo = new tb_modelo();

                objmodelo.cod_marca = Convert.ToInt32(cmbFabricante.SelectedValue);
                objmodelo.cod_categoria = Convert.ToInt32(cmbCategoria.SelectedValue);
                objmodelo.nome_modelo = txtNomeModelo.Text.Trim();
                objmodelo.preco_modelo = txtPreco.Text.Trim();


                try
                {
                    if (txtCod.Text.Trim() == string.Empty)
                    {
                        objdao.InserirModelo(objmodelo);
                        MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        objmodelo.cod_modelo = Convert.ToInt32(txtCod.Text);
                        objdao.AlterarModelo(objmodelo);
                        MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    CarregarTela();
                    // telaOrcamento();

                }
                catch (Exception)
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                txtPreco_Validated(sender, e);
                ModeloDAO objdao = new ModeloDAO();

                tb_modelo objmodelo = new tb_modelo();

                objmodelo.cod_marca = Convert.ToInt32(cmbFabricante.SelectedValue);
                objmodelo.cod_categoria = Convert.ToInt32(cmbCategoria.SelectedValue);
                objmodelo.nome_modelo = txtNomeModelo.Text.Trim();
                objmodelo.preco_modelo = txtPreco.Text.Trim();

                try
                {
                    if (txtCod.Text.Trim() == string.Empty)
                    {
                        objdao.InserirModelo(objmodelo);
                        MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        objmodelo.cod_modelo = Convert.ToInt32(txtCod.Text);
                        objdao.AlterarModelo(objmodelo);

                        MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }

                    CarregarTela();

                }
                catch (Exception)
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            ModeloDAO objdao = new ModeloDAO();
            try
            {
                objdao.ExcluirModelo(Convert.ToInt32(txtCod.Text));
                CarregarTela();

                MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }


            catch (Exception)
            {

                MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (Util.tipo == Util.TipoTela.ProdutoCadastrado)
            {
                frmProdutoCadastrado frm = Util.RetornafrmProdutoCadastrado();
                frm.CarregarGrid();
                this.Hide();
            }
            else
            {
                this.Close();
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmCadastroMarcaFabricante_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }
        #endregion

        #region METODOS

        public bool validarCampos()
        {
            bool ret = true;
            string campos = string.Empty;

            if (cmbFabricante.SelectedIndex < 0)
            {
                ret = false;
                campos += "\n -Seleçionar Fabricante!!";
            }
            if (txtNomeModelo.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n -Nome do Modelo!!";
            }
            if (txtPreco.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n -Preço do Produto!!";
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return ret;
        }

        public void CarregarTela()
        {
            LimparCampos();
            CarregarGrid();
            CarregarCmbMarca();
            CarregarCmbCategoria();
            HabilitarBotoes(2);
        }

        public void LimparCampos()
        {
            cmbFabricante.SelectedIndex = -1;
            txtNomeModelo.Clear();
            txtCod.Clear();
            txtPreco.Clear();
        }

        public void CarregarGrid()
        {
            ModeloDAO objdao = new ModeloDAO();

            string nome = string.Empty;

            dgvModeloFabricante.DataSource = objdao.ListarModelos(nome);

            dgvModeloFabricante.Columns["codMarca"].Visible = false;
            dgvModeloFabricante.Columns["codModelo"].Visible = false;
            dgvModeloFabricante.Columns["codCategoria"].Visible = false;

            dgvModeloFabricante.Columns["nomeMarca"].HeaderText = "Marca";
            dgvModeloFabricante.Columns["nomeModelo"].HeaderText = "Modelo";

        }

        public void CarregarCmbMarca()
        {
            MarcaDAO objdao = new MarcaDAO();
            cmbFabricante.DataSource = objdao.ListarMarcas();
        }

        public void HabilitarBotoes(int cod)
        {
            if (cod == 1)
            {
                btnSalvar.Enabled = true;
                btnAlterar.Enabled = true;
                btnExcluir.Enabled = true;
                btnNovo.Enabled = true;

            }
            else if (cod == 2)
            {
                btnSalvar.Enabled = false;
                btnAlterar.Enabled = false;
                btnExcluir.Enabled = false;
                txtPreco.Enabled = false;
                txtNomeModelo.Enabled = false;
                cmbCategoria.Enabled = false;
                cmbFabricante.Enabled = false;


            }


        }

        private void CarregarCmbCategoria()
        {
            CategoriaDAO objdao = new CategoriaDAO();

            cmbCategoria.DataSource = objdao.ConsultarCategoria();
        }



        #endregion

        private void frmCadastroModeloMarca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        public void txtPreco_Validated(object sender, EventArgs e)
        {
            Decimal valor;
            // verificar se espaço ou vazio
            if ((txtPreco.Text != "") && (txtPreco.Text != " "))
            {
                txtPreco.Text = txtPreco.Text.Replace("R$", "");
                valor = Convert.ToDecimal(txtPreco.Text);
                txtPreco.Text = string.Format("{0}", valor);
            }
        }

        private void dgvModeloFabricante_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvModeloFabricante.RowCount > 0)
            {
                ModeloVO objLinha = (ModeloVO)dgvModeloFabricante.CurrentRow.DataBoundItem;

                txtCod.Text = Convert.ToString(objLinha.codModelo);
                txtNomeModelo.Text = objLinha.NomeModelo;
                cmbFabricante.SelectedValue = objLinha.codMarca;
                cmbCategoria.SelectedValue = Convert.ToInt32(objLinha.codCategoria);
                txtPreco.Text = objLinha.Preco;

                HabilitarBotoes(2);
                btnAlterar.Enabled = true;
                btnExcluir.Enabled = true;
                txtNomeModelo.Enabled = true;
                cmbFabricante.Enabled = true;
                cmbCategoria.Enabled = true;
                txtPreco.Enabled = true;


            }
        }



    }
}
