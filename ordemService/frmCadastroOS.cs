﻿using DAO;
using DAO.VO;
using JamaaTech.Smpp.Net.Client;
using JamaaTech.Smpp.Net.Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmCadastroOS : Form
    {
        public frmCadastroOS()
        {
            InitializeComponent();
        }

        //variavel global

        public int Tela = 0;

        #region ButtonsTela

        private void Salvar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos(1))
            {
                OrdemServicoDAO objdaoService = new OrdemServicoDAO();
                StatusDAO objdaoStatus = new StatusDAO();
                ClienteDAO objdaoCliente = new ClienteDAO();
                TipoServicoDAO objdaoTipoServico = new TipoServicoDAO();
                TecnicoDAO objtecnico = new TecnicoDAO();
                smsVO objSms = new smsVO();

                tb_os objos = new tb_os();
                tb_status_os objstatus = new tb_status_os();
                tb_cliente objcliente = new tb_cliente();
                tb_tipo_servico objServico = new tb_tipo_servico();

                if (txtCodCliente.Text.Trim() == string.Empty)
                {
                    objcliente.cod_administrador = 1;
                    objcliente.nome_cliente = txtNomeCliente.Text;
                    objcliente.endereco_cliente = txtEnderecoCliente.Text;
                    objcliente.email_cliente = txtEmailCliente.Text;
                    objcliente.cod_cidade = Convert.ToInt32(cmbCidade.SelectedValue);
                    objcliente.bairro_cliente = txtBairroCliente.Text;
                    objcliente.celular_cliente = mkCelularCliente.ToString();
                    objcliente.telefone_cliente = mkTelefoneComercial.ToString();
                    objcliente.cnpj_cliente = mkdCnpjCliente.Text.ToString();
                    objcliente.cpf_cliente = mkdCpfCliente.Text.ToString();
                    objcliente.contato_cliente = txtContatoCliente.Text;
                    objcliente.complemento_cliente = txtComplementoCliente.Text;
                    objcliente.inscricaoEstadual_cliente = mkdInscricaoCliente.ToString();


                }
                else
                {
                    //INSERE NA TB_OS
                    objos.cod_administrador = 1;
                    objos.cod_cliente = Convert.ToInt32(txtCodCliente.Text);
                    objos.cod_tecnico = Convert.ToInt32(cmbTecnico.SelectedValue);
                    objos.descricao_os = txtDefeitoReclamado.Text;
                    objos.data_os = dtData.Value;
                    objos.cod_modelo = Convert.ToInt32(cmbModelo.SelectedValue);

                    //INSERE NA TB_STATUS_OS
                    objstatus.cod_status = Convert.ToInt32(cmbStatus.SelectedValue);
                    objstatus.cod_os = objos.cod_os;
                    objstatus.data = dtData.Value;

                    //INSERE NA TB_TIPO_SERVICO
                    objServico.cod_os = objos.cod_os;
                    objServico.cod_servico = Convert.ToInt32(cmbTipoServico.SelectedValue);
                    objServico.cod_modelo = Convert.ToInt32(cmbModelo.SelectedValue);
                    objServico.data = dtData.Value;
                    //CONSULTA O CELULAR DO TECNICO PARA ENVIO DE SMS
                    tb_tecnico consulta = objtecnico.buscarTecnico(Convert.ToInt32(cmbTecnico.SelectedValue));

                    objSms.numeroCelular = consulta.celular_tecnico.ToString();
                    objSms.nomeTecnico = consulta.nome_tecnico.ToString();

                }


                try
                {
                    if (txtCodCliente.Text.Trim() == string.Empty)
                    {

                        //INSERE NOVO CLIENTE NA TB_CLIENTE
                        objdaoCliente.InserirCliente(objcliente);

                        //INSERE O.S NA TB_OS
                        objos.cod_administrador = 1;
                        int codCliente = objcliente.cod_cliente;
                        objos.cod_cliente = codCliente;
                        objos.cod_modelo = Convert.ToInt32(cmbModelo.SelectedValue);
                        objos.cod_tecnico = Convert.ToInt32(cmbTecnico.SelectedValue);
                        objos.descricao_os = txtDefeitoReclamado.Text;
                        objos.data_os = dtData.Value;

                        objdaoService.inserirOrdemServico(objos);

                        //INSERE STATUS NA TB_STATUS_OS
                        int codOs = objos.cod_os;
                        objstatus.cod_os = codOs;

                        objstatus.cod_status = Convert.ToInt32(cmbStatus.SelectedValue);
                        objstatus.cod_os = codOs;
                        objstatus.data = dtData.Value;

                        objdaoStatus.InserirStatus(objstatus);

                        //INSERE NA TB_TIPO_SERVICO
                        objServico.cod_os = codOs;
                        objServico.cod_servico = Convert.ToInt32(cmbTipoServico.SelectedValue);
                        objServico.cod_modelo = Convert.ToInt32(cmbModelo.SelectedValue);
                        objServico.data = dtData.Value;

                        objdaoTipoServico.InserirTipoServico(objServico);

                        //ENVIA E-EMAIL
                        EnviarEmail(codOs, MenssagemEmail.osAberta, MenssagemEmail.cabAberto);

                       // EnviarSMS(objSms.numeroCelular, msgSMS.tecOsAberta);

                    }
                    else
                    {


                        objdaoService.inserirOrdemServico(objos);

                        int codOs = objos.cod_os;

                        objstatus.cod_os = codOs;
                        objdaoStatus.InserirStatus(objstatus);

                        objServico.cod_os = codOs;
                        objdaoTipoServico.InserirTipoServico(objServico);

                        EnviarEmail(codOs, MenssagemEmail.osAberta, MenssagemEmail.cabAberto);

                        
                            

                    }
                    LimparCampo();
                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
                catch (Exception)
                {
                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private void Imprimir_Click(object sender, EventArgs e)
        {
            //instancia a DAO
            OrdemServicoDAO objdao = new OrdemServicoDAO();

            //cria uma LIst de consulta pra armazenar os valores na VO
            List<OrdemServicoVO> ListaRetorno = new List<OrdemServicoVO>();

            //converte
            int tipo = 0;
            DateTime dtinicio = dtDataInicial.Value.AddDays(-1);
            DateTime dtfim = dtDataFinal.Value;



            ListaRetorno = objdao.ConsultarOS(Convert.ToInt32(txtCodOrdemServico.Text.Trim()), tipo, dtinicio, dtfim);

            FormsReports.frmImprimirOS frm = new FormsReports.frmImprimirOS();
            frm.RecebeResultado(ListaRetorno);
            frm.ShowDialog();



        }

        private void Finalizar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos(4))
            {
                OrdemServicoDAO objordem = new OrdemServicoDAO();
                StatusDAO objstatus = new StatusDAO();

                tb_os objos = new tb_os();
                tb_status_os objsts = new tb_status_os();

                objos.cod_os = Convert.ToInt32(txtCodOrdemServico.Text.Trim());
                objos.servicoRealizado_os = txtServicoRealizado.Text;

                objsts.cod_os = Convert.ToInt32(txtCodOrdemServico.Text.Trim());
                objsts.cod_status = Convert.ToInt32(cmbStatus.SelectedValue);
                objsts.data = this.dtData.Value;

                try
                {
                    objordem.AlterarOrdemServiço(objos);

                    objstatus.AlterarStatusOS(objsts);

                    EnviarEmail(objos.cod_os, MenssagemEmail.osFechada, MenssagemEmail.cabFechado);

                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                catch (Exception)
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ClienteCadastrado objform = Util.RetornaClienteCad();
            objform.Activate();
            Util.chamadora(Util.TipoTela.OS);
            objform.Show();

        }

        private void Sair_Click(object sender, EventArgs e)
        {
            this.Hide();
            LimparCampo();
            HabilitaDesabilitaButtons(0);


        }

        private void NovaOS_Click(object sender, EventArgs e)
        {
            tabControlOS.SelectedTab = tabPage1;
            CarregarTela();


        }

        private void frmCadastroOS_Load(object sender, EventArgs e)
        {
            CarregarTela();
            HabilitaDesabilitaButtons(0);

        }

        //GRID BUSCA CLIENTE TABCONTROL
        private void dgvConsultarOs_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvConsultarOs.RowCount > 0)
            {
                OrdemServicoVO objlinha = (OrdemServicoVO)dgvConsultarOs.CurrentRow.DataBoundItem;
                //tb_cliente objlinha3 = (tb_cliente)dgvConsultarOs.CurrentRow.DataBoundItem;
                //tb_status_os objlinha2 = (tb_status_os)dgvConsultarOs.CurrentRow.DataBoundItem;
                txtCodOrdemServico.Text = objlinha.CodOS;
                txtCodCliente.Text = objlinha.CodCliente;
                txtNomeCliente.Text = objlinha.NomeCliente;
                cmbStatus.SelectedValue = objlinha.StatusOS;
                cmbTecnico.SelectedValue = objlinha.CodTecnico;
                txtDefeitoReclamado.Text = objlinha.DescricaoOS;
                txtServicoRealizado.Text = objlinha.servicoRealizado;
                cmbCidade.SelectedValue = objlinha.CodCidade;

                txtEmailCliente.Text = objlinha.EmailCliente;
                txtEnderecoCliente.Text = objlinha.EnderecoCliente;
                txtBairroCliente.Text = objlinha.BairroCliente;
                txtContatoCliente.Text = objlinha.ContatoCliente;
                mkdCnpjCliente.Text = objlinha.CnpjCliente;
                mkdCpfCliente.Text = objlinha.CpfCliente;
                mkCelularCliente.Text = objlinha.CelularCliente;
                mkTelefoneComercial.Text = objlinha.TelefoneCliente;




                tabControlOS.SelectedTab = tabPage1;

                HabilitaDesabilitaButtons(0);
                cmbStatus.Enabled = true;
                txtServicoRealizado.Enabled = true;
                Finalizar.Enabled = true;
                Imprimir.Enabled = true;

                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = "**STATUS";





            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            if (tabControlFiltro.SelectedTab == tabPage3 && ValidarCampos(3) == true)
            {

                CarregarGrid();

            }
            else if (tabControlFiltro.SelectedTab == tabPage4 && ValidarCampos(2) == true)
            {

                CarregarGrid();

            }

        }

        private void cmbMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMarca.SelectedIndex > -1)
            {
                int cod = Convert.ToInt32(cmbMarca.SelectedValue);
                CarregarModelo(cod);
            }
        }


        #endregion


        #region METODOS TELA

        private void HabilitaDesabilitaButtons(int cod)
        {
            if (cod == 0)
            {
                //BUTTONN
                Imprimir.Enabled = false;
                Salvar.Enabled = false;
                Finalizar.Enabled = false;

                //TEXTBOX
                txtBairroCliente.Enabled = false;
                txtAutorizado.Enabled = false;
                txtComplementoCliente.Enabled = false;
                txtContatoCliente.Enabled = false;
                txtEmailCliente.Enabled = false;
                txtEnderecoCliente.Enabled = false;
                txtNomeCliente.Enabled = false;
                txtDefeitoReclamado.Enabled = false;
                txtServicoRealizado.Enabled = false;
                dtData.Enabled = false;
                //MASKEDTEXTBOx
                mkdCnpjCliente.Enabled = false;
                mkdCpfCliente.Enabled = false;
                mkCelularCliente.Enabled = false;
                mkTelefoneComercial.Enabled = false;
                mkdInscricaoCliente.Enabled = false;

                //COMBOBOX
                cmbCidade.Enabled = false;
                cmbTecnico.Enabled = false;
                btnConsultar.Enabled = false;

                cmbStatus.Enabled = false;
                cmbStatus.Focus();

                cmbMarca.Enabled = false;
                cmbModelo.Enabled = false;
                cmbTipoServico.Enabled = false;
            }
            else if (cod == 1)
            {
                Imprimir.Enabled = false;
                Salvar.Enabled = true;
                Finalizar.Enabled = false;

                txtBairroCliente.Enabled = true;
                txtAutorizado.Enabled = true;
                txtComplementoCliente.Enabled = true;
                txtContatoCliente.Enabled = true;
                txtEmailCliente.Enabled = true;
                txtEnderecoCliente.Enabled = true;
                txtNomeCliente.Enabled = true;
                txtServicoRealizado.Enabled = true;
                txtDefeitoReclamado.Enabled = true;


                mkdCnpjCliente.Enabled = true;
                mkdCpfCliente.Enabled = true;
                mkCelularCliente.Enabled = true;
                mkTelefoneComercial.Enabled = true;
                mkdInscricaoCliente.Enabled = true;


                cmbStatus.Enabled = true;
                cmbCidade.Enabled = true;
                cmbTecnico.Enabled = true;
                cmbTipoServico.Enabled = true;
                cmbMarca.Enabled = true;
                cmbModelo.Enabled = true;

                btnConsultar.Enabled = true;

                dtDataFinal.Enabled = false;
                dtDataInicial.Enabled = false;

                dtData.Enabled = true;
            }
        }


        private bool ValidarCampos(int parteValidar)
        {


            bool ret = true;

            string campos = string.Empty;

            if (parteValidar == 1)
            {

                if (txtNomeCliente.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -Nome do Cliente";
                }

                if (txtEnderecoCliente.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -Endereço do Cliente";
                }
                if (txtEmailCliente.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -E-mail do Cliente";
                }
                if (mkTelefoneComercial.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -Telefone comercial do Cliente";
                }
                if (mkCelularCliente.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -Celular do Cliente";
                }
                if (txtBairroCliente.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -Bairro do Cliente";
                }
                if (cmbStatus.SelectedIndex < 0)
                {
                    ret = false;
                    campos += " \n -Status da Ordem de Serviço";
                }
                if (cmbTecnico.SelectedIndex < 0)
                {
                    ret = false;
                    campos += " \n -Selecionar Técnico para atendimento";
                }
                if (cmbCidade.SelectedIndex < 0)
                {
                    ret = false;
                    campos += " \n -Selecionar Cidade para atendimento";
                }
                if (txtDefeitoReclamado.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -Defeito Reclamado";
                }
            }
            else if (parteValidar == 2)
            {
                if (txtCodOS.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += " \n -inserir Codigo Da Ordem de serviço";
                }

            }
            else if (parteValidar == 3)
            {

                if (cmbFiltroStatus.SelectedIndex == -1)
                {
                    ret = false;
                    campos += " \n -Selecionar status para Pesquisa";
                }
            }
            else if (parteValidar == 4)
            {
                if (cmbStatus.SelectedIndex == -1)
                {
                    ret = false;
                    campos += " \n -Selecionar algun Status para Encerramento do Chamado";
                }
                if (txtServicoRealizado.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -'Serviço realizado' não preenchido ";
                }
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return ret;

        }

        private void CarregarCidade()
        {
            ClienteDAO objCliente = new ClienteDAO();

            cmbCidade.DataSource = objCliente.ConsultarCidade();
        }

        private void CarregarTecnico()
        {
            TecnicoDAO objTecnico = new TecnicoDAO();

            cmbTecnico.DataSource = objTecnico.ListarTecnico();

        }

        //CARREGA A COMBOBOS STATUS
        private void CarregarStatus()
        {
            StatusDAO objStatus = new StatusDAO();

            cmbStatus.DataSource = objStatus.CosnultarStatus();

            cmbFiltroStatus.DataSource = objStatus.CosnultarStatus();

            cmbFiltroStatus.SelectedIndex = -1;

        }

        private void LimparCampo()
        {
            txtCodCliente.Clear();
            txtAutorizado.Clear();
            txtBairroCliente.Clear();
            txtComplementoCliente.Clear();
            txtContatoCliente.Clear();
            txtDefeitoReclamado.Clear();
            txtEmailCliente.Clear();
            txtEnderecoCliente.Clear();
            txtNomeCliente.Clear();
            txtServicoRealizado.Clear();
            cmbStatus.SelectedIndex = -1;
            cmbTecnico.SelectedIndex = -1;
            cmbCidade.SelectedIndex = -1;
            cmbModelo.SelectedIndex = -1;
            cmbMarca.SelectedIndex = -1;
            cmbTipoServico.SelectedIndex = -1;
            mkdCnpjCliente.Clear();
            mkdCpfCliente.Clear();
            mkCelularCliente.Clear();
            mkTelefoneComercial.Clear();
            mkdInscricaoCliente.Clear();
            txtCodOS.Clear();


        }

        //CARREGAR TELA
        private void CarregarTela()
        {
            if (Tela == 0)
            {
                LimparCampo();
            }

            CarregarData();
            CarregarCidade();
            CarregarTecnico();
            CarregarStatus();
            HabilitaDesabilitaButtons(1);
            CarregarMarca();
            CarregarTipoServico();
            LimparCampo();

        }


        //RECEBE OS DADOS DA TELA CLIENTE 
        public void RecebeInformacao(ClienteVO obj)
        {
            txtCodCliente.Text = Convert.ToString(obj.CodCliente);
            txtNomeCliente.Text = obj.Nome;
            txtEnderecoCliente.Text = obj.Endereco;
            txtComplementoCliente.Text = obj.Complemento;
            txtContatoCliente.Text = obj.Contato;
            cmbCidade.SelectedValue = Convert.ToInt32(obj.cod_cidade);
            txtEmailCliente.Text = obj.Email;
            txtBairroCliente.Text = obj.bairro;
            mkdCnpjCliente.Text = obj.Cnpj;
            mkdCpfCliente.Text = obj.Cpf;
            mkdInscricaoCliente.Text = obj.inscricaoEstadual;
            mkCelularCliente.Text = obj.Celular;
            mkTelefoneComercial.Text = obj.Telefone;
            //cmbCidade.SelectedValue = obj.CodCidade;


        }

        public void CarregarGrid()
        {


            //instancia a DAO
            OrdemServicoDAO objdao = new OrdemServicoDAO();

            //cria uma LIst de consulta pra armazenar os valores na VO
            List<OrdemServicoVO> ListaRetorno = new List<OrdemServicoVO>();

            //converte
            int tipo = Convert.ToInt32(cmbFiltroStatus.SelectedValue);
            DateTime dtinicio = dtDataInicial.Value.AddDays(-1);
            DateTime dtfim = dtDataFinal.Value.AddDays(+1);

            //limpa a datagridview
            dgvConsultarOs.DataSource = null;

            //pesquisa por CODIGO
            if (cmbFiltroStatus.SelectedIndex == -1)
            {

                ListaRetorno = objdao.ConsultarOS(Convert.ToInt32(txtCodOS.Text.Trim()), 0, dtinicio, dtfim);
            }
            //pesquisa por STATUS
            else
            {

                ListaRetorno = objdao.ConsultarOS(0, tipo, dtinicio, dtfim);
            }

            //preenche gridview com resuldado
            if (ListaRetorno.Count > 0)
            {
                dgvConsultarOs.DataSource = ListaRetorno;
                formataGridView();
                dgvConsultarOs.Columns["CodOS"].HeaderText = "Cod O.S";
                dgvConsultarOs.Columns["CodCliente"].Visible = false;
                dgvConsultarOs.Columns["CidadeCliente"].HeaderText = "Cidade";
                dgvConsultarOs.Columns["DataOS"].HeaderText = "Data";
                dgvConsultarOs.Columns["NomeCliente"].HeaderText = "Cliente";
                dgvConsultarOs.Columns["EnderecoCliente"].HeaderText = "Endereço";
                dgvConsultarOs.Columns["NomeTecnico"].HeaderText = "Tecnico";
                dgvConsultarOs.Columns["DescricaoOS"].HeaderText = "Serviço Reclamado";
                dgvConsultarOs.Columns["ServicoRealizado"].HeaderText = "Serviço Realizado";
                dgvConsultarOs.Columns["Equipamento"].HeaderText = "Equipamento";
                dgvConsultarOs.Columns["StatusOS"].HeaderText = "STATUS";
                dgvConsultarOs.Columns["TipoOS"].Visible = false;
                dgvConsultarOs.Columns["EmailCliente"].Visible = false;
                dgvConsultarOs.Columns["CnpjCliente"].Visible = false;
                dgvConsultarOs.Columns["CpfCliente"].Visible = false;
                dgvConsultarOs.Columns["BairroCliente"].Visible = false;
                dgvConsultarOs.Columns["TelefoneCliente"].Visible = false;
                dgvConsultarOs.Columns["CelularCliente"].Visible = false;
                dgvConsultarOs.Columns["ContatoCliente"].Visible = false;
                dgvConsultarOs.Columns["MarcaEquip"].Visible = false;
                dgvConsultarOs.Columns["CodCidade"].Visible = false;
                dgvConsultarOs.Columns["CodTecnico"].Visible = false;
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";

            }
            else
            {
                MessageBox.Show(MenssagemAmbiente.NaoEncontradoMsg, MenssagemAmbiente.TituloNaoEncontrado, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }

        private void dtData_ValueChanged(object sender, EventArgs e)
        {


        }

        private void tabControlOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            //se a pagina2 da controlTAB estiver aberta desativa o botão SALVAR
            if (tabControlOS.SelectedTab == tabPage2)
            {
                Salvar.Enabled = false;
                Imprimir.Enabled = false;
                dgvConsultarOs.DataSource = null;
            }
            if (tabControlOS.SelectedTab == tabPage1)
            {
                HabilitaDesabilitaButtons(0);
            }
        }

        private void tabControlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            //STATUS


            if (tabControlFiltro.SelectedTab == tabPage3)
            {

                LimparCampo();
                txtCodOS.Enabled = false;
                cmbFiltroStatus.Enabled = true;
                dtDataFinal.Enabled = true;
                dtDataInicial.Enabled = true;

                Salvar.Enabled = false;
                Imprimir.Enabled = false;
                dgvConsultarOs.DataSource = null;

            }
            if (tabControlFiltro.SelectedTab == tabPage4)
            {

                CarregarTela();
                Salvar.Enabled = false;
                Imprimir.Enabled = false;
                dgvConsultarOs.DataSource = null;

            }
        }

        private void cmbFiltroStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //se combobox selecionada desativa txtCODos, senao ativa.
            if (cmbFiltroStatus.SelectedIndex == -1)
            {
                txtCodOS.Enabled = true;

            }
            else
            {
                txtCodOS.Enabled = false;
                LimparCampo();
                dtDataInicial.Enabled = true;
                dtDataFinal.Enabled = true;
            }
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
            // ValidarCampos(3);
        }

        private void EnviarEmail(int codosemail, string msg, string cabecalho)
        {

            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.UseDefaultCredentials = false;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential("canadaaircondicionado@gmail.com", "joaquim24112016");
            MailMessage mail = new MailMessage();
            mail.Sender = new System.Net.Mail.MailAddress(txtEmailCliente.Text, "canadaaircondicionado@gmail.com");
            mail.From = new MailAddress("canadaaircondicionado@gmail.com", "Canada Ar Condicionado");
            mail.To.Add(new MailAddress(txtEmailCliente.Text, "ORDEM DE SERVIÇO"));
            mail.Subject = "Ordem de Serviço - Canada Ar Condicionado";
            mail.Body =
                "</br><b> DADOS DA ORDEM DE SERVIÇO </b> " + cabecalho +
                "</br>" +
                "</br> <b>Código da OS:</b> " + codosemail +
                "</br><b>Cliente:</b> " + txtNomeCliente.Text +
                "</br><b>Endereço:</b> " + txtEnderecoCliente.Text +
                "</br> <b>Telefone 1:</b> " + mkTelefoneComercial.Text +
                "</br> <b>Telefone 2:</b> " + mkCelularCliente.Text +
                "</br> <b>Cidade: </b>" + cmbCidade.Text +
                "<br/> <b>Email: </b>" + txtEmailCliente.Text +
                "</br> <b>Contato :</b>" + txtContatoCliente.Text +
                "</br> <b>Técnico Atendente:</b> " + cmbTecnico.Text +
                "</br> <b>Status:</b> " + cmbStatus.Text +
                "</br> <b>Soliçitação do Cliente:</b> " + txtDefeitoReclamado.Text +
                "</br> <b>Serviço Realizado no Local:</b> " + txtServicoRealizado.Text +
                 "</br>" +
                "</br> <b>" + msg;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;
            try
            {
                client.Send(mail);
                MessageBox.Show(MenssagemAmbiente.EmailEnviado, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception)
            {
                MessageBox.Show(MenssagemAmbiente.EmailNaoEnviado, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                mail = null;
            }
        }

        private void EnviarSMS(string celular, string msg)
        {
            smsDAO dadosCom = new smsDAO();

            tb_sms retorno = dadosCom.ConsultarGW();

            var textMessage = new TextMessage() { DestinationAddress = celular, SourceAddress = "Satélite Telecom", Text = msg };
            SmppClient client = new SmppClient();
            client.Properties.SystemID = retorno.user_sms;
            client.Properties.Password = retorno.senha_sms;
            client.Properties.Port = 7777;
            client.Properties.Host = retorno.ip_sms;
            client.Properties.DefaultEncoding = DataCoding.SMSCDefault;
            client.Properties.AddressNpi = NumberingPlanIndicator.Unknown;
            client.Properties.AddressTon = TypeOfNumber.Unknown;

            try
            {
                client.ForceConnect();
                client.Start();
                client.SendMessage(textMessage);
                client.Shutdown();
                MessageBox.Show(msgSMS.envioSMS, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
            catch (Exception)
            {

                MessageBox.Show(msgSMS.envioSMSErrO, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }



        }


        private void CarregarData()
        {
            dtData.Value = DateTime.Now;
        }

        private void CarregarTipoServico()
        {
            ServicoDAO objdao = new ServicoDAO();

            cmbTipoServico.DataSource = objdao.ListarServico();
        }

        private void CarregarMarca()
        {
            MarcaDAO objdao = new MarcaDAO();

            cmbMarca.DataSource = objdao.ListarMarcas();


        }

        private void CarregarModelo(int codMarca)
        {
            ModeloDAO objdao = new ModeloDAO();
            cmbModelo.DataSource = objdao.ConsultarModelo(codMarca);



        }



        #endregion

        private void frmCadastroOS_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void cmbFiltroStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.CarregarGrid();
            }
        }

        private void dtDataInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.CarregarGrid();
            }
        }

        private void dtDataFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.CarregarGrid();
            }
        }

        private void txtCodOS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                this.CarregarGrid();
            }
        }

        private void formataGridView()
        {
            var grade = dgvConsultarOs;
            grade.AutoGenerateColumns = false;
            grade.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            grade.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            //altera a cor das linhas alternadas no grid
            grade.RowsDefaultCellStyle.BackColor = Color.White;
            grade.AlternatingRowsDefaultCellStyle.BackColor = Color.CadetBlue;
            //altera o nome das colunas

            //formata as colunas valor, vencimento e pagamento
            // grade.Columns[6].DefaultCellStyle.Format = "c";
            //grade.Columns[5].DefaultCellStyle.Format = "c";
            //seleciona a linha inteira

            // exibe nulos formatados
            //grade.DefaultCellStyle.NullValue = " - ";
            //permite que o texto maior que célula não seja truncado
            grade.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //define o alinhamamento à direita
            //grade.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //grade.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        #region FORMATAÇÃO GROUPBOX
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor, Color backgroundColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Coloque a cor do background aqui
                g.Clear(backgroundColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }


        private void groupBox2_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox4_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox8_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox9_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox3_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox1_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox5_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox6_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox7_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }
        #endregion

    }
}

