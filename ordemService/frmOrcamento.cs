﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmOrcamento : Form
    {
        public frmOrcamento()
        {
            InitializeComponent();

            label4.ForeColor = System.Drawing.Color.Blue;
            label5.ForeColor = System.Drawing.Color.Blue;
            label6.ForeColor = System.Drawing.Color.Blue;
            labelSubTotal.ForeColor = System.Drawing.Color.Red;
            labelValorMo.ForeColor = System.Drawing.Color.Blue;

        }

        //lista pra guarda itens pra salvar no banco
        List<OrcamentoVO> listaItens = new List<OrcamentoVO>();
        //lista pra guardar itens quando buscado do banco
        List<OrcamentoVO> ListaNovoItens = new List<OrcamentoVO>();


        #region METODOS TELA
        //PINTAS AS CELULAS DA GRID INTERCALANDO
        private void formataGridView()
        {
            var grade = dgvOrcamento;
            grade.AutoGenerateColumns = false;
            grade.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            grade.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            //altera a cor das linhas alternadas no grid
            grade.RowsDefaultCellStyle.BackColor = Color.White;
            grade.AlternatingRowsDefaultCellStyle.BackColor = Color.Cyan;
            //altera o nome das colunas

            //formata as colunas valor, vencimento e pagamento
            // grade.Columns[6].DefaultCellStyle.Format = "c";
            //grade.Columns[5].DefaultCellStyle.Format = "c";
            //seleciona a linha inteira

            // exibe nulos formatados
            //grade.DefaultCellStyle.NullValue = " - ";
            //permite que o texto maior que célula não seja truncado
            grade.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //define o alinhamamento à direita
            //grade.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //grade.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        private void frmOrcamento_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }
        //CARREGA CIDADE CLIENTE
        public void CarregarCidade()
        {
            ClienteDAO objCliente = new ClienteDAO();

            List<tb_cidades> ListaConsulta = objCliente.ConsultarCidade();

            cmbCidade.DataSource = ListaConsulta;

            cmbCidade.SelectedIndex = -1;

        }
        //IMPRIMI ORCAMENTo
        public void imprimirOrc(List<OrcamentoVO> lst)
        {
            // listaItens.Clear();
            //orcamentoDAO objdao = new orcamentoDAO();

            // List<OrcamentoVO> listaConsulta = new List<OrcamentoVO>();

            //listaConsulta = objdao.ListarItensOrcamento(Convert.ToInt32(txtCodOrcamento.Text.Trim())).ToList();

            FormsReports.frmImprimirOrcamento frm = new FormsReports.frmImprimirOrcamento();
            // frm.RecebeResultado(listaConsulta);
            frm.RecebeResultado(lst);
            frm.ShowDialog();
        }
        //VALIDA CAMPOS
        public bool validarCampos(int tipo)
        {
            bool ret = true;
            string campos = string.Empty;
            if (tipo == 1)
            {
                if (txtQuantidade.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Quantidade do Produto";
                }
                if (txtPreco.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Preço do Produto";
                }

                if (txtNomeCliente.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Inserir Cliente!!";
                }

                if (txtNomeproduto.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Nome do Produto";
                }


            }
            if (tipo == 2)
            {
                if (listaItens.Count <= 0)
                {
                    ret = false;
                    campos += "\n -Adicionar Produto na Lista";
                }
            }
            if (tipo == 3)
            {
                if (listaItens.Exists(x => x.codProduto.Equals(Convert.ToInt32(txtCodProduto.Text))))
                {
                    ret = false;
                    campos += "\n -Produto ja foi inserido na Lista!!";
                    LimparCampos(1);
                }

            }
            if (tipo == 4)
            {
                if (txtBuscarCodOrc.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Digitar Código do Orçamento!!!";
                }
            }
            if (tipo == 5)
            {
                if (txtDesconto.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Digitar Valor para Desconto!!";
                }

            }
            if (tipo == 6)
            {
                if (txtMaoObra.Text.Trim() == string.Empty)
                {
                    ret = false;
                    campos += "\n -Digitar Valor para Mão de Obra!!";
                }
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return ret;


        }
        //VALIDA PRODUTO NA LISTA
        public bool validarListaItens()
        {
            bool ret = true;
            string campos = string.Empty;

            if (listaItens.Exists(x => x.codProduto.Equals(Convert.ToInt32(txtCodProduto.Text))))
            {
                ret = false;
                campos += "\n-Produto ja foi inserido na Lista!!";
            }
            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atencao", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return ret;
        }

        public void CarregarTela()
        {
            CarregarCidade();
            CarregarData();
            LimparCampos(0);
            ResetarGrid();
            carregarBotoes(0);



        }

        public void LimparCampos(int tipo)
        {
            if (tipo == 1)
            {
                txtCodProduto.Clear();
                txtNomeproduto.Clear();
                txtPreco.Clear();
            }

            else
            {

                txtMaoObra.Clear();
                txtCodCliente.Clear();
                txtCodOrcamento.Clear();
                txtNomeCliente.Clear();
                txtCnpjCpf.Clear();
                txtPreco.Clear();
                txtPrecoTotal.Clear();
                txtQuantidade.Clear();
                txtEndereco.Clear();
                mskTelefone.Clear();
                txtCodProduto.Clear();
                txtContato.Clear();
                txtSubTotal.Clear();
                txtNomeproduto.Clear();
                txtEmail.Clear();
                txtDesconto.Clear();
                txtObs.Clear();
                txtBuscarCodOrc.Clear();
                listaItens.Clear();
                ListaNovoItens.Clear();
                dgvOrcamento.DataSource = null;
            }

        }

        private void CarregarData()
        {
            dtData.Value = DateTime.Now;
        }

        private void ResetarGrid()
        {
            dgvOrcamento.DataSource = null;
            // listaItens = null;
        }

        public void carregarBotoes(int tipo)
        {
            if (tipo == 0)
            {
                txtCnpjCpf.Enabled = true;
                txtNomeCliente.Enabled = true;
                txtEndereco.Enabled = true;
                txtEmail.Enabled = true;
                txtContato.Enabled = true;
                cmbCidade.Enabled = true;
                mskTelefone.Enabled = true;
                btnBuscarCliente.Enabled = true;
                btnImprimir.Enabled = false;
                txtDesconto.Enabled = false;
                txtMaoObra.Enabled = false;



            }
            else if (tipo == 1)
            {

                btnImprimir.Enabled = true;

            }
            else if (tipo == 2)
            {
                txtCnpjCpf.Enabled = false;
                txtNomeCliente.Enabled = false;
                txtEndereco.Enabled = false;
                txtEmail.Enabled = false;
                txtContato.Enabled = false;
                cmbCidade.Enabled = false;
                mskTelefone.Enabled = false;

            }


        }

        public void ValidarCampoDinheiro(string objentrada)
        {
            Decimal valor;
            // verificar se espaço ou vazio
            if ((objentrada != "") && (objentrada != " "))
            {
                objentrada = objentrada.Replace("R$", "");
                valor = Convert.ToDecimal(objentrada);
                objentrada = string.Format("{0:C}", valor);
            }

        }

        public void PercorrerGridTotalBusca()
        {
            decimal valorTotal = 0;
            //6 da "col.Cells[6].Valu" é a sexta posiçao da Classe OrcamentoVO
            foreach (DataGridViewRow col in dgvOrcamento.Rows)
            {
                valorTotal = valorTotal + Convert.ToDecimal(col.Cells[6].Value);
            }

            txtSubTotal.Text = string.Format("{0}", valorTotal);
            txtPrecoTotal.Text = string.Format("{0:c}", valorTotal);

            //SE A MAO DE OBRA VIER COM REGISTRO DO BANCO EXECUTA O METODO"MaoObra"
            if (txtMaoObra.Text.Trim() != "0,00")
            {
                MaoObra();
            }

            //SE O DESCONTO VIER COM REGISTRO DO BANCO EXECUTA O METODO"Desconto"
            if (txtDesconto.Text.Trim() != "")
            {
                Desconto();
            }

            if (listaItens.Count > 0 | ListaNovoItens.Count > 0)
            {
                txtDesconto.Enabled = true;
                txtMaoObra.Enabled = true;
            }


        }

        //RECEBE OS DADOS DO CLIENTE
        public void RecebeInformaçao(ClienteVO objvo)
        {
            txtCodCliente.Text = objvo.CodCliente;
            txtCnpjCpf.Text = objvo.Cnpj;
            txtNomeCliente.Text = objvo.Nome;
            txtEndereco.Text = objvo.Endereco;
            mskTelefone.Text = objvo.Telefone;
            txtContato.Text = objvo.Contato;
            txtEmail.Text = objvo.Email;
            cmbCidade.SelectedValue = Convert.ToInt32(objvo.cod_cidade);

        }

        //RECEBE OS DADOS DO PRODUTO
        public void RecebeProduto(ModeloVO objvo)
        {
            txtNomeproduto.Text = objvo.NomeModelo;
            txtPreco.Text = objvo.Preco;
            txtCodProduto.Text = objvo.codModelo.ToString();
        }

        //OCULTA NOME DAS COLUNAS NA DATAGRIDVIEW
        public void ocultaNomeColunaGrid(int tipo)
        {
            if (tipo == 1)
            {
                dgvOrcamento.Columns["preco"].HeaderText = "Preço";
                dgvOrcamento.Columns["quantidade"].HeaderText = "Quantidade";
                dgvOrcamento.Columns["produto"].HeaderText = "Produto";
                dgvOrcamento.Columns["codProduto"].Visible = false;
                dgvOrcamento.Columns["codOrc"].Visible = false;
                dgvOrcamento.Columns["subTotal"].Visible = false;
                dgvOrcamento.Columns["maoObra"].Visible = false;
                dgvOrcamento.Columns["MargenLucro"].Visible = false;
                dgvOrcamento.Columns["Observacao"].Visible = false;
                dgvOrcamento.Columns["Desconto"].Visible = false;
                dgvOrcamento.Columns["nomeCliente"].Visible = false;
                dgvOrcamento.Columns["nomeVendedor"].Visible = false;
                dgvOrcamento.Columns["enderecoCliente"].Visible = false;
                dgvOrcamento.Columns["emailCliente"].Visible = false;
                dgvOrcamento.Columns["cnpjCliente"].Visible = false;
                dgvOrcamento.Columns["telefoneCliente"].Visible = false;
                dgvOrcamento.Columns["cepCliente"].Visible = false;
                dgvOrcamento.Columns["contatoCliente"].Visible = false;
                dgvOrcamento.Columns["dataEntradaCliente"].Visible = false;
                dgvOrcamento.Columns["servicosSolicitados"].Visible = false;
                dgvOrcamento.Columns["Status"].Visible = false;
                dgvOrcamento.Columns["codCliente"].Visible = false;
                dgvOrcamento.Columns["codItens"].Visible = false;
                dgvOrcamento.Columns["codCidade"].Visible = false;
                dgvOrcamento.Columns["nomeCidade"].Visible = false;
                dgvOrcamento.Columns["SubTotalRelat"].Visible = false;
                dgvOrcamento.Columns["TotalRelat"].Visible = false;
                formataGridView();
            }

        }

        public void InserirRegistroListaItens()
        {
            listaItens[0].nomeCliente = txtNomeCliente.Text;
            listaItens[0].enderecocliente = txtEndereco.Text;
            listaItens[0].emailCliente = txtEmail.Text;
            listaItens[0].telefoneCliente = mskTelefone.Text;
            listaItens[0].nomeCidade = cmbCidade.Text;
            listaItens[0].cnpjCliente = txtCnpjCpf.Text;
            listaItens[0].contatoCliente = txtContato.Text;
            listaItens[0].SubTotalRelat = Convert.ToDecimal(txtSubTotal.Text);
            listaItens[0].TotalRelat = Convert.ToDecimal(txtPrecoTotal.Text.Replace("R$", " "));
            listaItens[0].Observacao = txtObs.Text.Trim();
            listaItens[0].dataEntradaCliente = dtData.Value.ToString();
            listaItens[0].codOrc = Convert.ToInt32(txtCodOrcamento.Text.Trim());
        }

        //SALVA ORCAMENTO NO BANCO COM PARAMETRO DO STATUS
        public void GravarOrcamentoStatus(int tipo)
        {
            if (validarCampos(2))
            {


                itensDAO itdao = new itensDAO();
                ModeloDAO moddao = new ModeloDAO();
                orcamentoDAO orcDAO = new orcamentoDAO();
                tb_orcamento tborc = new tb_orcamento();
                tb_cliente tbcli = new tb_cliente();
                ClienteDAO clidao = new ClienteDAO();

                tb_status_orcamento tso = new tb_status_orcamento();
                Status_OrcamentoDAO so = new Status_OrcamentoDAO();

                //SE O USUARIO NAO BUSCAR O CLIENTE "BOTAO BUSCAR".. ADICIONA NOVO CLIENTE
                if (txtCodCliente.Text.Trim() == string.Empty)
                {
                    tbcli.nome_cliente = txtNomeCliente.Text.Trim();
                    tbcli.endereco_cliente = txtEndereco.Text.Trim();
                    tbcli.cnpj_cliente = txtEndereco.Text.Trim();
                    tbcli.email_cliente = txtEmail.Text.Trim();
                    tbcli.cod_cidade = Convert.ToInt32(cmbCidade.SelectedValue);
                    tbcli.telefone_cliente = mskTelefone.Text.Trim();
                    tbcli.contato_cliente = txtContato.Text.Trim();

                    try
                    {
                        clidao.InserirCliente(tbcli);
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.ToString());

                    }

                }
                //SE FOR NOVO CLIENTE BUSCA O CODIGO NA TB_CLIENTE
                if (txtCodCliente.Text.Trim() == string.Empty)
                {
                    int codCli = tbcli.cod_cliente;
                    tborc.cod_cliente = codCli;
                }
                //SENAO PEGA O CODIGO NO TXTCLIENTE
                else
                {
                    tborc.cod_cliente = Convert.ToInt32(txtCodCliente.Text.Trim());
                }
                tborc.data_orcamento = dtData.Value;
                tborc.cod_administrador = 1;
                tborc.obs_orcamento = txtObs.Text;

                //se nao preencher desconto e mao de obra o sistema envia 0 para o banco
                string mo = "0,00";
                string des = "0";
                if (txtMaoObra.Text.Trim() == null)
                {
                    txtMaoObra.Text = mo.ToString(); //"0,00" TEXTBOX RECEBE ESSE VALOR
                    tborc.mo_orcamento = txtMaoObra.Text.Trim(); //ATRIBUI TABELA -> TEXTBOX
                }
                else
                {
                    tborc.mo_orcamento = txtMaoObra.Text.Trim();
                }

                if (txtDesconto.Text.Trim() == string.Empty)
                {
                    txtDesconto.Text = des;
                    tborc.desconto_orcamento = txtDesconto.Text.Trim();
                }
                else
                {
                    tborc.desconto_orcamento = txtDesconto.Text.Trim();
                }

                //tipo de status ABERTO, FECHADO OU CANCELADO
                if (tipo == 1)
                {
                    tso.cod_status = 1;
                }
                else if (tipo == 2)
                {
                    tso.cod_status = 2;
                }
                else
                {
                    tso.cod_status = 3;
                }


                //grava orcamento no banco
                try
                {
                    if (txtCodOrcamento.Text == string.Empty)
                    {
                        for (int i = 0; i < listaItens.Count; i++)
                        {

                            tb_itens tbiten = new tb_itens();
                            //recupera codigo do produto e insere no orcamentoVO
                            // int Produto = Convert.ToInt32(txtCodProduto.Text.Trim());


                            tbiten.qtde_itens = listaItens[i].quantidade;
                            tbiten.cod_modelo = listaItens[i].codProduto;
                            tbiten.cod_orcamento = listaItens[i].codOrc;
                            tbiten.valor_itens = listaItens[i].preco.ToString();

                            tborc.tb_itens.Add(tbiten);

                        }

                        orcDAO.inserirOrcamento(tborc);

                        //insere status FECHADO no orcamento
                        int codorc = tborc.cod_orcamento;
                        txtCodOrcamento.Text = codorc.ToString(); //ATRIBUI O CODIGO DO ORCAMENTO AO TEXTBOX PARA IMPRESSAO;
                        tso.cod_orcamento = codorc;
                        txtCodOrcamento.Text = codorc.ToString();
                        tso.data_status_orc = dtData.Value;


                        so.InserirStatus(tso);


                        MessageBox.Show(String.Concat(Orcamento.finalizarOrcamento), "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        if (DialogResult.Yes == MessageBox.Show("Deseja imprimir Orçamento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                        {
                            InserirRegistroListaItens();
                            dgvOrcamento.DataSource = null;
                            listaItens[0].nomeVendedor = Usuario.NomeLogado;
                            imprimirOrc(listaItens);


                            //MessageBox.Show("Registro apagado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }

                        //listaItens.Clear();
                        CarregarTela();
                    }

                    //SALVA AS ALTERAÇOES NO ORCAMENTO
                    else
                    {
                        if (txtCodOrcamento.Text.Trim() != "" && txtCodCliente.Text.Trim() != "")
                        {
                            int cod = Convert.ToInt32(txtCodOrcamento.Text.Trim());

                            List<OrcamentoVO> consulta = orcDAO.ListarItensOrcamento(cod).ToList();

                            if (listaItens.Count <= 1)
                            {
                                listaItens = ListaNovoItens;
                            }
                            //EXCLUI ITENS
                            for (int i = 0; i < consulta.Count; i++)
                            {

                                tb_itens tbiten = new tb_itens();

                                tbiten.qtde_itens = consulta[i].quantidade;
                                tbiten.cod_modelo = consulta[i].codProduto;
                                tbiten.cod_orcamento = consulta[i].codOrc;
                                tbiten.valor_itens = consulta[i].preco.ToString();

                                tborc.tb_itens.Remove(tbiten);
                                orcDAO.ExcluirItens(cod);
                            }
                            //ADICIONA ITENS EXCLUIDOS + NOVOS ITENS
                            for (int i = 0; i < listaItens.Count; i++)
                            {

                                tb_itens tbiten = new tb_itens();

                                tbiten.qtde_itens = listaItens[i].quantidade;
                                tbiten.cod_modelo = listaItens[i].codProduto;
                                tbiten.cod_orcamento = listaItens[i].codOrc;
                                tbiten.valor_itens = listaItens[i].preco.ToString();

                                //tborc.tb_itens.Add(tbiten);
                                orcDAO.inserirItens(tbiten);

                            }

                            tborc.cod_orcamento = Convert.ToInt32(txtCodOrcamento.Text.Trim());
                            tborc.desconto_orcamento = txtDesconto.Text.Trim();
                            tborc.obs_orcamento = txtObs.Text.Trim();
                            tborc.mo_orcamento = txtMaoObra.Text.Trim();
                            tborc.data_orcamento = dtData.Value;

                            orcDAO.AlterarOrcamento(tborc);




                            MessageBox.Show(String.Concat(Orcamento.finalizarOrcamento), "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (DialogResult.Yes == MessageBox.Show("Deseja imprimir Orçamento?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                            {
                                InserirRegistroListaItens();
                                dgvOrcamento.DataSource = null;
                                imprimirOrc(listaItens);


                                //MessageBox.Show("Registro apagado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }

                        }
                    }

                    CarregarTela();
                }



                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());

                }





            }
        }

        //BUSCA ORCAMENTO CADASTRADO NO BANCO
        public void BuscarOrcamento()
        {
            if (validarCampos(4))
            {
                orcamentoDAO objdao = new orcamentoDAO();

                ListaNovoItens = objdao.ListarItensOrcamento(Convert.ToInt32(txtBuscarCodOrc.Text.Trim())).ToList();

                if (ListaNovoItens.Count >= 1)
                {

                    for (int i = 0; i < ListaNovoItens.Count; i++)
                    {
                        txtNomeCliente.Text = ListaNovoItens[i].nomeCliente;
                        txtEndereco.Text = ListaNovoItens[i].enderecocliente;
                        mskTelefone.Text = ListaNovoItens[i].telefoneCliente;
                        txtEmail.Text = ListaNovoItens[i].emailCliente;
                        txtContato.Text = ListaNovoItens[i].contatoCliente;
                        txtCodOrcamento.Text = ListaNovoItens[i].codOrc.ToString();
                        txtCodCliente.Text = ListaNovoItens[i].codCliente;
                        cmbCidade.SelectedValue = ListaNovoItens[i].codCidade;


                    }
                    //FAZ A MULTIPLICAÇÃO DE QUANTIDADE * PRECO/PRODUTO
                    for (int i = 0; i < ListaNovoItens.Count; i++)
                    {
                        decimal result = 0;

                        result = ListaNovoItens[i].quantidade * ListaNovoItens[i].preco;

                        ListaNovoItens[i].Total = Convert.ToDecimal(result);

                    }
                    txtObs.Text = ListaNovoItens[0].Observacao;
                    txtDesconto.Text = ListaNovoItens[0].Desconto.ToString();
                    txtMaoObra.Text = ListaNovoItens[0].maoObra.ToString();

                    dgvOrcamento.DataSource = ListaNovoItens;
                    ocultaNomeColunaGrid(1);

                    //FAZ O CALCULO DE DESCONTO, MAO DE OBRA E VALOR TOTAL
                    PercorrerGridTotalBusca();

                    btnBuscarCliente.Enabled = false;
                    carregarBotoes(1);

                    listaItens = ListaNovoItens;
                }
                else
                {
                    MessageBox.Show(String.Concat(Orcamento.NaolocalizadoOrc), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    CarregarTela();
                }


            }

        }

        //ADICIONA PRODUTO NA LISTA E MOSTRA NO DATAGRIDVIEW
        public void AdicionarOrcLista()
        {
            if (validarCampos(1))
            {

                //OrcamentoVO objvo = new OrcamentoVO();
                ModeloDAO objdao = new ModeloDAO();
                OrcamentoVO objvo = new OrcamentoVO();
                tb_modelo tbmod = new tb_modelo();

                //adiciona produto novo **AVULSO**
                if (txtCodProduto.Text.Trim() == string.Empty)
                {
                    tbmod.nome_modelo = txtNomeproduto.Text;
                    tbmod.preco_modelo = txtPreco.Text;
                    tbmod.cod_marca = 29;
                    tbmod.cod_categoria = 5;

                    try
                    {
                        if (txtCodProduto.Text.Trim() == string.Empty)
                        {
                            objdao.InserirModelo(tbmod);
                        }
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                    int cod = tbmod.cod_modelo;
                    txtCodProduto.Text = cod.ToString();
                }



                //variavel decimal **TOTAL EM R$**
                decimal result = 0;

                objvo.produto = txtNomeproduto.Text.Trim();
                objvo.quantidade = Convert.ToInt32(txtQuantidade.Text);
                objvo.preco = Convert.ToDecimal(txtPreco.Text.Trim());
                objvo.codProduto = Convert.ToInt32(txtCodProduto.Text);
                if (txtCodOrcamento.Text != "")
                {
                    objvo.codOrc = Convert.ToInt32(txtCodOrcamento.Text.Trim());
                }
                // objvo.codProduto = Convert.ToInt32(cmbProduto.SelectedValue);

                if (objvo.quantidade > 1)
                {
                    result = Convert.ToDecimal(objvo.quantidade) * Convert.ToDecimal(objvo.preco);

                    objvo.Total = result; //string.Format("{0:f}", result);

                }
                else if (objvo.quantidade == 1)
                {

                    result = Convert.ToDecimal(objvo.preco);
                    objvo.Total = result;   //string.Format("{0:f}", result);

                }
                if (ListaNovoItens.Count >= 1)
                {
                    listaItens = ListaNovoItens;
                }

                if (validarCampos(3))
                {
                    listaItens.Add(objvo);

                    LimparCampos(1);
                    carregarBotoes(1);
                    dgvOrcamento.DataSource = null;
                }

                dgvOrcamento.DataSource = listaItens;
                dgvOrcamento.Columns["preco"].HeaderText = "Preço";
                dgvOrcamento.Columns["quantidade"].HeaderText = "Quantidade";
                dgvOrcamento.Columns["produto"].HeaderText = "Produto";

                ocultaNomeColunaGrid(1);
                btnImprimir.Enabled = false;

                PercorrerGridTotalBusca();

            }
        }

        //EXCLUI PRODUTO DA LISTA E DATAGRIDVIEW
        public void ExcluirOrcLista()
        {

            if (listaItens.Count >= 1)
            {
                listaItens.RemoveAt(dgvOrcamento.CurrentRow.Index);
                dgvOrcamento.DataSource = listaItens.ToList();
            }
            else if (ListaNovoItens.Count >= 1)
            {
                try
                {
                    if (dgvOrcamento.CurrentRow.Index != 0)
                    {
                        ListaNovoItens.RemoveAt(dgvOrcamento.CurrentRow.Index);
                    }

                }
                catch (Exception)
                {

                    MessageBox.Show("\n -Selecionar item para Exclusão!!");
                }


                listaItens = ListaNovoItens;
                dgvOrcamento.DataSource = ListaNovoItens.ToList();
            }
            PercorrerGridTotalBusca();
        }

        public double CalcularDesconto(double Desconto, double Valor)
        {

            double percentual = Desconto / 100;

            double valor_final = Valor - (percentual * Valor);

            return valor_final;
        }

        //ADICIONA E DECREMENTA VALOR DA MAO DE OBRA
        public void MaoObra()
        {

            decimal result = -1;
            //se maoObra estiver preenchido
            if (txtMaoObra.Text.Trim() != "")
            {
                if (txtDesconto.Text.Trim() != "0" && txtDesconto.Text.Trim() != "")
                {
                    string mobra = txtMaoObra.Text.Trim();
                    string msb = txtSubTotal.Text.Trim();

                    result = Convert.ToDecimal(mobra) + Convert.ToDecimal(msb);

                    txtPrecoTotal.Text = string.Format("{0:c}", result);
                }
                else
                {
                    string mo = txtMaoObra.Text.Trim();
                    string st = txtPrecoTotal.Text.Trim().Replace("R$", "");

                    result = Convert.ToDecimal(st) + Convert.ToDecimal(mo);

                    txtPrecoTotal.Text = string.Format("{0:c}", result);
                }
              

                if (txtDesconto.Text.Trim() != "0" && txtDesconto.Text.Trim() != "")
                {
                    double valor = Convert.ToDouble(txtPrecoTotal.Text.Replace("R$", "").Trim());

                    double percentual = Convert.ToDouble(txtDesconto.Text.Trim());

                    double valor_final = CalcularDesconto(percentual, valor);

                    txtPrecoTotal.Text = string.Format("{0:c}", valor_final);
                }


            }
            else if(txtMaoObra.Text.Trim() == string.Empty)
            {
                decimal valorDefault = Convert.ToDecimal(txtSubTotal.Text.Trim());
                txtPrecoTotal.Text = string.Format("{0:c}", valorDefault);

                if (txtDesconto.Text.Trim() != "0" && txtDesconto.Text.Trim() != "")
                {
                    double valor = Convert.ToDouble(txtPrecoTotal.Text.Replace("R$", "").Trim());

                    double percentual = Convert.ToDouble(txtDesconto.Text.Trim());

                    double valor_final = CalcularDesconto(percentual, valor);

                    txtPrecoTotal.Text = string.Format("{0:c}", valor_final);
                }
            }
                     

            ResultadoDescMao();
        }

        //ADICIONA E DECREMENTA VALOR DO DESCONTO
        public void Desconto()
        {

            //se o desconto e mao de obra estiverem preenchidos executa esse if
            if (txtDesconto.Text.Trim() != "" && txtMaoObra.Text.Trim() != "")
            {
                double valor = Convert.ToDouble(txtPrecoTotal.Text.Replace("R$", "").Trim());
                double desconto = Convert.ToDouble(txtDesconto.Text.Trim());
                double valorFinal = CalcularDesconto(desconto, valor);

                txtPrecoTotal.Text = string.Format("{0:c}", valorFinal);
            }
            //se preencher somente desconto executa esse if
            else if (txtDesconto.Text.Trim() != "")
            {
                double valor = Convert.ToDouble(txtPrecoTotal.Text.Replace("R$", "").Trim());
                double percentual = Convert.ToDouble(txtDesconto.Text.Trim());
                double valor_final = CalcularDesconto(percentual, valor);

                txtPrecoTotal.Text = string.Format("{0:c}", valor_final);
            }
            else
            {     //apaguei o desconto e mao de obra esta "preenchida"
                if (txtMaoObra.Text.Trim() != "")
                {
                    double valor_final = Convert.ToDouble(txtSubTotal.Text) + Convert.ToDouble(txtMaoObra.Text.Trim());

                    txtPrecoTotal.Text = string.Format("{0:c}", valor_final);
                }
                else
                {   //apaguei o desconto e a mao de obra
                    txtSubTotal.Text = txtSubTotal.Text;
                    double valor_final = Convert.ToDouble(txtSubTotal.Text);

                    txtPrecoTotal.Text = string.Format("{0:c}", valor_final);
                }

            }
            if (txtDesconto.Text.Trim() == string.Empty)
            {
                txtDesconto.Text = "0";
                decimal des = Convert.ToDecimal(txtDesconto.Text);

                if (listaItens.Count > 0)
                {
                    listaItens[0].Desconto = des;
                }
                else if (ListaNovoItens.Count > 0)
                {
                    ListaNovoItens[0].Desconto = des;
                }
            }

            ResultadoDescMao();
        }

        //ADICIONA RESULTADO DO DESCONTO E MAO-DE-OBRA NA LISTA PARA IMPRESSAO
        public void ResultadoDescMao()
        {
            if (listaItens.Count > 0)
            {
                if (txtDesconto.Text.Trim() != "")
                {
                    listaItens[0].Desconto = Convert.ToDecimal(txtDesconto.Text);
                }
                else
                {
                    listaItens[0].Desconto = 0;
                }
                if (txtMaoObra.Text.Trim() != "")
                {
                    listaItens[0].maoObra = Convert.ToDecimal(txtMaoObra.Text.Trim());
                }
                else
                {
                    listaItens[0].maoObra = 0;
                }
                listaItens[0].SubTotalRelat = Convert.ToDecimal(txtSubTotal.Text);
                listaItens[0].TotalRelat = Convert.ToDecimal(txtPrecoTotal.Text.Replace("R$",""));
            }
            else if (ListaNovoItens.Count > 0)
            {
                Decimal nulo = 0;
                if (txtDesconto.Text.Trim() != "")
                {
                    ListaNovoItens[0].Desconto = Convert.ToDecimal(txtDesconto.Text);
                }
                else
                {
                    ListaNovoItens[0].Desconto = nulo;
                }
                if (txtMaoObra.Text.Trim() != "")
                {
                    ListaNovoItens[0].maoObra = Convert.ToDecimal(txtMaoObra.Text.Trim());
                }
                else
                {
                    ListaNovoItens[0].maoObra = 0;
                }
                ListaNovoItens[0].SubTotalRelat = Convert.ToDecimal(txtSubTotal.Text);
                ListaNovoItens[0].TotalRelat = Convert.ToDecimal(txtPrecoTotal.Text.Replace("R$",""));
            }




        }

        #endregion

        //METOTO DO EVENTO GROUPBOX
        #region FORMATACAO GROUP-BOX
        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor, Color backgroundColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Coloque a cor do background aqui
                g.Clear(backgroundColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void groupBox2_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }

        private void groupBox4_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.LightGray, Color.WhiteSmoke);
        }

        //EVENTO PARA MUDAR COR DA GROUPBOX **COR DO TITULO FUNDO E ETC**
        private void groupBox1_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);

        }

        private void groupBox3_Paint(object sender, PaintEventArgs e)
        {
            //* 1-AGR=TITULO 2-ARG=BORDA 3-ARG=FUNDO
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.Red, Color.Black, Color.WhiteSmoke);
        }
        #endregion




        #region BOTOES TELA
        private void btnAdicionarProduto_Click(object sender, EventArgs e)
        {
            frmProdutoCadastrado frm = new frmProdutoCadastrado();
            frm.ShowDialog();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            ClienteCadastrado objform = Util.RetornaClienteCad();
            objform.Activate();
            Util.chamadora(Util.TipoTela.Orcamento);
            objform.Show();
            carregarBotoes(2);
        }

        private void BtnExcluirItemGrid_Click(object sender, EventArgs e)
        {

            ExcluirOrcLista();

        }

        public void btnAdicionar_Click(object sender, EventArgs e)
        {
            AdicionarOrcLista();

        }

        private void NovoOrcamento_Click(object sender, EventArgs e)
        {
            CarregarTela();
        }

        private void SalvarOrcamento_Click(object sender, EventArgs e)
        {

            GravarOrcamentoStatus(1);

        }

        public void FecharOrcamento_Click(object sender, EventArgs e)
        {
            GravarOrcamentoStatus(2);
        }

        private void ImprimirOrcamento_Click(object sender, EventArgs e)
        {
            imprimirOrc(ListaNovoItens);
        }

        private void Sair_Click(object sender, EventArgs e)
        {
            listaItens.Clear();
            this.Close();

        }

        private void btnBuscarOrcamento_Click(object sender, EventArgs e)
        {
            BuscarOrcamento();

        }
        //SE PRESSIONAR ENTER ADICIONA OU DECREMENTA VALOR NO "PRECO-TOTAL"
        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                Desconto();
            }
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {

                e.Handled = true;

            }

        }

        //SE PRESSIONAR ENTER ADICIONA OU DECREMENTA VALOR NO "PRECO-TOTAL"
        private void txtMaoObra_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                MaoObra();
            }
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {

                e.Handled = true;

            }

        }
        //TECLAS DE ATALHO F1,F2,F3 ETC...
        private void frmOrcamento_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    NovoOrcamento_Click(sender, e);

                    break;
                case Keys.F2:
                    SalvarOrcamento_Click(sender, e);

                    break;
                case Keys.F3:
                    FecharOrcamento_Click(sender, e);

                    break;
                case Keys.F4:
                    ImprimirOrcamento_Click(sender, e);

                    break;
                case Keys.F5:

                    break;
                case Keys.F6:
                    btnBuscarCliente_Click(sender, e);
                    break;
            }
        }

        //SÓ ACEITA VALOR "NUMERICO NO TXT-QUANTIDADE"
        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {
                e.Handled = true;
            }
        }
        ////SÓ ACEITA VALOR "NUMERICO NO TXT-PRECO"
        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {

                e.Handled = true;

            }
        }

        private void txtBuscarCodOrc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
            {

                e.Handled = true;

            }
            if (e.KeyChar == 13)
            {
                btnBuscarOrcamento_Click(sender, e);
            }
        }

        #endregion


    }
}

