﻿namespace ordemService
{
    partial class Principal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.técnicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordemDeServiçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fabricanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fabricanteModeloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoOrcamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarOrçamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatoriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.geralToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeOrdemDeServiçoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeOSAtendidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeOSAbertoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatorioDeOSCanceladaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatórioDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aJUSTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurarServidorSMTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurarServidorSMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNovoCliente = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnNovoTecnico = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAdicionarEquip = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnOrcamento = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAbrirOS = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRelatorio = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem,
            this.relatoriosToolStripMenuItem,
            this.aJUSTESToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(998, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clienteToolStripMenuItem,
            this.técnicoToolStripMenuItem,
            this.ordemDeServiçoToolStripMenuItem,
            this.produtosToolStripMenuItem,
            this.orçamentoToolStripMenuItem});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.cadastrosToolStripMenuItem.Text = "CADASTROS";
            // 
            // clienteToolStripMenuItem
            // 
            this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
            this.clienteToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.clienteToolStripMenuItem.Text = "Cliente";
            this.clienteToolStripMenuItem.Click += new System.EventHandler(this.clienteToolStripMenuItem_Click);
            // 
            // técnicoToolStripMenuItem
            // 
            this.técnicoToolStripMenuItem.Name = "técnicoToolStripMenuItem";
            this.técnicoToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.técnicoToolStripMenuItem.Text = "Técnico";
            this.técnicoToolStripMenuItem.Click += new System.EventHandler(this.técnicoToolStripMenuItem_Click);
            // 
            // ordemDeServiçoToolStripMenuItem
            // 
            this.ordemDeServiçoToolStripMenuItem.Name = "ordemDeServiçoToolStripMenuItem";
            this.ordemDeServiçoToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.ordemDeServiçoToolStripMenuItem.Text = "Ordem de Serviço";
            this.ordemDeServiçoToolStripMenuItem.Click += new System.EventHandler(this.ordemDeServiçoToolStripMenuItem_Click);
            // 
            // produtosToolStripMenuItem
            // 
            this.produtosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fabricanteToolStripMenuItem,
            this.fabricanteModeloToolStripMenuItem});
            this.produtosToolStripMenuItem.Name = "produtosToolStripMenuItem";
            this.produtosToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.produtosToolStripMenuItem.Text = "Produto";
            // 
            // fabricanteToolStripMenuItem
            // 
            this.fabricanteToolStripMenuItem.Name = "fabricanteToolStripMenuItem";
            this.fabricanteToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.fabricanteToolStripMenuItem.Text = "Fabricante";
            this.fabricanteToolStripMenuItem.Click += new System.EventHandler(this.fabricanteToolStripMenuItem_Click);
            // 
            // fabricanteModeloToolStripMenuItem
            // 
            this.fabricanteModeloToolStripMenuItem.Name = "fabricanteModeloToolStripMenuItem";
            this.fabricanteModeloToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.fabricanteModeloToolStripMenuItem.Text = "Produto";
            this.fabricanteModeloToolStripMenuItem.Click += new System.EventHandler(this.fabricanteModeloToolStripMenuItem_Click);
            // 
            // orçamentoToolStripMenuItem
            // 
            this.orçamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoOrcamentoToolStripMenuItem,
            this.consultarOrçamentoToolStripMenuItem});
            this.orçamentoToolStripMenuItem.Name = "orçamentoToolStripMenuItem";
            this.orçamentoToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.orçamentoToolStripMenuItem.Text = "Orçamento";
            // 
            // novoOrcamentoToolStripMenuItem
            // 
            this.novoOrcamentoToolStripMenuItem.Name = "novoOrcamentoToolStripMenuItem";
            this.novoOrcamentoToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.novoOrcamentoToolStripMenuItem.Text = "Novo Orçamento";
            this.novoOrcamentoToolStripMenuItem.Click += new System.EventHandler(this.novoOrcamentoToolStripMenuItem_Click);
            // 
            // consultarOrçamentoToolStripMenuItem
            // 
            this.consultarOrçamentoToolStripMenuItem.Name = "consultarOrçamentoToolStripMenuItem";
            this.consultarOrçamentoToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.consultarOrçamentoToolStripMenuItem.Text = "Consultar Orçamento";
            this.consultarOrçamentoToolStripMenuItem.Click += new System.EventHandler(this.consultarOrçamentoToolStripMenuItem_Click);
            // 
            // relatoriosToolStripMenuItem
            // 
            this.relatoriosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clienteToolStripMenuItem1,
            this.relatórioDeOrdemDeServiçoToolStripMenuItem});
            this.relatoriosToolStripMenuItem.Name = "relatoriosToolStripMenuItem";
            this.relatoriosToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.relatoriosToolStripMenuItem.Text = "RELATORIO";
            // 
            // clienteToolStripMenuItem1
            // 
            this.clienteToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.geralToolStripMenuItem});
            this.clienteToolStripMenuItem1.Name = "clienteToolStripMenuItem1";
            this.clienteToolStripMenuItem1.Size = new System.Drawing.Size(234, 22);
            this.clienteToolStripMenuItem1.Text = "Relatório de Clientes";
            // 
            // geralToolStripMenuItem
            // 
            this.geralToolStripMenuItem.Name = "geralToolStripMenuItem";
            this.geralToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.geralToolStripMenuItem.Text = "Relatório de todos Clientes";
            this.geralToolStripMenuItem.Click += new System.EventHandler(this.geralToolStripMenuItem_Click);
            // 
            // relatórioDeOrdemDeServiçoToolStripMenuItem
            // 
            this.relatórioDeOrdemDeServiçoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.relatórioDeOSAtendidaToolStripMenuItem,
            this.relatórioDeOSAbertoToolStripMenuItem,
            this.relatorioDeOSCanceladaToolStripMenuItem,
            this.relatórioDeToolStripMenuItem});
            this.relatórioDeOrdemDeServiçoToolStripMenuItem.Name = "relatórioDeOrdemDeServiçoToolStripMenuItem";
            this.relatórioDeOrdemDeServiçoToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.relatórioDeOrdemDeServiçoToolStripMenuItem.Text = "Relatório de Ordem de Serviço";
            // 
            // relatórioDeOSAtendidaToolStripMenuItem
            // 
            this.relatórioDeOSAtendidaToolStripMenuItem.Name = "relatórioDeOSAtendidaToolStripMenuItem";
            this.relatórioDeOSAtendidaToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.relatórioDeOSAtendidaToolStripMenuItem.Text = "Abertas";
            this.relatórioDeOSAtendidaToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeOSAtendidaToolStripMenuItem_Click);
            // 
            // relatórioDeOSAbertoToolStripMenuItem
            // 
            this.relatórioDeOSAbertoToolStripMenuItem.Name = "relatórioDeOSAbertoToolStripMenuItem";
            this.relatórioDeOSAbertoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.relatórioDeOSAbertoToolStripMenuItem.Text = "Fechadas";
            this.relatórioDeOSAbertoToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeOSAbertoToolStripMenuItem_Click);
            // 
            // relatorioDeOSCanceladaToolStripMenuItem
            // 
            this.relatorioDeOSCanceladaToolStripMenuItem.Name = "relatorioDeOSCanceladaToolStripMenuItem";
            this.relatorioDeOSCanceladaToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.relatorioDeOSCanceladaToolStripMenuItem.Text = "Canceladas";
            this.relatorioDeOSCanceladaToolStripMenuItem.Click += new System.EventHandler(this.relatorioDeOSCanceladaToolStripMenuItem_Click);
            // 
            // relatórioDeToolStripMenuItem
            // 
            this.relatórioDeToolStripMenuItem.Name = "relatórioDeToolStripMenuItem";
            this.relatórioDeToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.relatórioDeToolStripMenuItem.Text = "O.S por Técnico";
            this.relatórioDeToolStripMenuItem.Click += new System.EventHandler(this.relatórioDeToolStripMenuItem_Click);
            // 
            // aJUSTESToolStripMenuItem
            // 
            this.aJUSTESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurarServidorSMTPToolStripMenuItem,
            this.configurarServidorSMSToolStripMenuItem});
            this.aJUSTESToolStripMenuItem.Name = "aJUSTESToolStripMenuItem";
            this.aJUSTESToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.aJUSTESToolStripMenuItem.Text = "FERRAMENTAS";
            // 
            // configurarServidorSMTPToolStripMenuItem
            // 
            this.configurarServidorSMTPToolStripMenuItem.Name = "configurarServidorSMTPToolStripMenuItem";
            this.configurarServidorSMTPToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.configurarServidorSMTPToolStripMenuItem.Text = "Configurar Servidor SMTP";
            this.configurarServidorSMTPToolStripMenuItem.Click += new System.EventHandler(this.configurarServidorSMTPToolStripMenuItem_Click);
            // 
            // configurarServidorSMSToolStripMenuItem
            // 
            this.configurarServidorSMSToolStripMenuItem.Name = "configurarServidorSMSToolStripMenuItem";
            this.configurarServidorSMSToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.configurarServidorSMSToolStripMenuItem.Text = "Configurar Servidor SMS";
            this.configurarServidorSMSToolStripMenuItem.Click += new System.EventHandler(this.configurarServidorSMSToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.sairToolStripMenuItem.Text = "SAIR";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // toolTip
            // 
            this.toolTip.IsBalloon = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNovoCliente,
            this.toolStripSeparator1,
            this.btnNovoTecnico,
            this.toolStripSeparator2,
            this.btnAdicionarEquip,
            this.toolStripSeparator6,
            this.btnOrcamento,
            this.toolStripSeparator5,
            this.btnAbrirOS,
            this.toolStripSeparator3,
            this.btnRelatorio,
            this.toolStripSeparator4,
            this.btnSair});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(998, 68);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNovoCliente
            // 
            this.btnNovoCliente.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovoCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoCliente.Image")));
            this.btnNovoCliente.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNovoCliente.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNovoCliente.Name = "btnNovoCliente";
            this.btnNovoCliente.Size = new System.Drawing.Size(77, 65);
            this.btnNovoCliente.Text = "Novo Cliente";
            this.btnNovoCliente.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNovoCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNovoCliente.Click += new System.EventHandler(this.btnNovoCliente_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 68);
            // 
            // btnNovoTecnico
            // 
            this.btnNovoTecnico.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovoTecnico.Image = ((System.Drawing.Image)(resources.GetObject("btnNovoTecnico.Image")));
            this.btnNovoTecnico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNovoTecnico.Name = "btnNovoTecnico";
            this.btnNovoTecnico.Size = new System.Drawing.Size(78, 65);
            this.btnNovoTecnico.Text = "Novo Técnico";
            this.btnNovoTecnico.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNovoTecnico.Click += new System.EventHandler(this.btnNovoTecnico_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 68);
            // 
            // btnAdicionarEquip
            // 
            this.btnAdicionarEquip.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdicionarEquip.Image = ((System.Drawing.Image)(resources.GetObject("btnAdicionarEquip.Image")));
            this.btnAdicionarEquip.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdicionarEquip.Name = "btnAdicionarEquip";
            this.btnAdicionarEquip.Size = new System.Drawing.Size(80, 65);
            this.btnAdicionarEquip.Text = "Equipamento";
            this.btnAdicionarEquip.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAdicionarEquip.Click += new System.EventHandler(this.btnAdicionarEquip_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 68);
            // 
            // btnOrcamento
            // 
            this.btnOrcamento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrcamento.Image = ((System.Drawing.Image)(resources.GetObject("btnOrcamento.Image")));
            this.btnOrcamento.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnOrcamento.Name = "btnOrcamento";
            this.btnOrcamento.Size = new System.Drawing.Size(68, 65);
            this.btnOrcamento.Text = "Orçamento";
            this.btnOrcamento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnOrcamento.Click += new System.EventHandler(this.btnOrcamento_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 68);
            // 
            // btnAbrirOS
            // 
            this.btnAbrirOS.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbrirOS.Image = ((System.Drawing.Image)(resources.GetObject("btnAbrirOS.Image")));
            this.btnAbrirOS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAbrirOS.Name = "btnAbrirOS";
            this.btnAbrirOS.Size = new System.Drawing.Size(55, 65);
            this.btnAbrirOS.Text = "Nova OS";
            this.btnAbrirOS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnAbrirOS.Click += new System.EventHandler(this.btnAbrirOS_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 68);
            // 
            // btnRelatorio
            // 
            this.btnRelatorio.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorio.Image = ((System.Drawing.Image)(resources.GetObject("btnRelatorio.Image")));
            this.btnRelatorio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRelatorio.Name = "btnRelatorio";
            this.btnRelatorio.Size = new System.Drawing.Size(97, 65);
            this.btnRelatorio.Text = "Relatório Cliente";
            this.btnRelatorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnRelatorio.Click += new System.EventHandler(this.btnRelatorio_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 68);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(52, 65);
            this.btnSair.Text = "Sair";
            this.btnSair.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 452);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(998, 24);
            this.statusStrip1.TabIndex = 6;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(115, 19);
            this.toolStripStatusLabel1.Text = "Usuario Conectado:";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(76, 19);
            this.toolStripStatusLabel2.Text = "Data e Hora:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 92);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(998, 384);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(998, 476);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "Principal";
            this.Text = "CONTROLE DE ORDEM DE SERVIÇO E ORÇAMENTO | CANADA AR CONDICIONADO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Principal_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem técnicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatoriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordemDeServiçoToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNovoCliente;
        private System.Windows.Forms.ToolStripButton btnNovoTecnico;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnAbrirOS;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnRelatorio;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem geralToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeOrdemDeServiçoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeOSAtendidaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeOSAbertoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatorioDeOSCanceladaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatórioDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fabricanteModeloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fabricanteToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnAdicionarEquip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem aJUSTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurarServidorSMTPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurarServidorSMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoOrcamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarOrçamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnOrcamento;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}



