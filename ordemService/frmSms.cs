﻿using DAO;
using DAO.VO;
using JamaaTech.Smpp.Net.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmSms : Form
    {
        public frmSms()
        {
            InitializeComponent();
        }


        public bool validarCampos()
        {
            bool ret = true;
            string campos = string.Empty;

            if (txtIpGateway.Text.Trim() == string.Empty)
            {
                ret = false;

                campos += " \n Digitar IP do Gateway";
            }
            if (txtUserSmpp.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n Digitar Usuario do Servidor SMPP";
            }
            if (txtSenhaSmpp.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n Digitar senha do Servidor SMPP ";
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atencao", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            return ret;
        }



        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                smsDAO objDAO = new smsDAO();

                tb_sms obtb = new tb_sms();

                obtb.ip_sms = txtIpGateway.Text;
                obtb.user_sms = txtUserSmpp.Text;
                obtb.senha_sms = txtSenhaSmpp.Text;

                try
                {
                    if (txtcod_sms.Text.Trim() == string.Empty)
                    {
                        objDAO.inserirGateway(obtb);
                    }
                    else
                    {
                        obtb.cod_sms = Convert.ToInt32(txtcod_sms.Text);
                        objDAO.alterarGateway(obtb);
                    }
                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);


                }
                catch (Exception)
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void CarregarDadosGW()
        {
            smsDAO objdao = new smsDAO();

            tb_sms tb_obj = objdao.ConsultarGW();

            txtcod_sms.Text = tb_obj.cod_sms.ToString();
            txtIpGateway.Text = tb_obj.ip_sms;
            txtUserSmpp.Text = tb_obj.user_sms;
            txtSenhaSmpp.Text = tb_obj.senha_sms;

        }

        private void frmSms_Load(object sender, EventArgs e)
        {
            CarregarDadosGW();
        }
     
      
        private void btnTestarConexao_Click_1(object sender, EventArgs e)
        {
            SmppClient client = new SmppClient();
            SmppConnectionProperties properties = client.Properties;
            properties.SystemID = txtUserSmpp.Text;
            properties.Password = txtSenhaSmpp.Text;
            properties.Port = 7777; //IP port to use
            properties.Host = txtIpGateway.Text; //SMSC host name or IP Address
            properties.SystemType = "mysystemtype";
            properties.DefaultServiceType = "mydefaultservicetype";

            //Resume a lost connection after 30 seconds
            client.AutoReconnectDelay = 3000;

            //Send Enquire Link PDU every 15 seconds
            client.KeepAliveInterval = 15000;
            
            client.Start();

           Thread.Sleep(100);

            if (client.ConnectionState != SmppConnectionState.Connected)
            {
                MessageBox.Show(String.Concat(msgSMS.conexaoFAIL), "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                MessageBox.Show(String.Concat(msgSMS.conexaoOK), "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }



           

        }
    }
}
