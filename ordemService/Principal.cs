﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class Principal : Form
    {


        public Principal()
        {

            InitializeComponent();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmClientePai frmobj = Util.Retornafrmcliente();
            //frmobj.MdiParent = this;
            frmobj.Activate();
            frmobj.ShowDialog();
            //frmobj.CarregarTela();
        }

        private void técnicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTecnicoPai frmobj = Util.RetornafrmTecnicoPai();
            //frmobj.MdiParent = this;
            frmobj.Activate();
            frmobj.ShowDialog();
            frmobj.CarregarTela();

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ordemDeServiçoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastroOS frmobj = Util.RetornaFormCadastro();
            frmobj.Activate();
            frmobj.ShowDialog();



        }

       

        private void btnNovoCliente_Click(object sender, EventArgs e)
        {
            frmClienteFilho frmobj = Util.Retornafrmclientefilho();
            frmobj.CarregarCidade(0);
            //frmobj.MdiParent = this;
            //frmobj.Activate();
            frmobj.Show();

        }

       

        private void btnAbrirOS_Click(object sender, EventArgs e)
        {
            frmCadastroOS frmobj = Util.RetornaFormCadastro();
            //frmobj.MdiParent = this;
            frmobj.Activate();
            frmobj.ShowDialog();

        }

        private void btnRelatorio_Click(object sender, EventArgs e)
        {
            frmRpConsultaCliente frm = new frmRpConsultaCliente();
            frm.ShowDialog();
        }

       

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            IsMdiContainer = true;
            carregarStatusTrip();
        }

        private void geralToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRpConsultaCliente frm = new frmRpConsultaCliente();

            frm.ShowDialog();
        }

        private void dataDeCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void relatórioDeOSAtendidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormsReports.RpConsultaOs frm = new FormsReports.RpConsultaOs();

            frm.ConsultarOrdemServicoAberta();
        }

        private void relatórioDeOSAbertoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormsReports.RpConsultaOs frm = new FormsReports.RpConsultaOs();
            frm.ConsultaOrdemServicoFechada();
        }

        private void relatorioDeOSCanceladaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormsReports.RpConsultaOs frm = new FormsReports.RpConsultaOs();
            frm.ConsultaOrdemServicoCancelado();
        }

        private void relatórioDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRpConsultaTecnicoOs frm = new frmRpConsultaTecnicoOs();
            frm.ShowDialog();
        }

        private void fabricanteModeloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastroModeloMarca frm = new frmCadastroModeloMarca();
            frm.ShowDialog();
        }

        private void fabricanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastroMarca frm = new frmCadastroMarca();
            frm.ShowDialog();
        }

      

        private void btnNovoTecnico_Click(object sender, EventArgs e)
        {
            frmTecnicoPai frmobj = Util.RetornafrmTecnicoPai();
            //frmobj.MdiParent = this;
            frmobj.Activate();
            frmobj.ShowDialog();
            frmobj.CarregarTela();

        }

        private void btnAdicionarEquip_Click(object sender, EventArgs e)
        {
            frmCadastroModeloMarca frm = new frmCadastroModeloMarca();
            frm.ShowDialog();
        }

        public void carregarStatusTrip()
        {
            toolStripStatusLabel1.Text = "Usuario: " + Usuario.NomeLogado;
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = DateTime.Now.ToString();
        }

        private void configurarServidorSMTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void configurarServidorSMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSms objform = new frmSms();
            objform.Show();
        }

        private void gERALToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ClienteCadastrado objfrm = new ClienteCadastrado();
            objfrm.ShowDialog();
        }

        private void ciente2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmClientePai objfrm = new frmClientePai();
            objfrm.ShowDialog();
        }

      
        private void novoOrcamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOrcamento frmobj = Util.RetornafrmOrcamento();
            //frmobj.MdiParent = this;
            frmobj.Activate();
            frmobj.ShowDialog();
        }

        private void consultarOrçamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOrcamentoCadastrado frm = new frmOrcamentoCadastrado();
            frm.ShowDialog();
        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
            frmOrcamento frm = Util.RetornafrmOrcamento();
            frm.Activate();
            frm.ShowDialog();
        }
    }
}
