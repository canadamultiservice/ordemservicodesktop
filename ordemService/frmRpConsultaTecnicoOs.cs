﻿using DAO;
using DAO.VO;
using ordemService.FormsReports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmRpConsultaTecnicoOs : Form
    {
        public frmRpConsultaTecnicoOs()
        {
            InitializeComponent();
        }

        private void CarregarTecnico()
        {
            TecnicoDAO objTecnico = new TecnicoDAO();

            cmbTecnico.DataSource = objTecnico.ListarTecnico();

        }

        private void CarregarTela()
        {
            CarregarTecnico();
        }


        private void btnEmitir_Click(object sender, EventArgs e)
        {
            OrdemServicoDAO dao = new OrdemServicoDAO();

            int codigo = Convert.ToInt32(cmbTecnico.SelectedValue);
            DateTime dtinicio = dtInicial.Value.AddDays(-1);
            DateTime dtfim = dtFinal.Value;



            List<OrdemServicoVO> listaTecnico = dao.ConsultarTecnicoOS(codigo, dtinicio, dtfim);

            FormsReports.frmRelatorioOsTecnico frm = new frmRelatorioOsTecnico ();
            frm.RecebeResultado(listaTecnico);
            frm.ShowDialog();


        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmRpConsultaTecnicoOs_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }

        private void frmRpConsultaTecnicoOs_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}
