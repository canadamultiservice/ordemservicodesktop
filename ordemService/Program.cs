﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

           frmLogin2 frm = new frmLogin2();
           if (frm.ShowDialog() == DialogResult.OK)
           {
                Application.Run(new Principal());
           }

           
        }
    }
}
