﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmCategoria : Form
    {
        public frmCategoria()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                CategoriaDAO objddao = new CategoriaDAO();

                tb_categoria objCategoria = new tb_categoria();

                objCategoria.nome_categoria = txtNomeCategoria.Text;


            }
        }

        private void frmCategoria_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            CarregarTela();
        }


        private bool ValidarCampos()
        {
            bool ret = true;
            string campos = string.Empty;

            if (txtNomeCategoria.Text.Trim() == string.Empty)
            {
                ret = false;
                campos = "\n -Digitar Nome da 'Categoria'";
            }

            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return ret;
        } 


        private void CarregarGrid()
        {
            CategoriaDAO dao = new CategoriaDAO();

            dgvCategoria.DataSource = dao.ConsultarCategoria();
        }

        private void CarregarTela()
        {
            CarregarGrid();
        }

        private void frmCategoria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}
