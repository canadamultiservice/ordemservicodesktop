﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService.FormsReports
{
    public partial class frmImprimirCliente : Form
    {
        private List<ClienteVO> lista;

        public frmImprimirCliente()
        {
            InitializeComponent();
        }

        public void RecebeResultado(List<ClienteVO> lst)
        {
            lista = lst;
            ClienteVOBindingSource.DataSource = lista;
            this.reportViewer1.RefreshReport();


        }

        private void frmImprimirCliente_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
