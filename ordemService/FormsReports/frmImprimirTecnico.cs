﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService.FormsReports
{
    public partial class frmImprimirTecnico : Form
    {
        private List<TecnicoVO> lista;

        public frmImprimirTecnico()
        {
            InitializeComponent();
        }
        public void RecebeResultado(List<TecnicoVO> lst)
        {
            lista = lst;
            TecnicoVOBindingSource.DataSource = lista;
            this.reportViewer1.RefreshReport();


        }

        private void frmImprimirTecnico_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
