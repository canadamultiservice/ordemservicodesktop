﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ordemService.FormsReports
{

    public class RpConsultaOs
    {

        public void ConsultarOrdemServicoAberta()
        {
            DateTime data_inicio = DateTime.Now;
            DateTime data_fim = DateTime.Now;
            int status = 1;
            OrdemServicoDAO dao = new OrdemServicoDAO();


            List<OrdemServicoVO> resultado = dao.ConsultarOS(0, status, Convert.ToDateTime(data_inicio).AddDays(-256), Convert.ToDateTime(data_fim));

            frmRelatorioOsAberta frm = new frmRelatorioOsAberta();
            frm.RecebeResultado(resultado);
            frm.ShowDialog();

        }
        public void ConsultaOrdemServicoFechada()
        {
            DateTime data_inicio = DateTime.Now;
            DateTime data_fim = DateTime.Now;
            int status = 2;
            OrdemServicoDAO dao = new OrdemServicoDAO();


            List<OrdemServicoVO> resultado = dao.ConsultarOS(0, status, Convert.ToDateTime(data_inicio).AddDays(-256), Convert.ToDateTime(data_fim));

            frmRelatorioOsFechada frm = new frmRelatorioOsFechada();
            frm.RecebeResultado(resultado);
            frm.ShowDialog();
        }

        public void ConsultaOrdemServicoCancelado()
        {
            DateTime data_inicio = DateTime.Now;
            DateTime data_fim = DateTime.Now;
            int status = 3;
            OrdemServicoDAO dao = new OrdemServicoDAO();


            List<OrdemServicoVO> resultado = dao.ConsultarOS(0, status, Convert.ToDateTime(data_inicio).AddDays(-256), Convert.ToDateTime(data_fim));

            frmRelatorioOsCancelada frm = new frmRelatorioOsCancelada();
            frm.RecebeResultado(resultado);
            frm.ShowDialog();
        }


    }
}
