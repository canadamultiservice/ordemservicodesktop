﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService.FormsReports
{
    public partial class frmRelatorioOsTecnico : Form
    {
        private List<OrdemServicoVO> lista;

        public frmRelatorioOsTecnico()
        {
            InitializeComponent();
        }

        public void RecebeResultado(List<OrdemServicoVO> lst)
        {
            lista = lst;
           OrdemServicoVOBindingSource.DataSource = lista;
            this.reportViewer1.Refresh();
        }

        private void RelatorioOsTecnico_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
