﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService.FormsReports
{
    public partial class frmImprimirOrcamento : Form
    {
        private List<OrcamentoVO> lista;
        public frmImprimirOrcamento()
        {
            InitializeComponent();
        }

        public void RecebeResultado(List<OrcamentoVO> lst)
        {
            lista = lst;
            OrcamentoVOBindingSource.DataSource = lista;
            this.reportViewer1.RefreshReport();
        }

        private void frmImprimirOrcamento_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
