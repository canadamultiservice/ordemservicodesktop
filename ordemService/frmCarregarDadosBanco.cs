﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmCarregarDadosBanco : Form
    {
        public frmCarregarDadosBanco()
        {
            InitializeComponent();
        }
        private OleDbConnection _olecon;
        private OleDbCommand _oleCmd;

        private static String _Arquivo = @"C:\Users\gonca\Desktop\teste\clientes.xlsx";


        private String _StringConexao = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=YES;ReadOnly=False';", _Arquivo);




        private void button1_Click(object sender, EventArgs e)
        {
            _olecon = new OleDbConnection(_StringConexao);
            _olecon.Open();

            _oleCmd = new OleDbCommand();
            _oleCmd.Connection = _olecon;
            _oleCmd.CommandType = CommandType.Text;

            _oleCmd.CommandText = "SELECT * FROM [cliente$]";
            OleDbDataReader reader = _oleCmd.ExecuteReader();

            banco b = new banco();
            try
            {
                while (reader.Read())
                {
                    if (reader.GetValue(1).ToString() != "")
                    {
                        tb_cliente cliente = new tb_cliente();

                        cliente.nome_cliente = reader.GetValue(0).ToString().Trim();
                        cliente.endereco_cliente = reader.GetValue(1).ToString().Trim();
                        cliente.bairro_cliente = reader.GetValue(2).ToString().Trim();
                        cliente.cod_cidade = Convert.ToInt32(reader.GetValue(3));
                        cliente.cnpj_cliente = reader.GetValue(5).ToString().Trim();
                        cliente.contato_cliente = reader.GetValue(6).ToString().Trim();
                        cliente.telefone_cliente = reader.GetValue(7).ToString().Trim();
                        cliente.email_cliente = reader.GetValue(8).ToString().Trim();


                        new ClienteDAO().InserirCliente(cliente);

                        
                    }

                }
                MessageBox.Show("ok");
            }
            catch (Exception)
            {

                throw;
            }
           

            reader.Close();
        }
    }
}
