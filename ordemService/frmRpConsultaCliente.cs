﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmRpConsultaCliente : Form
    {
        public frmRpConsultaCliente()
        {
            InitializeComponent();
        }

        #region EventosTela
        private void btnEmitir_Click(object sender, EventArgs e)
        {
            ClienteDAO dao = new ClienteDAO();


            List<ClienteVO> listaCliente = dao.ConsultarCliente(txtNomeBusca.Text.Trim());

            frmRelatorioClientes frm = new frmRelatorioClientes();
            frm.RecebeResultado(listaCliente);
            frm.ShowDialog();

        }

        private void chkListarTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (chkListarTodos.Checked)
            {
                txtNomeBusca.Clear();
                txtNomeBusca.Enabled = false;
            }
            else
            {
                txtNomeBusca.Enabled = true;
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region METODOS



        #endregion

        private void frmRpConsultaCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void txtNomeBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnEmitir_Click(sender, e);
            }
        }

        private void chkListarTodos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnEmitir_Click(sender, e);
            }
        }
    }
}
