﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmTecnicoPai : Form
    {
        public frmTecnicoPai()
        {
            InitializeComponent();
        }

        private void btnNovoTecnico_Click(object sender, EventArgs e)
        {
            frmTecnicoFilho frmFilho = new frmTecnicoFilho();
            frmFilho.CarregarCidade();
            frmFilho.Show();

        }

        private void btnEditarTecnico_Click(object sender, EventArgs e)
        {
            if (dgvRelatorioTecnico.RowCount > 0)
            {
                TecnicoVO obj = (TecnicoVO)dgvRelatorioTecnico.CurrentRow.DataBoundItem;


                frmTecnicoFilho objform = new frmTecnicoFilho();
                objform.CarregarCidade();
                objform.RecebeInformacao(obj);
                objform.Activate();
                objform.Show();

            }
        }

        private void btnExcluirTecnico_Click(object sender, EventArgs e)
        {
            if (dgvRelatorioTecnico.RowCount > 0)
            {
                try
                {
                    TecnicoVO obj = (TecnicoVO)dgvRelatorioTecnico.CurrentRow.DataBoundItem;
                    TecnicoDAO objtecnico = new TecnicoDAO();
                    objtecnico.ExcluirTecnico(Convert.ToInt32(obj.CodTecnico));
                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CarregarGrid();
                }
                catch (Exception)
                {
                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }
        }

        private void btnImprimirRelstorioTecnico_Click(object sender, EventArgs e)
        {
            if (dgvRelatorioTecnico.RowCount > 0)
            {
                TecnicoVO obj = (TecnicoVO)dgvRelatorioTecnico.CurrentRow.DataBoundItem;

                TecnicoDAO objdao = new TecnicoDAO();

                string nome = obj.NomeTecnico;

                List<TecnicoVO> listaTecnico = objdao.ConsultaTecnico(nome);

                FormsReports.frmImprimirTecnico frm = new FormsReports.frmImprimirTecnico();
                frm.RecebeResultado(listaTecnico);
                frm.ShowDialog();
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

       
        private void frmTecnicoPai_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }




        private void CarregarGrid()
        {
            TecnicoDAO objdao = new TecnicoDAO();

           

            dgvRelatorioTecnico.DataSource = objdao.ConsultaTecnico(txtNomeTecnicoFiltro.Text.Trim()).ToList();
            dgvRelatorioTecnico.Refresh();

            //dgvRelatorioClientes.Columns["Nome"].HeaderText = "Cliente";
            dgvRelatorioTecnico.Columns["CodTecnico"].Visible = false;
            dgvRelatorioTecnico.Columns["codCidade"].Visible = false;
            dgvRelatorioTecnico.Columns["NomeTecnico"].HeaderText = "Tecnico";
            dgvRelatorioTecnico.Columns["DataCadastro"].HeaderText = "Data Cadastro";
            dgvRelatorioTecnico.Columns["enderecoTecnico"].HeaderText = "Endereco";
            dgvRelatorioTecnico.Columns["EmailTecnico"].HeaderText = "Email";
            dgvRelatorioTecnico.Columns["CelularTecnico"].HeaderText = "Celular";



        }

        public void CarregarTela()
        {
            CarregarGrid();
        }

        private void txtNomeTecnicoFiltro_TextChanged(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void frmTecnicoPai_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }

        private void dgvRelatorioTecnico_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvRelatorioTecnico.RowCount > 0)
            {
                TecnicoVO objvo = (TecnicoVO)dgvRelatorioTecnico.CurrentRow.DataBoundItem;

                frmTecnicoFilho objfrm = new frmTecnicoFilho();
                objfrm.CarregarCidade();
                objfrm.RecebeInformacao(objvo);
                objfrm.Activate();

                objfrm.ShowDialog();
            }
        }
    }
}
