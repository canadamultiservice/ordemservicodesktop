﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmOrcamentoCadastrado : Form
    {
        public frmOrcamentoCadastrado()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            CarregarGrid();

        }

        private void Imprimir_Click(object sender, EventArgs e)
        {

        }

        private void Sair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void CarregarGrid()
        {
            //instancia a DAO
            orcamentoDAO objdao = new orcamentoDAO();

            //cria uma LIst de consulta pra armazenar os valores na VO
            List<OrcamentoVO> ListaRetorno = new List<OrcamentoVO>();

            //converte
            int tipo = Convert.ToInt32(cmbFiltroStatus.SelectedValue);
            DateTime dtinicio = dtDataInicial.Value.AddDays(-1);
            DateTime dtfim = dtDataFinal.Value.AddDays(+1);

            //limpa a datagridview
            dgvRelaOrcamento.DataSource = null;

            //pesquisa por CODIGO
            if (cmbFiltroStatus.SelectedIndex > -1)
            {
                ListaRetorno = objdao.PesquisarOrcamento(0, tipo, dtinicio, dtfim);
               
            }
            //pesquisa por STATUS
            else if (txtCodOrc.Text != "")
            {
                int CodOrc = Convert.ToInt32(txtCodOrc.Text);
                ListaRetorno = objdao.PesquisarOrcamento(CodOrc, 0, dtinicio, dtfim);

            }
            else if (txtCodOrc.Text == string.Empty)
            {
                ListaRetorno = objdao.PesquisarTodosOrcamento();
            }
          

            //preenche gridview com resuldado
            if (ListaRetorno.Count > 0)
            {
                dgvRelaOrcamento.DataSource = ListaRetorno;
                
                dgvRelaOrcamento.Columns["produto"].Visible = false;
                dgvRelaOrcamento.Columns["quantidade"].Visible = false;
                dgvRelaOrcamento.Columns["preco"].Visible = false;
                dgvRelaOrcamento.Columns["total"].Visible = false;
                dgvRelaOrcamento.Columns["codProduto"].Visible = false;
                dgvRelaOrcamento.Columns["subTotal"].Visible = false;
                dgvRelaOrcamento.Columns["maoObra"].Visible = false;
                dgvRelaOrcamento.Columns["Desconto"].Visible = false;
                dgvRelaOrcamento.Columns["MargenLucro"].Visible = false;
                dgvRelaOrcamento.Columns["Observacao"].Visible = false;
                dgvRelaOrcamento.Columns["enderecocliente"].Visible = false;
                dgvRelaOrcamento.Columns["cnpjCliente"].Visible = false;
                dgvRelaOrcamento.Columns["emailCliente"].Visible = false;
                dgvRelaOrcamento.Columns["telefoneCliente"].Visible = false;
                dgvRelaOrcamento.Columns["cepCliente"].Visible = false;
                dgvRelaOrcamento.Columns["contatoCliente"].Visible = false;
                dgvRelaOrcamento.Columns["dataEntradaCliente"].Visible = false;
                dgvRelaOrcamento.Columns["servicosSolicitados"].Visible = false;
                dgvRelaOrcamento.Columns["codCliente"].Visible = false;
                dgvRelaOrcamento.Columns["codItens"].Visible = false;
                dgvRelaOrcamento.Columns["codCidade"].Visible = false;
                dgvRelaOrcamento.Columns["nomeCidade"].Visible = false;
                dgvRelaOrcamento.Columns["TotalRelat"].Visible = false;
                dgvRelaOrcamento.Columns["SubTotalRelat"].Visible = false;

                dgvRelaOrcamento.Columns["CodOrc"].HeaderText = "Código";
                dgvRelaOrcamento.Columns["nomeCliente"].HeaderText = "Cliente";
                dgvRelaOrcamento.Columns["nomeVendedor"].HeaderText = "Vendedor";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";
                //grdResultado.Columns[""].HeaderText = "";

            }
            else
            {
                MessageBox.Show(MenssagemAmbiente.NaoEncontradoMsg, MenssagemAmbiente.TituloNaoEncontrado, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void LimparCampos()
        {
            txtCodOrc.Clear();
            cmbFiltroStatus.SelectedIndex = -1;
        }

        public void CarregarStatus()
        {
            StatusDAO objdao = new StatusDAO();

            cmbFiltroStatus.DataSource = objdao.CosnultarStatus().ToList();

            cmbFiltroStatus.SelectedIndex = -1;
        }

        public void CarregarTela()
        {
            CarregarStatus();
        }

        private void frmOrcamentoCadastrado_Load(object sender, EventArgs e)
        {
            CarregarTela();       }

        private void tabControlFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlFiltro.SelectedTab == tabPage3)
            {

                LimparCampos();

                cmbFiltroStatus.Enabled = true;
                dtDataFinal.Enabled = true;
                dtDataInicial.Enabled = true;
                dgvRelaOrcamento.DataSource = null;

            }
            if (tabControlFiltro.SelectedTab == tabPage4)
            {
                LimparCampos();
                cmbFiltroStatus.Enabled = false;
                dtDataFinal.Enabled = false;
                dtDataInicial.Enabled = false;

            }
        }
    }
}
