﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ordemService
{
    class MenssagemAmbiente
    {
        public static string TituloMsgObrigatorio = "Favor preencher o(s) campo(s) obrigatorio(s): \n";
        public static string ErroMsg = "Ocorreu um erro na Operação";
        public static string SucessoMsg = "Dados gravados com sucesso";
        public static string ExcluidoMsg = "Excluido com Sucesso";
        public static string TituloNaoEncontrado = "Resultado";
        public static string NaoEncontradoMsg = "nao foi encontrado nenhum registro";
        public static string EmailNaoEnviado = "Não foi possivel enviar o E-mail";
        public static string EmailEnviado = "E-mail enviado com Sucesso!";
        public static string Erroitemjaadd = "Produto ja foi inserido!!";
    }

    class MenssagemEmail
    {
        public static string osAberta = "Sua Ordem de Servço foi cadastrada com Sucesso e será Atendida em Breve!! ";
        public static string osFechada = "Sua Ordem de Servço foi Fechada com Sucesso!! ";
        public static string cabFechado = "FECHADA!! ";
        public static string cabAberto = "ABERTA!! ";

    }

    class msgSMS
    {
        public static string conexaoOK = "Teste de Conexão Efetuado Com Sucesso!! ";
        public static string conexaoFAIL = "Teste de Conexão falhou.... ";

        public static string tecOsAberta = " Tem O.S aberta esperando seu atendimento: ";
        public static string envioSMS = "SMS enviado com Sucesso ";
        public static string envioSMSErrO = "Não foi possivel enviar SMS: \n" +
            "\n -gateway nao conectado" +
            "\n -gateway sem Chip " +
            "\n -Chip sem Crédito";

    }
    class Orcamento
    {
        public static string SalvarOrcamento = "Orçamento Cadastrado com Sucesso!!! \n";
        public static string finalizarOrcamento = "Orçamento Fechado com Sucesso!!! \n";
        public static string CancelarOrcamento = "Orçamento Cancelado com Sucesso!!! \n";
        public static string ImprimirOrcamento = "Deseja Imprimir Orçamento? \n";
        public static string NaolocalizadoOrc = "Não foi Possivel localizar Orçamento com esse Código!!";
    }
    
}
