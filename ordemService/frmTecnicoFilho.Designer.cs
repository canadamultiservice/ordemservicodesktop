﻿namespace ordemService
{
    partial class frmTecnicoFilho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTecnicoFilho));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBairroTecnico = new System.Windows.Forms.TextBox();
            this.txtObsTecnico = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtComplementoTecnico = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmailTecnico = new System.Windows.Forms.TextBox();
            this.mkdFoneCelularTecnico = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.mkdCpfTecnico = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbCidadeTecFilho = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEnderecoTecnico = new System.Windows.Forms.TextBox();
            this.txtNomeTecnico = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodTecnico = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDataTecnico = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBairroTecnico);
            this.groupBox1.Controls.Add(this.txtObsTecnico);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtComplementoTecnico);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtEmailTecnico);
            this.groupBox1.Controls.Add(this.mkdFoneCelularTecnico);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.mkdCpfTecnico);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cmbCidadeTecFilho);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtEnderecoTecnico);
            this.groupBox1.Controls.Add(this.txtNomeTecnico);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.btnSair);
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(636, 444);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados do Técnico";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 15);
            this.label2.TabIndex = 76;
            this.label2.Text = "Observações";
            // 
            // txtBairroTecnico
            // 
            this.txtBairroTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBairroTecnico.Location = new System.Drawing.Point(378, 90);
            this.txtBairroTecnico.Name = "txtBairroTecnico";
            this.txtBairroTecnico.Size = new System.Drawing.Size(251, 21);
            this.txtBairroTecnico.TabIndex = 3;
            // 
            // txtObsTecnico
            // 
            this.txtObsTecnico.Location = new System.Drawing.Point(6, 274);
            this.txtObsTecnico.Multiline = true;
            this.txtObsTecnico.Name = "txtObsTecnico";
            this.txtObsTecnico.Size = new System.Drawing.Size(432, 148);
            this.txtObsTecnico.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(165, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 15);
            this.label16.TabIndex = 73;
            this.label16.Text = "Complemento";
            // 
            // txtComplementoTecnico
            // 
            this.txtComplementoTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplementoTecnico.Location = new System.Drawing.Point(165, 150);
            this.txtComplementoTecnico.Name = "txtComplementoTecnico";
            this.txtComplementoTecnico.Size = new System.Drawing.Size(464, 21);
            this.txtComplementoTecnico.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 71;
            this.label1.Text = "E-mail";
            // 
            // txtEmailTecnico
            // 
            this.txtEmailTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailTecnico.Location = new System.Drawing.Point(6, 212);
            this.txtEmailTecnico.Name = "txtEmailTecnico";
            this.txtEmailTecnico.Size = new System.Drawing.Size(334, 21);
            this.txtEmailTecnico.TabIndex = 6;
            // 
            // mkdFoneCelularTecnico
            // 
            this.mkdFoneCelularTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mkdFoneCelularTecnico.Location = new System.Drawing.Point(346, 212);
            this.mkdFoneCelularTecnico.Mask = "(00)00000-0000";
            this.mkdFoneCelularTecnico.Name = "mkdFoneCelularTecnico";
            this.mkdFoneCelularTecnico.Size = new System.Drawing.Size(135, 21);
            this.mkdFoneCelularTecnico.TabIndex = 7;
            this.mkdFoneCelularTecnico.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(343, 194);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 15);
            this.label13.TabIndex = 68;
            this.label13.Text = "Telefone Celular";
            // 
            // mkdCpfTecnico
            // 
            this.mkdCpfTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mkdCpfTecnico.Location = new System.Drawing.Point(487, 212);
            this.mkdCpfTecnico.Mask = "000.000.000.00";
            this.mkdCpfTecnico.Name = "mkdCpfTecnico";
            this.mkdCpfTecnico.Size = new System.Drawing.Size(142, 21);
            this.mkdCpfTecnico.TabIndex = 8;
            this.mkdCpfTecnico.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(484, 194);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 15);
            this.label10.TabIndex = 66;
            this.label10.Text = "CPF";
            // 
            // cmbCidadeTecFilho
            // 
            this.cmbCidadeTecFilho.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCidadeTecFilho.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCidadeTecFilho.DisplayMember = "nome_cidade";
            this.cmbCidadeTecFilho.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCidadeTecFilho.FormattingEnabled = true;
            this.cmbCidadeTecFilho.Location = new System.Drawing.Point(6, 148);
            this.cmbCidadeTecFilho.Name = "cmbCidadeTecFilho";
            this.cmbCidadeTecFilho.Size = new System.Drawing.Size(153, 23);
            this.cmbCidadeTecFilho.TabIndex = 4;
            this.cmbCidadeTecFilho.ValueMember = "cod_cidade";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 15);
            this.label9.TabIndex = 65;
            this.label9.Text = "Cidade";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(375, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 15);
            this.label8.TabIndex = 64;
            this.label8.Text = "Bairro";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 15);
            this.label7.TabIndex = 62;
            this.label7.Text = "Endereço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 61;
            this.label6.Text = "Nome";
            // 
            // txtEnderecoTecnico
            // 
            this.txtEnderecoTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnderecoTecnico.Location = new System.Drawing.Point(6, 90);
            this.txtEnderecoTecnico.Name = "txtEnderecoTecnico";
            this.txtEnderecoTecnico.Size = new System.Drawing.Size(366, 21);
            this.txtEnderecoTecnico.TabIndex = 2;
            // 
            // txtNomeTecnico
            // 
            this.txtNomeTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeTecnico.Location = new System.Drawing.Point(6, 37);
            this.txtNomeTecnico.Name = "txtNomeTecnico";
            this.txtNomeTecnico.Size = new System.Drawing.Size(623, 21);
            this.txtNomeTecnico.TabIndex = 1;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.Image = ((System.Drawing.Image)(resources.GetObject("btnSalvar.Image")));
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(469, 288);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(142, 38);
            this.btnSalvar.TabIndex = 10;
            this.btnSalvar.Text = "Salvar Cadastro";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(469, 351);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(142, 38);
            this.btnSair.TabIndex = 11;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(141, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 15);
            this.label4.TabIndex = 58;
            this.label4.Text = "Data Cadastro";
            // 
            // txtCodTecnico
            // 
            this.txtCodTecnico.Enabled = false;
            this.txtCodTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodTecnico.HideSelection = false;
            this.txtCodTecnico.Location = new System.Drawing.Point(21, 28);
            this.txtCodTecnico.Name = "txtCodTecnico";
            this.txtCodTecnico.Size = new System.Drawing.Size(65, 21);
            this.txtCodTecnico.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 57;
            this.label5.Text = "Registro";
            // 
            // txtDataTecnico
            // 
            this.txtDataTecnico.Enabled = false;
            this.txtDataTecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataTecnico.HideSelection = false;
            this.txtDataTecnico.Location = new System.Drawing.Point(144, 28);
            this.txtDataTecnico.Name = "txtDataTecnico";
            this.txtDataTecnico.Size = new System.Drawing.Size(118, 21);
            this.txtDataTecnico.TabIndex = 59;
            // 
            // frmTecnicoFilho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 526);
            this.Controls.Add(this.txtDataTecnico);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCodTecnico);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTecnicoFilho";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CADASTRAR/ALTERAR TÉCNICO";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmTecnicoFilho_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmTecnicoFilho_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCodTecnico;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.TextBox txtObsTecnico;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtComplementoTecnico;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmailTecnico;
        private System.Windows.Forms.MaskedTextBox mkdFoneCelularTecnico;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox mkdCpfTecnico;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbCidadeTecFilho;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEnderecoTecnico;
        private System.Windows.Forms.TextBox txtNomeTecnico;
        private System.Windows.Forms.TextBox txtBairroTecnico;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDataTecnico;
    }
}