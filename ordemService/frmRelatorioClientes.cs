﻿using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmRelatorioClientes : Form
    {
        private List<ClienteVO> Lista;

        public frmRelatorioClientes()
        {
            InitializeComponent();
        }

        public void RecebeResultado(List<ClienteVO> lst)
        {
            Lista = lst;
            ClienteVOBindingSource.DataSource = Lista;
            this.reportViewer1.RefreshReport();


        }


        private void frmRelatorioClientes_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void frmRelatorioClientes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}
