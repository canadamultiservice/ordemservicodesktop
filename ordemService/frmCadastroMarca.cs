﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmCadastroMarca : Form
    {
        public frmCadastroMarca()
        {
            InitializeComponent();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            CarregarTela();
            btnSalvar.Enabled = true;
            txtNomeFabricante.Enabled = true;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                MarcaDAO objdao = new MarcaDAO();

                tb_marca objmarca = new tb_marca();

                objmarca.nome_marca = txtNomeFabricante.Text;


                try
                {
                    if (txtCod.Text.Trim() == string.Empty)
                    {
                        objdao.Inserirmarca(objmarca);
                    }
                    else
                    {
                        objmarca.cod_marca = Convert.ToInt32(txtCod.Text);
                        objdao.AlterarMarca(objmarca);
                    }
                    CarregarTela();
                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

                
                catch (Exception)
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                MarcaDAO objdao = new MarcaDAO();

                tb_marca objmarca = new tb_marca();

                objmarca.nome_marca = txtNomeFabricante.Text;
                try
                {

                    if (txtCod.Text.Trim() == string.Empty)
                    {
                        objdao.Inserirmarca(objmarca);
                    }
                    else
                    {
                        objmarca.cod_marca = Convert.ToInt32(txtCod.Text);
                        objdao.AlterarMarca(objmarca);
                    }
                    CarregarTela();
                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {

                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {


            try
            {
                MarcaDAO objdao = new MarcaDAO();
                objdao.ExcluirMarca(Convert.ToInt32(txtCod.Text));

                MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CarregarTela();
            }
            catch (Exception)
            {

                MessageBox.Show(MenssagemAmbiente.ErroMsg, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCadastroMarca_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }

        private void dgvModeloFabricante_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvFabricante.RowCount > 0)
            {
                tb_marca objlinha = (tb_marca)dgvFabricante.CurrentRow.DataBoundItem;

                txtCod.Text = Convert.ToString(objlinha.cod_marca);
                txtNomeFabricante.Text = objlinha.nome_marca;
                HabilitarBotoes(2);
                btnExcluir.Enabled = true;
                btnAlterar.Enabled = true;
                txtNomeFabricante.Enabled = true;

            }
        }

        private void CarregarTela()
        {
            CarregarGrid();
            LimparCampos();
            HabilitarBotoes(2);
        }

        private bool validarCampos()
        {
            bool ret = true;
            string campos = string.Empty;

            if (txtNomeFabricante.Text.Trim() == string.Empty)
            {
                ret = false;
                campos += "\n -Digitar Nome do Fabricante!";
            }
            if (!ret)
            {
                MessageBox.Show(String.Concat(MenssagemAmbiente.TituloMsgObrigatorio, campos), "ATENÇÃO!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return ret;
        }

        private void LimparCampos()
        {
            txtCod.Clear();
            txtNomeFabricante.Clear();
        }

        private void CarregarGrid()
        {
            MarcaDAO objdao = new MarcaDAO();

            dgvFabricante.DataSource = objdao.ListarMarcas();

            dgvFabricante.Columns["nome_marca"].HeaderText = "MARCA";
            dgvFabricante.Columns["cod_marca"].HeaderText = "CODIGO";
            dgvFabricante.Columns["tb_modelo"].Visible = false;
        }

        private void HabilitarBotoes(int cod)
        {
            if (cod == 1)
            {
                btnNovo.Enabled = true;
                btnSalvar.Enabled = true;
                btnExcluir.Enabled = true;
                btnAlterar.Enabled = true;
                txtNomeFabricante.Enabled = true;
            }
            else if (cod == 2)
            {

                btnSalvar.Enabled = false;
                btnExcluir.Enabled = false;
                btnAlterar.Enabled = false;
                txtNomeFabricante.Enabled = false;
            }
        }

        private void frmCadastroMarca_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}
