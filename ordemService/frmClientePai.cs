﻿using DAO;
using DAO.VO;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ordemService
{
    public partial class frmClientePai : Form
    {
        public frmClientePai()
        {
            InitializeComponent();
        }


        public void CarregarGrid()
        {

            ClienteDAO objgrid = new ClienteDAO();

            dgvRelatorioClientes.DataSource = objgrid.ConsultarCliente(txtConsultaCliente.Text.Trim()).ToList();

            //dgvRelatorioClientes.Refresh();


            dgvRelatorioClientes.Columns["CodCliente"].Visible = false;
            dgvRelatorioClientes.Columns["Celular"].Visible = false;
            dgvRelatorioClientes.Columns["Complemento"].Visible = false;
            dgvRelatorioClientes.Columns["Cpf"].Visible = false;
            dgvRelatorioClientes.Columns["inscricaoEstadual"].Visible = false;
            dgvRelatorioClientes.Columns["bairro"].Visible = false;
            dgvRelatorioClientes.Columns["DataCadastro"].Visible = false;
            dgvRelatorioClientes.Columns["Observacao"].Visible = false;
            dgvRelatorioClientes.Columns["cod_cidade"].Visible = false;
            dgvRelatorioClientes.Columns["Contato"].Visible = false;
            dgvRelatorioClientes.Columns["Telefone"].Visible = false;
            dgvRelatorioClientes.Columns["Email"].Visible = false;
            dgvRelatorioClientes.Columns["Cnpj"].Visible = false;


            dgvRelatorioClientes.Columns["Telefone"].HeaderText = "Telefone";
            dgvRelatorioClientes.Columns["Nome"].HeaderText = "Cliente";
            dgvRelatorioClientes.Columns["nome_cidade"].HeaderText = "Cidade";
        }

        public void CarregarTela()
        {
            CarregarGrid();

        }


        private void btnNovoCliente_Click(object sender, EventArgs e)
        {
            frmClienteFilho frmFilho = new frmClienteFilho();
            frmFilho.CarregarCidade(0);
            frmFilho.Show();
        }

        private void btnEditarCliente_Click(object sender, EventArgs e)
        {
            if (dgvRelatorioClientes.RowCount > 0)
            {
                ClienteVO obj = (ClienteVO)dgvRelatorioClientes.CurrentRow.DataBoundItem;


                frmClienteFilho objform = new frmClienteFilho();
                objform.CarregarCidade(0);
                objform.RecebeInformacao(obj);
                objform.Activate();
                objform.Show();

            }
        }

        private void btnExcluirCliente_Click(object sender, EventArgs e)
        {
            if (dgvRelatorioClientes.RowCount > 0)
            {
                try
                {
                    ClienteVO obj = (ClienteVO)dgvRelatorioClientes.CurrentRow.DataBoundItem;
                    ClienteDAO objCliente = new ClienteDAO();
                    objCliente.ExcluirCliente(Convert.ToInt32(obj.CodCliente));
                    MessageBox.Show(MenssagemAmbiente.SucessoMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CarregarGrid();
                }
                catch (Exception)
                {
                    MessageBox.Show(MenssagemAmbiente.ErroMsg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }
        }

        private void btnImprimirRelstorioCliente_Click(object sender, EventArgs e)
        {
            if (dgvRelatorioClientes.RowCount > 0)
            {
                ClienteVO obj = (ClienteVO)dgvRelatorioClientes.CurrentRow.DataBoundItem;

                ClienteDAO dao = new ClienteDAO();

                string nome = obj.Nome;

                List<ClienteVO> listaCliente = dao.ConsultarCliente(nome);

                FormsReports.frmImprimirCliente frm = new FormsReports.frmImprimirCliente();
                frm.RecebeResultado(listaCliente);
                frm.ShowDialog();


            }
        }

        private void frmTesteCliente_Load(object sender, EventArgs e)
        {
            CarregarTela();
        }

        private void txtConsultaCliente_TextChanged(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void dgvRelatorioClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvRelatorioClientes.RowCount > 0)
            {
                ClienteVO obj = (ClienteVO)dgvRelatorioClientes.CurrentRow.DataBoundItem;


                frmClienteFilho objform = new frmClienteFilho();
                objform.CarregarCidade(0);
                objform.RecebeInformacao(obj);
                objform.Activate();

                objform.Show();

            }
        }
    }
}
