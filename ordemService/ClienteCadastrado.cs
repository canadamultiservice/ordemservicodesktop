﻿using DAO;
using DAO.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ordemService
{
    public partial class ClienteCadastrado : MetroFramework.Forms.MetroForm
    {
                
        
        public ClienteCadastrado()
        {
            InitializeComponent();
            
        }


        #region TELA

        private void ClienteCadastrado_Load(object sender, EventArgs e)
        {
            Carregargrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvClienteCadastrado.RowCount > 0)
            {
                 ClienteVO obj = (ClienteVO)dgvClienteCadastrado.CurrentRow.DataBoundItem;

                if (Util.tipo == Util.TipoTela.Orcamento)
                {

                    frmOrcamento objform = Util.RetornafrmOrcamento();
                    //objform.Tela = 1;
                    objform.RecebeInformaçao(obj);
                    objform.Activate();
                    Util.chamadora(Util.TipoTela.Orcamento);                   
                    objform.Show();
                    this.Hide();
                    
                  


                }
                else if (Util.tipo == Util.TipoTela.OS)
                {
                    frmCadastroOS frm = Util.RetornaFormCadastro();
                    frm.Tela = 1;
                    frm.RecebeInformacao(obj);
                    frm.Activate();
                    Util.chamadora(Util.TipoTela.OS);
                    frm.Show();
                    this.Hide();
                }







            }


        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            Carregargrid();


        }

        #endregion

        //-----------------//-----------------------//---------

        #region METODOS

        private void Carregargrid()
        {
            ClienteDAO objdao = new ClienteDAO();

            dgvClienteCadastrado.DataSource = objdao.ConsultarCliente(TxtPesquisar.Text.Trim());

            //esconde colunas
            dgvClienteCadastrado.Columns["CodCliente"].Visible = false;
            dgvClienteCadastrado.Columns["cod_cidade"].Visible = false;
            dgvClienteCadastrado.Columns["celular"].Visible = false;
            dgvClienteCadastrado.Columns["Cnpj"].Visible = false;
            dgvClienteCadastrado.Columns["Cpf"].Visible = false;
            dgvClienteCadastrado.Columns["InscricaoEstadual"].Visible = false;
            dgvClienteCadastrado.Columns["DataCadastro"].Visible = false;
            dgvClienteCadastrado.Columns["Observacao"].Visible = false;
            dgvClienteCadastrado.Columns["Complemento"].Visible = false;
            dgvClienteCadastrado.Columns["Contato"].Visible = false;
            dgvClienteCadastrado.Columns["Bairro"].Visible = false;
            dgvClienteCadastrado.Columns["Telefone"].Visible = false;
            dgvClienteCadastrado.Columns["Email"].Visible = false;

            //altera titulo das colunas

            dgvClienteCadastrado.Columns["nome_cidade"].HeaderText = "Cidade";
            //dgvClienteCadastrado.Columns[""].HeaderText = "";
            //dgvClienteCadastrado.Columns[""].HeaderText = "";
            //dgvClienteCadastrado.Columns[""].HeaderText = "";

        }

        #endregion

        private void ClienteCadastrado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Hide();
            }
        }

        private void TxtPesquisar_TextChanged(object sender, EventArgs e)
        {
            Carregargrid();
        }
    }
}
