﻿namespace ordemService
{
    partial class frmMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvMonitor = new System.Windows.Forms.DataGridView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.relogioMonitor = new System.Windows.Forms.TextBox();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.lblPainel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtResultAberta = new System.Windows.Forms.TextBox();
            this.txtResultFechado = new System.Windows.Forms.TextBox();
            this.txtResultCancelado = new System.Windows.Forms.TextBox();
            this.labefd = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMonitor
            // 
            this.dgvMonitor.AllowUserToOrderColumns = true;
            this.dgvMonitor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMonitor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMonitor.GridColor = System.Drawing.Color.LightSlateGray;
            this.dgvMonitor.Location = new System.Drawing.Point(22, 41);
            this.dgvMonitor.MultiSelect = false;
            this.dgvMonitor.Name = "dgvMonitor";
            this.dgvMonitor.ReadOnly = true;
            this.dgvMonitor.RowHeadersVisible = false;
            this.dgvMonitor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonitor.Size = new System.Drawing.Size(1328, 424);
            this.dgvMonitor.TabIndex = 0;
            this.dgvMonitor.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvMonitor_CellFormatting);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Green;
            this.pictureBox1.Location = new System.Drawing.Point(1183, 664);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(19, 15);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1284, 666);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Cancelado";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox2.Location = new System.Drawing.Point(1114, 663);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 15);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1133, 666);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Aberto";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Red;
            this.pictureBox3.Location = new System.Drawing.Point(1265, 664);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 15);
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1203, 666);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Fechado";
            // 
            // relogioMonitor
            // 
            this.relogioMonitor.Enabled = false;
            this.relogioMonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.relogioMonitor.HideSelection = false;
            this.relogioMonitor.Location = new System.Drawing.Point(22, 655);
            this.relogioMonitor.Name = "relogioMonitor";
            this.relogioMonitor.Size = new System.Drawing.Size(669, 40);
            this.relogioMonitor.TabIndex = 15;
            this.relogioMonitor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer3
            // 
            this.timer3.Interval = 10000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // lblPainel
            // 
            this.lblPainel.AutoSize = true;
            this.lblPainel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPainel.Location = new System.Drawing.Point(467, 9);
            this.lblPainel.Name = "lblPainel";
            this.lblPainel.Size = new System.Drawing.Size(350, 29);
            this.lblPainel.TabIndex = 16;
            this.lblPainel.Text = "MONITOR DE ATENDIMENTO";
            this.lblPainel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.labefd);
            this.groupBox1.Controls.Add(this.txtResultCancelado);
            this.groupBox1.Controls.Add(this.txtResultFechado);
            this.groupBox1.Controls.Add(this.txtResultAberta);
            this.groupBox1.Location = new System.Drawing.Point(12, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1347, 701);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // txtResultAberta
            // 
            this.txtResultAberta.BackColor = System.Drawing.Color.Yellow;
            this.txtResultAberta.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResultAberta.Enabled = false;
            this.txtResultAberta.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultAberta.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtResultAberta.Location = new System.Drawing.Point(10, 510);
            this.txtResultAberta.Name = "txtResultAberta";
            this.txtResultAberta.Size = new System.Drawing.Size(289, 109);
            this.txtResultAberta.TabIndex = 0;
            this.txtResultAberta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResultFechado
            // 
            this.txtResultFechado.BackColor = System.Drawing.Color.Green;
            this.txtResultFechado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResultFechado.Enabled = false;
            this.txtResultFechado.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultFechado.ForeColor = System.Drawing.SystemColors.Info;
            this.txtResultFechado.Location = new System.Drawing.Point(334, 510);
            this.txtResultFechado.Name = "txtResultFechado";
            this.txtResultFechado.Size = new System.Drawing.Size(289, 109);
            this.txtResultFechado.TabIndex = 0;
            this.txtResultFechado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResultCancelado
            // 
            this.txtResultCancelado.BackColor = System.Drawing.Color.Red;
            this.txtResultCancelado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResultCancelado.Enabled = false;
            this.txtResultCancelado.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultCancelado.ForeColor = System.Drawing.SystemColors.Info;
            this.txtResultCancelado.HideSelection = false;
            this.txtResultCancelado.Location = new System.Drawing.Point(661, 510);
            this.txtResultCancelado.Name = "txtResultCancelado";
            this.txtResultCancelado.Size = new System.Drawing.Size(289, 109);
            this.txtResultCancelado.TabIndex = 0;
            this.txtResultCancelado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labefd
            // 
            this.labefd.AutoSize = true;
            this.labefd.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labefd.Location = new System.Drawing.Point(26, 476);
            this.labefd.Name = "labefd";
            this.labefd.Size = new System.Drawing.Size(260, 31);
            this.labefd.TabIndex = 1;
            this.labefd.Text = "EM ATENDIMENTO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(398, 476);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 31);
            this.label4.TabIndex = 1;
            this.label4.Text = "ATENDIDAS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(704, 476);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(199, 31);
            this.label5.TabIndex = 1;
            this.label5.Text = "CANCELADAS";
            // 
            // frmMonitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 697);
            this.Controls.Add(this.lblPainel);
            this.Controls.Add(this.relogioMonitor);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvMonitor);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMonitor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MONITOR DE ATENDIMENTO";
            this.Load += new System.EventHandler(this.frmMonitor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonitor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.DataGridView dgvMonitor;
        public System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox relogioMonitor;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Label lblPainel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtResultAberta;
        private System.Windows.Forms.TextBox txtResultCancelado;
        private System.Windows.Forms.TextBox txtResultFechado;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labefd;
    }
}